//
//  ViewController.swift
//  DubaiKitchen
//
//  Created by Pankaj Kumhar on 2/21/19.
//  Copyright © 2019 Pankaj Kumhar. All rights reserved.
//

import UIKit

enum UserLoginType:Int
{
    case ADMIN = 1
    case USER = 2
    case COOK = 3
}

public var seletcted_Role = NewtworkManager.shared.AdminLogOn

class ViewController: CommonViewController {

    @IBOutlet weak var mToggleBtn: UISwitch!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var userid: UITextField!
   let dispatchGroup = DispatchGroup()
    var isLoginSuccess:Bool = false
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        if let keyGuest  = UserDefaults.standard.value(forKey: defaultsKeys.langSelected) {
            if keyGuest as! String == ARABIC {
                TABLE_LANGUAGE = ARABIC
                //mToggleBtn.setOn(true, animated: true)
                UIView.appearance().semanticContentAttribute = .forceRightToLeft
                UserDefaults.standard.set(ARABIC, forKey: defaultsKeys.langSelected)
                UserDefaults.standard.set(ARABIC_STORYBOARD, forKey: defaultsKeys.sbSelected)
                UserDefaults.standard.synchronize()
            } else {
                TABLE_LANGUAGE = ENGLISH
                // mToggleBtn.setOn(false, animated: true)
                UIView.appearance().semanticContentAttribute = .forceLeftToRight
                UserDefaults.standard.set(ENGLISH, forKey: defaultsKeys.langSelected)
                UserDefaults.standard.set(ENGLISH_STORYBOARD, forKey: defaultsKeys.sbSelected)
                UserDefaults.standard.synchronize()
            }
        } else {
            TABLE_LANGUAGE = ENGLISH
            //mToggleBtn.setOn(false, animated: true)
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            UserDefaults.standard.set(ENGLISH, forKey: defaultsKeys.langSelected)
            UserDefaults.standard.set(ENGLISH_STORYBOARD, forKey: defaultsKeys.sbSelected)
            UserDefaults.standard.synchronize()
        }
       // self.navigationController?.navigationBar.isHidden = false
        // Do any additional setup after loading the view, typically from a nib.
        self.userid.placeholder = NSLocalizedString("Email ID", comment:"")
        self.password.isSecureTextEntry = true
//        self.userid.text = "cooktilak@gm.com"//"d@d.com"//1"
//        self.password.text = "123456789"//"123"//"123"
        
//        self.userid.text = "d@d.com"//1"
//        self.password.text = "123"//"123"
        
//        self.userid.text = "tilak2@gmail.com"//1"
//        self.password.text = "123456789"//"123"
        
//        self.userid.text = "k@k.com"//1"
//        self.password.text = "a"//"123"
    }


    @IBAction func btnLanguageChangedClicked(_ sender: UIButton){
        let transition: UIView.AnimationOptions = .transitionFlipFromLeft
        if TABLE_LANGUAGE == ENGLISH {
            TABLE_LANGUAGE = ARABIC
            STORYBOARD_LANGUAGE = ARABIC_STORYBOARD
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            
            UserDefaults.standard.set(ARABIC, forKey: defaultsKeys.langSelected)
            UserDefaults.standard.set(ARABIC_STORYBOARD, forKey: defaultsKeys.sbSelected)
            UserDefaults.standard.synchronize()
        } else {
            TABLE_LANGUAGE = ENGLISH
            STORYBOARD_LANGUAGE = ENGLISH_STORYBOARD
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            
            UserDefaults.standard.set(ENGLISH, forKey: defaultsKeys.langSelected)
            UserDefaults.standard.set(ENGLISH_STORYBOARD, forKey: defaultsKeys.sbSelected)
            UserDefaults.standard.synchronize()
        }
        
        var nvc = UINavigationController()
        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        nvc = UINavigationController(rootViewController: (self.storyboard?.instantiateViewController(withIdentifier: "ViewController"))!)
        rootviewcontroller.rootViewController = nvc //self.storyboard?.instantiateViewController(withIdentifier: "ViewController")
        let mainwindow = (UIApplication.shared.delegate?.window!)!
        mainwindow.backgroundColor = UIColor(hue: 0.6477, saturation: 0.6314, brightness: 0.6077, alpha: 0.8)
        UIView.transition(with: mainwindow, duration: 0.55001, options: transition, animations: { () -> Void in
        }) { (finished) -> Void in
            
        }
        
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        
        //
        
    }
    @IBAction func btnLoginClicked(_ sender: UIButton)
    {
        var struid1 = ""
         var strpass1 = ""
        if let struid = self.userid.text
        {
            struid1 = struid
        }
        
        if let struid = self.password.text
        {
            strpass1 = struid
        }
        
        if(struid1.isEmpty)
        {
            self.showAlert(title: "", message: NSLocalizedString("Plesae enter email", comment:""))
            return
        }
        if(!struid1.isValidEmail())
        {
            self.showAlert(title: "", message: NSLocalizedString("Plesae enter valid email", comment:""))
            return
        }
        if(strpass1.isEmpty)
        {
            self.showAlert(title: "", message: NSLocalizedString("Plesae enter password", comment:""))
            return
        }
        

        self.dispatchGroup.enter()
        self.showLoader()
        NewtworkManager.shared.GetUserTypeByEmail(struid1) { (status1, datain1) in
            self.hideloader()
            if (status1)
            {
                NewtworkManager.shared.logoinUserid = struid1
                self.showLoader()
                NewtworkManager.shared.GetLogin("", struid1, strpass1)
                { (status, dataout) in
                    self.hideloader()
                    print("\(dataout ?? "" as AnyObject)")
                    
                    if(status && !NewtworkManager.shared.dwpi_tokenid.isEmpty)
                    {
                        self.isLoginSuccess = true
                        isUserLoggedIn = true
                        DispatchQueue.main.async
                            {
                                if(seletcted_Role == NewtworkManager.shared.UserLogOn)
                                {
                                    let adminVc = self.storyboard?.instantiateViewController(withIdentifier: "USerVC") as! USerVC
                                    self.navigationController?.pushViewController(adminVc, animated: false)
                                } else {
                                    let adminVc = self.storyboard?.instantiateViewController(withIdentifier: "AdminVC") as! AdminVC
                                    self.navigationController?.pushViewController(adminVc, animated: false)
                                }
                                
                        }
                        
                        let dispatchQueue = DispatchQueue(label: "QueueIdentification", qos: .background)
                        dispatchQueue.async{
                             self.updateNotificationDeviceId()
                        }
                        
                       
                    } else {
                         self.isLoginSuccess = false
                         isUserLoggedIn = false
                        DispatchQueue.main.async
                            {
                            self.alert(message: NSLocalizedString("Authentication failed", comment:""), title: NSLocalizedString("info", comment:""))
                        }
                    }
                    
                    self.dispatchGroup.leave()
                }
            } else {
                self.dispatchGroup.leave()
                DispatchQueue.main.async
                    {
                        self.alert(message: NSLocalizedString("Authentication failed", comment:""), title: NSLocalizedString("info", comment:""))
                }
            }
        }
        
        //self.dispatchGroup.enter()
        
        dispatchGroup.notify(queue: .main)
        {
            print("Both functions complete ")
            DispatchQueue.main.async {
               // self.tblView.reloadData()
            }
        }
    }
    
    func updateNotificationDeviceId()
    {
        if (self.notificationsToken != nil && self.isLoginSuccess)
        {
            let notificationToken1 = self.notificationsToken
            var lang = "E"
            if(isApplanguageIsArabic)
            {
                lang = "A"
            }
            NewtworkManager.shared.UpdateDeviceID(notificationToken1!,lang) { (status, dataIn) in
                //self.dispatchGroup.leave()
                
            }
        } else {
           ///self.dispatchGroup.leave()
        }
        
    }
    
    @IBAction func btnUserRoleClicked(_ sender: UIButton) {
        
        switch sender.tag {
        case 1:
            //admin
            selectedUserRole = 1
            seletcted_Role = NewtworkManager.shared.AdminLogOn
            self.userid.text = "d@d.com"//1"
            self.password.text = "123"//"123"
            //
        case 2:
            //user
            selectedUserRole = 2
            seletcted_Role = NewtworkManager.shared.UserLogOn
            self.userid.text = "vnsingh24@gmail.com"//1"
            self.password.text = "123"//"123"
        case 3:
            //cook
            selectedUserRole = 3
            seletcted_Role = NewtworkManager.shared.KichtenLogOn
            self.userid.text = "k@k.com"//1"
            self.password.text = "a"//"123"
        case 4:
                selectedUserRole = 3
                seletcted_Role = NewtworkManager.shared.CookLogOn
                self.userid.text = "cook@cook.com"//1"
                self.password.text = "123456789"//"123"
        default:
            print("")
        }
        
    }
}

