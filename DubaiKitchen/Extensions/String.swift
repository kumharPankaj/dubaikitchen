//
//  StringExtension.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 1/22/15.
//  Copyright (c) 2015 Yuji Hato. All rights reserved.
//

import Foundation

extension String {
    static func className(_ aClass: AnyClass) -> String {
        return NSStringFromClass(aClass).components(separatedBy: ".").last!
    }
    
    func substring(_ from: Int) -> String {
        return String(describing: [..<self.index(self.startIndex, offsetBy: from)])
        //return self.substring(from: self.characters.index(self.startIndex, offsetBy: from))
    }
    
    var length: Int {
        return self.count
    }
    func floatValue() -> Float? {
            if let floatval = Float(self) {
                return floatval
            }
            return nil
        }
    
    func safelyLimitedTo(length n: Int)->String {
        if (self.count <= n) {
            return self
        }
        return String( Array(self).prefix(upTo: n) )
    }
    
   
    var localized: String {
        
        return NSLocalizedString(self, comment: "")
        
        //return  NSLocalizedString(self, tableName: TABLE_LANGUAGE, bundle: Bundle.main, value: "", comment:COMMENT_HEADER)
    }
    func localizedNumber(_ from: String) -> String  {
        if let myInteger = Int(from) {
            let myNumber = NSNumber(value:myInteger)
            let numberFormatter  = NumberFormatter()
            numberFormatter.numberStyle = .decimal
            var indetifier = ""
            if TABLE_LANGUAGE == ENGLISH {
                indetifier = "en"
            }
            else{
                indetifier = "ar"
                
            }
            let locale  = Locale.init(identifier: indetifier)
            numberFormatter.locale  = locale
            return numberFormatter.string(from: myNumber)!
        }
        return ""
    }
}

