//
//  String+Extension.swift
//  ChartIQDemo
//
//  Created by Tao Man Kit on 12/1/2017.
//  Copyright © 2017 ROKO. All rights reserved.
//

import Foundation

extension String {
    var hex: Int? {
        return Int(self, radix: 16)
    }
    
    func replace(target: String, withString: String) -> String
    {
        return self.replacingOccurrences(of: target, with: withString, options: NSString.CompareOptions.literal, range: nil)
    }
    
    func index(from: Int) -> Index {
        return self.index(startIndex, offsetBy: from)
    }
    func substring(from: Int) -> String {
        let fromIndex = index(from: from)
        return String(self[..<fromIndex])//substring(from: fromIndex)
    }
    func substring(to: Int) -> String {
        let toIndex = index(from: to)
        return String(self[toIndex...])
    }
    func substring(with r: Range<Int>) -> String {
        let startIndex = index(from: r.lowerBound)
        let endIndex = index(from: r.upperBound)
        return String(self[startIndex..<endIndex])
    }
    
    //Check sentences in String
    func contains(word : String) -> Bool
    {
        do {
            let regex = try NSRegularExpression(pattern: "\\b\(word)\\b")
            return regex.numberOfMatches(in: self, range: NSRange(location: 0, length: self.utf16.count)) > 0
        } catch {
            return false
        }
    }
}

