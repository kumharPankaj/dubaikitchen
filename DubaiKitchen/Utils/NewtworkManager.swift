//
//  NewtworkManager.swift
//  DubaiKitchen
//
//  Created by tilak raj verma on 02/03/19.
//  Copyright © 2019 Pankaj Kumhar. All rights reserved.
//

import UIKit

public typealias DKAddUsers = [DKAddUserModel]
var arrUserCartItems = [DKAPIOrderElement]()
var arrGlobalActiveInactiveUsers = [DKAPIUserResponseElement]()
var isUserLoggedIn:Bool = false
var arrCookComment = [String]()

public class NewtworkManager
{
    
    
    static let shared = NewtworkManager()
     let baseUrl = "http://testdubaicookapi.diquery.com/"//"http://dubaicookapi.diquery.com/"
   
     let UserLogOn = "UserLogOn"
     let AdminLogOn = "AdminLogOn"
     let KichtenLogOn = "KichtenLogOn"
    let CookLogOn = "CookLogOn"
     var dwpi_tokenid = ""
    var logoinUserType = ""
    var userType = ""
    var logoinUserid = ""
    var selectedUser:DKAPIUserResponseElement? = nil
    
    
    func Callwebservices(_ apiToken:String? = nil ,_ requestType:String,_ urlstring:String,_ bodydata:String? = nil ,_ callback:WebCallbackHandler? = nil)
    {
        let url = URL(string: urlstring)!
        let urlSession = URLSession.shared
        var getRequest = URLRequest(url: url)
        getRequest.httpMethod = requestType
        if(requestType == "POST")
        {
            if let bodyda = bodydata
            {
               getRequest.httpBody = Data(bodyda.utf8)
            }
           
            if(urlstring.contains(word: "Account"))
            {
                getRequest.setValue(self.userType, forHTTPHeaderField: "UserType")
            }
            else {
                 getRequest.setValue(self.userType, forHTTPHeaderField: "UserType")
            }
            
            getRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            if let token_1 = apiToken
            {
                getRequest.setValue(token_1, forHTTPHeaderField: "DWPI_tokenid")
            }
        }
        else
        {
            if let token_1 = apiToken
            {
                getRequest.setValue(token_1, forHTTPHeaderField: "DWPI_tokenid")
            }
             getRequest.setValue(self.userType, forHTTPHeaderField: "UserType")
        }
        
        let task = urlSession.dataTask(with: getRequest as URLRequest, completionHandler: { data, urlresponse, error in
            if let err = error
            {
                callback!(false,err.localizedDescription as AnyObject)
            }
            else if let httpResponse = urlresponse as? HTTPURLResponse
                {
                    if (httpResponse.statusCode == 200)
                    {
                        if((httpResponse.url?.absoluteString.contains(word: "Account"))!)
                        {
                            if let token = httpResponse.allHeaderFields["DWPI_tokenid"] as? String
                            {
                                   self.dwpi_tokenid =  token
                            }
                         if let logoinUserid = httpResponse.allHeaderFields["LoginUserID"] as? String
                         {
                            self.logoinUserid = logoinUserid
                            }
                            
                            if let userType = httpResponse.allHeaderFields["UserType"] as? String
                            {
                                self.userType = userType
                            }
                        }
                        
                        if let datain = data
                        {
                            let backToString = String(data: datain, encoding: String.Encoding.utf8) as String!
                            print("\(backToString)")
                           
//                            if let _ = httpResponse.url?.absoluteString.contains(word: "Account")
//                            {
//                                callback!(true,backToString as AnyObject)
//                                return
//                            }
                            
                            if let back = backToString
                            {
                               
                                callback!(true,back.data(using: String.Encoding.utf8) as AnyObject)
                            }
                            else {
                                callback!(true,backToString!.data(using: String.Encoding.utf8) as AnyObject)
                            }
                        }
                        else
                        {
                            callback!(false,"No data available" as AnyObject)
                        }
                    }
                    else
                    {
                        callback!(false,"No data available" as AnyObject)
                    }
                }
                else
                {
                    callback!(false,"No data available" as AnyObject)
                }
        })
        
        // then call task.resume to fetch the results.
        task.resume()
    }
    
    //Account
    //POST api/Account/UserLogOn?userid={userid}&password={password}
    //POST api/Account/AdminLogOn?userid={userid}&password={password}
    //POST api/Account/KichtenLogOn?userid={userid}&password={password}
    
    func GetLogin(_ userType1:String,_ userid:String,_ password:String ,callback:WebCallbackHandler? = nil)
    {
        if( self.userType == "Admin")
        {
            self.logoinUserType = AdminLogOn
             selectedUserRole = 1
            
        }
        if( self.userType == "Kitchen")
        {
            selectedUserRole = 3
            self.logoinUserType = KichtenLogOn
        }
        if( self.userType == "User")
        {
            self.logoinUserType = UserLogOn
            selectedUserRole = 2
        }
        if( self.userType == "Cook")
        {
             selectedUserRole = 3
            self.logoinUserType = CookLogOn
        }
        seletcted_Role = self.logoinUserType
        
       let strUrl = baseUrl+"api/Account/"+self.logoinUserType+"?userid="+userid+"&password="+password
        
       
        Callwebservices(nil,"POST", strUrl, nil,callback)
    }
    
    
    //kitchen
    //POST api/kitchen/CreateKitchen
    func CreateKitchen(_ kitchenDetails:DKKitchenAdd,callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/kitchen/"+"CreateKitchen"
        let jsonEncoder = JSONEncoder()
        do
        {
            let jsonData = try jsonEncoder.encode(kitchenDetails)
            let json = String(data: jsonData, encoding: String.Encoding.utf8)
            Callwebservices(self.dwpi_tokenid,"POST", strUrl, json,callback)
        }
        catch {
            
        }
    }
    
    //POST api/kitchen/ModifiedKitchen
    func ModifiedKitchen(_ kitchenDetails:DKAPIKitchenRespone,callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/kitchen/"+"ModifiedKitchen"
        var dictdetails = [String:AnyObject]()
        
        
        dictdetails["KitchenDesc"] = kitchenDetails.kitchenDesc as AnyObject
        dictdetails["KitchenID"] = kitchenDetails.kitchenID as AnyObject
        dictdetails["KitchenTitle"] = kitchenDetails.kitchenTitle as AnyObject
        dictdetails["EmailID"] = "k@k.com" as AnyObject
        dictdetails["Password"] = "a" as AnyObject
        dictdetails["IsActive"] = true as AnyObject
        
        let json = dictdetails.json//kitchenDetails.toDictionary().json
        Callwebservices(self.dwpi_tokenid,"POST", strUrl, json,callback)
    }
    
    //GET api/kitchen/GetKitchenDetailsByID?kitid={kitid}
    func GetKitchenDetailsByID(_ kitid:String,callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/kitchen/"+"GetCategoryDetailsByCategoryID?kitid="+kitid
        Callwebservices(nil,"GET", strUrl, nil,callback)
    }
    
    //GET api/kitchen/GetAllKitchenDetails
    func GetAllKitchenDetails(callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/kitchen/"+"GetAllKitchenDetails"
        Callwebservices(self.dwpi_tokenid,"GET", strUrl, nil){(status,jsonData) in
            if(status)
            {
                do {
                    let json = try JSONSerialization.jsonObject(with: jsonData as! Data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [AnyObject]
                  
                    var arra:[DKAPIKitchenRespone] = [DKAPIKitchenRespone]()
                    
                    if let datajson = json
                    {
                        for obj in datajson
                        {
                            let temp = DKAPIKitchenRespone.init(fromDictionary: obj as! [String : Any])
                            arra.append(temp)
                        }
                        callback!(status,arra as AnyObject)
                    } else {
                        callback!(false,"No data" as AnyObject)
                    }
                    
                }
                catch {
                    callback!(false,"No data" as AnyObject)
                }
            }
            else
            {
                callback!(status,jsonData)
            }
        }
    }
    
   // User
    //GET api/User/GetUserDetails?userid={userid}
    func GetUserDetailsByUserID(_ userid:String,callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/User/"+"GetUserDetailsByID"+"?Userid="+userid
        Callwebservices(self.dwpi_tokenid,"GET", strUrl, nil){ (status, jsonData) in
            
            if(status)
            {
                let backToString = String(data: jsonData as! Data, encoding: String.Encoding.utf8) as String!
                print("\(backToString)")
                let user = try? newJSONDecoder().decode(DKAPIEditUserResponse.self, from: jsonData as! Data)
                if user != nil
                {
                   callback!(status,user as AnyObject)
                }
                else
                {
                    callback!(false,nil)
                }
                
            }
            else
            {
                callback!(status,jsonData)
            }
        }
    }
    
    //POST api/User/AddUser
    
    func AddUser(_ userDetails:DKAddUserModel,callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/User/"+"AddUser"
//        let userDetails = DKAddUserModel.init()
//        userDetails.userName = userName
//        userDetails.gender = gender
//        userDetails.emailID = emailID
//        userDetails.phoneNo = phoneNo
//        userDetails.password = password
        let jsonEncoder = JSONEncoder()
        do
        {
            let jsonData = try jsonEncoder.encode(userDetails)
            let json = String(data: jsonData, encoding: String.Encoding.utf8)
            Callwebservices(self.dwpi_tokenid,"POST", strUrl, json,callback)
        }
        catch {
            
        }
    }
    
    //POST api/User/ModifyUser
    func ModifyUser(_ userDetails:DKAPIEditUserResponse,callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/User/"+"ModifyUser"
        let jsonEncoder = JSONEncoder()
        do
        {
            let jsonData = try jsonEncoder.encode(userDetails)
            let json = String(data: jsonData, encoding: String.Encoding.utf8)
            Callwebservices(self.dwpi_tokenid,"POST", strUrl, json,callback)
        }
        catch {
            
        }
    }
    //POST api/User/ModifyCook
    func ModifyCook(_ userDetails:DKAPIEditCookResponse,callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/Cook/"+"Modifiedcook"
        let jsonEncoder = JSONEncoder()
        do
        {
            let jsonData = try jsonEncoder.encode(userDetails)
            let json = String(data: jsonData, encoding: String.Encoding.utf8)
            Callwebservices(self.dwpi_tokenid,"POST", strUrl, json,callback)
        }
        catch {
            
        }
    }
    
    
    //GET api/User/GetUserDetailsByID?userid={userid}
    func GetUserDetailsByID(_ userid:String,callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/User/"+"GetUserDetailsByID"+"?userid="+userid
        Callwebservices(self.dwpi_tokenid,"GET", strUrl, nil){ (status, jsonData) in
            
            if(status)
            {
                self.selectedUser = try? newJSONDecoder().decode(DKAPIUserResponseElement.self, from: jsonData as! Data)
                callback!(status,self.selectedUser as AnyObject)
            }
            else
            {
                callback!(status,jsonData)
            }
        }
    }
    //GET api/User/GetAllUserDetails
    func GetAllUserDetails(callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/User/"+"GetAllUserDetails"
       
        Callwebservices(self.dwpi_tokenid,"GET", strUrl, nil){ (status, jsonData) in
            
            if(status)
            {
                let dKAddUserModel = try? newJSONDecoder().decode([DKAPIUserResponseElement].self, from: jsonData as! Data)
                callback!(status,dKAddUserModel as AnyObject)
            }
            else
            {
                 callback!(status,jsonData)
            }
        }
    }
    
    //GET api/User/GetAllActiveInActiveUserDetails
    
    func GetAllActiveInActiveUserDetails(callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/User/"+"GetAllActiveInActiveUserDetails"
        
        Callwebservices(self.dwpi_tokenid,"GET", strUrl, nil){ (status, jsonData) in
            
            if(status)
            {
                let dKAddUserModel = try? newJSONDecoder().decode([DKAPIUserResponseElement].self, from: jsonData as! Data)
                callback!(status,dKAddUserModel as AnyObject)
            }
            else
            {
                callback!(status,jsonData)
            }
        }
    }
    
    //Menu
   // POST api/Menu/CreateMenu
    func CreateMenu(_ menuDetails:DKMenuAddMenuModel,callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/Menu/"+"CreateMenu"
        let jsonEncoder = JSONEncoder()
        do
        {
            let jsonData = try jsonEncoder.encode(menuDetails)
            let json = String(data: jsonData, encoding: String.Encoding.utf8)
            Callwebservices(self.dwpi_tokenid,"POST", strUrl, json,callback)
        }
        catch {
            
        }
    }
    
    //POST api/Menu/ModifiedMenu
    func ModifiedMenu(_ menuDetails:DKMenuAddMenuModel,callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/Menu/"+"ModifiedMenu"
        let jsonEncoder = JSONEncoder()
        do
        {
            let jsonData = try jsonEncoder.encode(menuDetails)
            let json = String(data: jsonData, encoding: String.Encoding.utf8)
            Callwebservices(self.dwpi_tokenid,"POST", strUrl, json,callback)
        }
        catch {
            
        }
    }
    
   // GET api/Menu/GetMenuDetailsByMenuID?cookid={cookid}
    func GetMenuDetailsByMenuID(_ menuid:String,callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/Menu/"+"GetMenuDetailsByMenuID"+"?menuid="+menuid
        Callwebservices(self.dwpi_tokenid,"GET", strUrl, nil,callback)
    }
    
   // GET api/Menu/GetAllMenuDetails
    func GetAllMenuDetails(callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/Menu/"+"GetAllMenuDetails"
        Callwebservices(self.dwpi_tokenid,"GET", strUrl, nil){(status,jsonData) in
            if(status)
            {
                do {
                    let json = try JSONSerialization.jsonObject(with: jsonData as! Data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [AnyObject]
                   
                    var arra:[DKAPIMenuResponseElement] = [DKAPIMenuResponseElement]()
                    
                    if let datajson = json
                    {
                        for obj in datajson
                        {
                            let temp = DKAPIMenuResponseElement.init(fromDictionary: obj as! [String : Any])
                            arra.append(temp)
                         
                        }
                        callback!(status,arra as AnyObject)
                    } else {
                        callback!(false,"No data" as AnyObject)
                    }
                    
                }
                catch {
                    callback!(false,"No data" as AnyObject)
                }
            }
            else
            {
                callback!(status,jsonData)
            }
        }
    }
    func GetAllMenuDetailsByCategoryID(categoryID: String, callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/Menu/"+"GetMenuDetailsByCategoryID"+"?CategoryID="+categoryID
        Callwebservices(self.dwpi_tokenid,"GET", strUrl, nil){(status,jsonData) in
            if(status)
            {
                do {
                    let json = try JSONSerialization.jsonObject(with: jsonData as! Data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [AnyObject]
                    
                    var arra:[DKAPIMenuResponseElement] = [DKAPIMenuResponseElement]()
                    
                    if let datajson = json
                    {
                        for obj in datajson
                        {
                            let temp = DKAPIMenuResponseElement.init(fromDictionary: obj as! [String : Any])
                            arra.append(temp)
                            
                        }
                        callback!(status,arra as AnyObject)
                    } else {
                        callback!(false,"No data" as AnyObject)
                    }
                    
                }
                catch {
                    callback!(false,"No data" as AnyObject)
                }
            }
            else
            {
                callback!(status,jsonData)
            }
        }
    }
    //Values
    //GET api/Values/Get
    func GetAllValuesDetails(callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/Values/"+"Get"
        Callwebservices(self.dwpi_tokenid,"GET", strUrl, nil,callback)
    }
    
    //GET api/Values/Get/{id}
    func GetAllValuesDetailsWithId(_ id:String,callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/Values/"+"Get/"+id
        Callwebservices(self.dwpi_tokenid,"GET", strUrl, nil,callback)
    }
    
    //POST api/Values/Post
    func ValuePost(_ postDetails:String = "",callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/Values/"+"Post"
        Callwebservices(self.dwpi_tokenid,"POST", strUrl, postDetails.stringByAddingPercentEncodingForRFC3986(),callback)
    }
    
    //PUT api/Values/Put/{id}
    func ValuePut(_ postDetails:String = "",_ id:String = "",callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/Values/"+"Put/"+id
        Callwebservices(self.dwpi_tokenid,"PUT", strUrl, postDetails.stringByAddingPercentEncodingForRFC3986(),callback)
    }
    //DELETE api/Values/Delete/{id}
    func ValueDelete(_ postDetails:String = "",_ id:String = "",callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/Values/"+"Delete/"+id
        Callwebservices(self.dwpi_tokenid,"DELETE", strUrl, postDetails.stringByAddingPercentEncodingForRFC3986(),callback)
    }
    
    //Category
    //POST api/Category/CreateCategory
    func CreateCategory(_ catDetails:DKCatagoryAddCreate,callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/Category/"+"CreateCategory"
        let jsonEncoder = JSONEncoder()
        do
        {
            
//
//            var dictdetails = [String:AnyObject]()
//            dictdetails["CatId"] = "" as AnyObject
//            dictdetails["CatName"] = catDetails.catName as AnyObject
//            dictdetails["CatDesc"] = catDetails.catDesc as AnyObject
//             dictdetails["IsActive"] = catDetails.isActive as AnyObject
//             dictdetails["CatImage"] = catDetails.catImage as AnyObject
//             dictdetails["CatImageName"] = catDetails.catImageName as AnyObject
//             dictdetails["CatImageExt"] = catDetails.catImageExt as AnyObject
            
            
            let jsonData = try jsonEncoder.encode(catDetails)
            let json = String(data: jsonData, encoding: String.Encoding.utf8)
            //print("\(json)")
            Callwebservices(self.dwpi_tokenid,"POST", strUrl, json,callback)
        }
        catch {
            
        }
    }
    
    //POST api/Category/ModifiedCategory
    func ModifiedCategory(_ catDetails:DKCatagoryAddCreate,callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/Category/"+"ModifiedCategory"
        let jsonEncoder = JSONEncoder()
        do
        {
            let jsonData = try jsonEncoder.encode(catDetails)
            let json = String(data: jsonData, encoding: String.Encoding.utf8)
            Callwebservices(self.dwpi_tokenid,"POST", strUrl, json,callback)
        }
        catch {
            
        }
    }
    
    
    //GET api/Category/GetCategoryDetailsByCategoryID?catid={catid}
    func GetCategoryDetailsByCategoryID(_ catid:String,callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/Category/"+"GetCategoryDetailsByCategoryID?catid="+catid
        Callwebservices(self.dwpi_tokenid,"GET", strUrl, nil,callback)
    }
    
    //GET api/Category/GetAllCategoryDetails
    func GetAllCategoryDetails(callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/Category/"+"GetAllCategoryDetails"
        //Callwebservices(self.dwpi_tokenid,"GET", strUrl, nil,callback)
        
        
        Callwebservices(self.dwpi_tokenid,"GET", strUrl, nil){(status,jsonData) in
            
            if(status)
            {
                do {
                    let json = try JSONSerialization.jsonObject(with: jsonData as! Data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [AnyObject]
                    
                    var arra:[DKAPICatDetail] = [DKAPICatDetail]()
                    
                    if let datajson = json
                    {
                        for obj in datajson
                        {
                            let temp = DKAPICatDetail.init(fromDictionary: obj as! [String : Any])
                            arra.append(temp)
                            
                        }
                        callback!(status,arra as AnyObject)
                    } else {
                        callback!(false,"No data" as AnyObject)
                    }
                    
                }
                catch {
                    callback!(false,"No data" as AnyObject)
                }
            }
            else
            {
                callback!(status,jsonData)
            }
            
            
            /*if(status)
            {
                if(status)
                {
                    let dKAddUserModel = try? newJSONDecoder().decode([DKAPICatDetail].self, from: jsonData as! Data)
                    callback!(status,dKAddUserModel as AnyObject)
                }
                else
                {
                    callback!(status,jsonData)
                }
            }
            else
            {
                callback!(status,jsonData)
            }*/
        }
    }
    
    
    //GET api/Category/GetAllCookDetails
    
    func GetAllCookDetails(callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/Category/"+"GetAllCookDetails"
        Callwebservices(self.dwpi_tokenid,"GET", strUrl, nil,callback)
    }
    
    //UserHome
    //POST api/UserHome/CreateHome

    func CreateHome(_ homeDetails:DKHomeAddHouse,callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/UserHome/"+"CreateHome"
        let jsonEncoder = JSONEncoder()
        do
        {
            let jsonData = try jsonEncoder.encode(homeDetails)
            let json = String(data: jsonData, encoding: String.Encoding.utf8)
            Callwebservices(self.dwpi_tokenid,"POST", strUrl, json,callback)
        }
        catch {
            
        }
    }
    
    //POST api/UserHome/ModifiedHome
    func ModifiedHome(_ homeDetails:DKHomeAddHouse,callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/UserHome/"+"ModifiedHome"
        let jsonEncoder = JSONEncoder()
        do
        {
            let jsonData = try jsonEncoder.encode(homeDetails)
            let json = String(data: jsonData, encoding: String.Encoding.utf8)
            Callwebservices(self.dwpi_tokenid,"POST", strUrl, json,callback)
        }
        catch {
            
        }
    }
    
    
    //GET api/UserHome/GetHomeDetailsByID?homeid={homeid}
    func GetHomeDetailsByID(_ homeid:String,callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/UserHome/"+"GetHomeDetailsByID?homeid="+homeid
        Callwebservices(self.dwpi_tokenid,"GET", strUrl, nil,callback)
    }
    
    //GET api/UserHome/GetAllHomeDetails
    func GetAllHomeDetails(callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/UserHome/"+"GetAllHomeDetails"
        Callwebservices(self.dwpi_tokenid,"GET", strUrl, nil){ (status, jsonData) in
            
            if(status)
            {
                let dKAddUserModel = try? newJSONDecoder().decode([DKAPIHomeResponseElement].self, from: jsonData as! Data)
                callback!(status,dKAddUserModel as AnyObject)
            }
            else
            {
                callback!(status,jsonData)
            }
        }
    }
    
    //Order
    //POST api/Order/PlaceOrder
    func OrderPlaceOrder(_ homeDetails:String,callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/Order/"+"PlaceOrder"
        do
        {
            Callwebservices(self.dwpi_tokenid,"POST", strUrl, homeDetails,callback)
        }
        catch {
            
        }
    }
    
    //POST api/Order/ChnageOrderStatus?orderid={orderid}&orderstatus={orderstatus}&changefrom={changefrom}&userid={userid}
   
    
    //GET api/Order/GetAllOrder
    func GetAllOrder(callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/Order/"+"GetAllOrder"
        Callwebservices(self.dwpi_tokenid,"GET", strUrl, nil,callback)
    }
    //GET api/Order/GetAllOrderByKitchenId?kitchenid={kitchenid}
    func GetAllOrderByKitchenId(_ kitchenid:String,callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/Order/"+"GetAllOrderByKitchenId?kitchenid="+kitchenid
        Callwebservices(self.dwpi_tokenid,"GET", strUrl, nil){(status,jsonData) in
            if(status)
            {
                do {
                    let json = try JSONSerialization.jsonObject(with: jsonData as! Data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [AnyObject]
                    var arra:[DKAPIOrderElement] = [DKAPIOrderElement]()
                    
                    if let datajson = json
                    {
                        for obj in datajson
                        {
                            let temp = DKAPIOrderElement.init(fromDictionary: obj as! [String : Any])
                            arra.append(temp)
                        }
                        callback!(status,arra as AnyObject)
                    } else {
                        callback!(false,"No data" as AnyObject)
                    }
                    
                }
                catch {
                    callback!(false,"No data" as AnyObject)
                }
            } else {
                callback!(false,"No data" as AnyObject)
            }
        }

    }
    //GET api/Order/GetAllOrderByUserId?userid={userid}
    func GetAllOrderByUserId(_ userid:String,callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/Order/"+"GetAllOrderByUserId?userid="+userid
        Callwebservices(self.dwpi_tokenid,"GET", strUrl, nil,callback)
    }
    
    //Cook
    //POST api/Cook/CreateCook
    func CreateCook(_ cookDetails:DKCookAddCook,callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/Cook/"+"CreateCook"
        let jsonEncoder = JSONEncoder()
        do
        {
            let jsonData = try jsonEncoder.encode(cookDetails)
            let json = String(data: jsonData, encoding: String.Encoding.utf8)
            Callwebservices(self.dwpi_tokenid,"POST", strUrl, json,callback)
        }
        catch {
            
        }
    }
    
    //POST api/Cook/Modifiedcook
    func Modifiedcook(_ cookDetails:DKCookAddCook,callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/Cook/"+"Modifiedcook"
        let jsonEncoder = JSONEncoder()
        do
        {
            let jsonData = try jsonEncoder.encode(cookDetails)
            let json = String(data: jsonData, encoding: String.Encoding.utf8)
            Callwebservices(self.dwpi_tokenid,"POST", strUrl, json,callback)
        }
        catch {
            
        }
    }
    
    //GET api/Cook/GetCookDetailsByCookID?cookid={cookid}
    func GetCookDetailsByCookID(_ cookid:String,callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/Cook/"+"GetCookDetailsByCookID?cookid="+cookid
        Callwebservices(self.dwpi_tokenid,"GET", strUrl, nil){ (status, jsonData) in
            
            if(status)
            {
                let backToString = String(data: jsonData as! Data, encoding: String.Encoding.utf8) as String!
               // print("\(backToString)")
                let user = try? newJSONDecoder().decode(DKAPIEditCookResponse.self, from: jsonData as! Data)
                if user != nil
                {
                    callback!(status,user as AnyObject)
                }
                else
                {
                    callback!(false,nil)
                }
                
            }
            else
            {
                callback!(status,jsonData)
            }
        }
    }
    
    //Cook/GetAllCookDetails
    func GetAllCookDetailsCook(callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/Cook/"+"GetAllCookDetails"
        Callwebservices(self.dwpi_tokenid,"GET", strUrl, nil) {(status,jsonData) in
            if(status)
            {
                do {
                    let json = try JSONSerialization.jsonObject(with: jsonData as! Data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [AnyObject]
                   var dKAddUserModel = [DKAPICookResponseElement]()
                    if let datajson = json
                    {
                        for obj in datajson
                    {
                        let temp = DKAPICookResponseElement.init(fromDictionary: obj as! [String : Any])
                        dKAddUserModel.append(temp)
                    }
                        
                    }
                    callback!(status,dKAddUserModel as AnyObject)
                }
                catch {
                    
                }
               
            }
            else
            {
                callback!(status,jsonData)
            }
        }
    }
    
    
    //Dashboard
    //GET api/Dashboard/GetDashBoardForAdmin
    func GetDashBoardForAdmin(callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/Dashboard/"+"GetDashBoardForAdmin"
        Callwebservices(self.dwpi_tokenid,"GET", strUrl, nil) {(status,jsonData) in
            if(status)
            {
                let dKAddUserModel = try? newJSONDecoder().decode(DKAPIAdminDashboradResponse.self, from: jsonData as! Data)
                callback!(status,dKAddUserModel as AnyObject)
            }
            else
            {
                callback!(status,jsonData)
            }
        }
    }
    //GET api/Dashboard/GetDashBoardForKitchen
    func GetDashBoardForKitchen(callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/Dashboard/"+"GetDashBoardForKitchen"
        Callwebservices(self.dwpi_tokenid,"GET", strUrl, nil) {(status,jsonData) in
            if(status)
            {
                let dKAddUserModel = try? newJSONDecoder().decode(DKAPIKitchenDashboradResponse.self, from: jsonData as! Data)
                callback!(status,dKAddUserModel as AnyObject)
            }
            else
            {
                callback!(status,jsonData)
            }
        }
    }
    
    //GET api/Menu/GetMenuDetailsByCategoryID?CategoryID={CategoryID}
    
    func GetMenuDetailsByCategoryID(_ categoryID:String = "",_ callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/Menu/"+"GetMenuDetailsByCategoryID?CategoryID="+categoryID
        Callwebservices(self.dwpi_tokenid,"GET", strUrl, nil) {(status,jsonData) in
            if(status)
            {
                do {
                    let json = try JSONSerialization.jsonObject(with: jsonData as! Data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [AnyObject]
                    var dKAddUserModel = [DKAPIGetMenuElement]()
                    var arra:[DKAPIOrderElement] = [DKAPIOrderElement]()
                    
                    if let datajson = json
                    {
                        for obj in datajson
                        {
                            let temp = DKAPIGetMenuElement.init(fromDictionary: obj as! [String : Any])
                            dKAddUserModel.append(temp)
                            
                            let objtemp = DKAPIOrderElement.init()
                            objtemp.menuItemID = temp.menuItemID
                            objtemp.kitchenID = "1"
                            objtemp.userID = "1"
                            objtemp.menuItemname = temp.menuItemName
                             objtemp.menuItemname_AR = temp.MenuItemName_ar
                            objtemp.orderQty = 0
                            objtemp.fullImagePath = temp.fullImagePath
                            objtemp.userComment = temp.menuItemDesc
                            objtemp.cookComment = temp.MenuItemDesc_ar
                            
                            arra.append(objtemp)
                        }
                        callback!(status,arra as AnyObject)
                    } else {
                        callback!(false,"No data" as AnyObject)
                    }
                   
                }
                catch {
                    callback!(false,"No data" as AnyObject)
                }
            }
            else
            {
                callback!(status,jsonData)
            }
        }
    }
    
    //POST api/Order/ChnageOrderStatus?orderid={orderid}&orderstatus={orderstatus}&changefrom={changefrom}&userid={userid}
    func ChnageOrderStatus(_ userid:String = "",_ orderid:String = "",_ orderstatus:String = "",_ changefrom:String = "1",_ cookcomment:String = "''",callback:WebCallbackHandler? = nil)
    {
        
        let strUrl = baseUrl+"api/Order/"+"ChnageOrderStatus?orderid="+orderid+"&orderstatus="+orderstatus.urlEncode+"&changefrom="+changefrom+"&userid="+userid+"&cookcomment="+cookcomment
            print("\(strUrl)")
         Callwebservices(self.dwpi_tokenid,"POST", strUrl, nil, callback)
    }
    
    func DeleteUserRoles(_ userid:String = "",_ isactive:Bool = false,callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/User/"+"DeleteUser?userid="+userid+"&isactive="+"\(isactive)"
        Callwebservices(self.dwpi_tokenid,"POST", strUrl, nil,callback)
    }
    
    func DeleteMenuRoles(_ menuid:String = "",_ isactive:Bool = false,callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/Menu/"+"DeleteMenu?menuid="+menuid+"&isactive="+"\(isactive)"
        Callwebservices(self.dwpi_tokenid,"POST", strUrl, nil,callback)
    }
    
    func DeleteHomeRoles(_ homeid:String = "",_ isactive:Bool = false,callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/UserHome/"+"DeleteHome?homeid="+homeid+"&isactive="+"\(isactive)"
        Callwebservices(self.dwpi_tokenid,"POST", strUrl, nil,callback)
    }
    
    func DeleteCookRoles(_ cookid:String = "",_ isactive:Bool = false,callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/Cook/"+"DeleteCook?cookid="+cookid+"&isactive="+"\(isactive)"
        Callwebservices(self.dwpi_tokenid,"POST", strUrl, nil,callback)
    }
    func DeleteCategory(_ categoryid:String = "",_ isactive:Bool = false,callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/Category/"+"DeleteCategory?catid="+categoryid+"&isactive="+"\(isactive)"
        Callwebservices(self.dwpi_tokenid,"POST", strUrl, nil,callback)
    }
    //GET api/Account/GetUserTypeByEmail?emailid={emailid}
    func GetUserTypeByEmail(_ emailid:String = "",_ callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/Account/"+"GetUserTypeByEmail?emailid="+emailid
        Callwebservices(nil,"GET", strUrl, nil) {(status,jsonData) in
            if(status)
            {
                callback!(status,jsonData as AnyObject)
            }
            else
            {
                callback!(status,jsonData)
            }
        }
    }
    
    func GetAllCookActiveInactiveDetails(callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/Cook/"+"GetAllCookActiveInactiveDetails"
        Callwebservices(self.dwpi_tokenid,"GET", strUrl, nil) {(status,jsonData) in
            if(status)
            {
                do {
                    let json = try JSONSerialization.jsonObject(with: jsonData as! Data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [AnyObject]
                    var dKAddUserModel = [DKAPICookResponseElement]()
                    if let datajson = json
                    {
                        for obj in datajson
                        {
                            let temp = DKAPICookResponseElement.init(fromDictionary: obj as! [String : Any])
                            dKAddUserModel.append(temp)
                        }
                        
                    }
                    callback!(status,dKAddUserModel as AnyObject)
                }
                catch {
                    
                }
                
            }
            else
            {
                callback!(status,jsonData)
            }
        }
    }
    
    //GetAllActiveInactiveMenuDetails
    
    func GetAllActiveInactiveMenuDetails(callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/Menu/"+"GetAllActiveInactiveMenuDetails"
        Callwebservices(self.dwpi_tokenid,"GET", strUrl, nil){(status,jsonData) in
            if(status)
            {
                do {
                    let json = try JSONSerialization.jsonObject(with: jsonData as! Data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [AnyObject]
                    
                    var arra:[DKAPIMenuResponseElement] = [DKAPIMenuResponseElement]()
                    
                    if let datajson = json
                    {
                        for obj in datajson
                        {
                            let temp = DKAPIMenuResponseElement.init(fromDictionary: obj as! [String : Any])
                            arra.append(temp)
                            
                        }
                        callback!(status,arra as AnyObject)
                    } else {
                        callback!(false,"No data" as AnyObject)
                    }
                    
                }
                catch {
                    callback!(false,"No data" as AnyObject)
                }
            }
            else
            {
                callback!(status,jsonData)
            }
        }
    }
    
    func GetAllActiveInActiveHomeDetails(callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/UserHome/"+"GetAllActiveInActiveHomeDetails"
        Callwebservices(self.dwpi_tokenid,"GET", strUrl, nil){ (status, jsonData) in
            
            if(status)
            {
                let dKAddUserModel = try? newJSONDecoder().decode([DKAPIHomeResponseElement].self, from: jsonData as! Data)
                callback!(status,dKAddUserModel as AnyObject)
            }
            else
            {
                callback!(status,jsonData)
            }
        }
    }
    
    //api/Account/UpdateDeviceID?userid={userid}&devicekey={devicekey}
    
    func UpdateDeviceID(_ devicekey:String = "",_ lang:String = "E",callback:WebCallbackHandler? = nil)
    {
        let userid = self.logoinUserid
        let strUrl = baseUrl+"api/Account/UpdateDeviceID?userid="+userid+"&devicekey="+devicekey+"&lang="+lang  //
        Callwebservices(self.dwpi_tokenid,"POST", strUrl, nil,callback)
    }
    
    //GET api/Order/GetAllOrder
    //http://testdubaicookapi.diquery.com/api/Order/GetAllOrderDetailsByOrderID?orderid=1
    func GetAllOrderByOrderId(_ orderID:String = "",callback:WebCallbackHandler? = nil)
    {
        let strUrl = baseUrl+"api/Order/"+"GetAllOrderDetailsByOrderID?orderid="+orderID
        Callwebservices(self.dwpi_tokenid,"GET", strUrl, nil){ status,jsonData in
            
            if(status)
            {
                do {
                    let json = try JSONSerialization.jsonObject(with: jsonData as! Data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [AnyObject]
                    var arra:[DKAPIOrderElement] = [DKAPIOrderElement]()
                    
                    if let datajson = json
                    {
                        for obj in datajson
                        {
                            let temp = DKAPIOrderElement.init(fromDictionary: obj as! [String : Any])
                            arra.append(temp)
                        }
                        callback!(status,arra as AnyObject)
                    } else {
                        callback!(false,"No data" as AnyObject)
                    }
                    
                }
                catch {
                    callback!(false,"No data" as AnyObject)
                }
            } else {
                callback!(false,"No data" as AnyObject)
            }
        }
    }
    
    
    func GetAllUnreadComments(callback:WebCallbackHandler? = nil)
    {
        let userid = self.logoinUserid
        let strUrl = baseUrl+"api/Order/"+"GetAllUnreadComments?userid="+userid
        Callwebservices(self.dwpi_tokenid,"GET", strUrl, nil){ status,jsonData in
            
            if(status)
            {
                do {
                    let json = try JSONSerialization.jsonObject(with: jsonData as! Data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [AnyObject]
                    var arra:[DKAPIOrderElement] = [DKAPIOrderElement]()
                    
                    if let datajson = json
                    {
                        for obj in datajson
                        {
                            let temp = DKAPIOrderElement.init(fromDictionary: obj as! [String : Any])
                            arra.append(temp)
                        }
                        callback!(status,arra as AnyObject)
                    } else {
                        callback!(false,"No data" as AnyObject)
                    }
                    
                }
                catch {
                    callback!(false,"No data" as AnyObject)
                }
            } else {
                callback!(false,"No data" as AnyObject)
            }
        }
    }
    
    //api/Order/UpdateUnreadMessage?UserID={UserID}&OrderID={OrderID}
    
    func UpdateUnreadMessage(_ userid:String = "",_ orderid:String = "",callback:WebCallbackHandler? = nil)
    {
        let userid = self.logoinUserid
        let strUrl = baseUrl+"api/Order/"+"UpdateUnreadMessage?OrderID="+orderid+"&UserID="+userid
        print("\(strUrl)")
        Callwebservices(self.dwpi_tokenid,"POST", strUrl, nil, callback)
    }
    
}

extension Dictionary {
    
    var json: String {
        let invalidJson = "Not a valid JSON"
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
            return String(bytes: jsonData, encoding: String.Encoding.utf8) ?? invalidJson
        } catch {
            return invalidJson
        }
    }
    
    func printJson() {
        print(json)
    }
    
}

extension String
{
    func stringByAddingPercentEncodingForRFC3986() -> String {
        let unreserved = "-._~/?"
        let allowed = NSMutableCharacterSet.alphanumeric()
        allowed.addCharacters(in: unreserved)
        return self.addingPercentEncoding(withAllowedCharacters: allowed as CharacterSet) ?? self
    }
    
    var urlEncode:String
    {
        let unreserved = "-._~/?"
        let allowed = NSMutableCharacterSet.alphanumeric()
        allowed.addCharacters(in: unreserved)
        return self.addingPercentEncoding(withAllowedCharacters: allowed as CharacterSet) ?? self
    }
}


// To parse the JSON, add this file to your project and do:
//
//   let dKAddUserModel = try? newJSONDecoder().decode(DKAddUserModel.self, from: jsonData)

public class DKAddUserModel: Codable
{
    var userID: String
    var userName: String
    var gender: String
    var emailID: String
    var phoneNo: String
     var password: String
    var isActive: Bool
    var homeDetails: DKUserHomeDetails
    var tranID: Int
    var tokenID: JSONNull?
    var lastDate: JSONNull?
    var statusCode: Int
    var versionNo: JSONNull?
    var successMessage: String
    var errorMessage: String
    var userGroup: String
    
    enum CodingKeys: String, CodingKey {
        case userID = "UserID"
        case userName = "UserName"
        case gender = "Gender"
        case emailID = "EmailID"
        case phoneNo = "PhoneNo"
        case password = "Password"
        case isActive = "IsActive"
        case homeDetails = "HomeDetails"
        case tranID = "TranID"
        case tokenID = "TokenID"
        case lastDate = "LastDate"
        case statusCode = "StatusCode"
        case versionNo = "VersionNo"
        case successMessage = "SuccessMessage"
        case errorMessage = "ErrorMessage"
        case userGroup = "UserGroup"
        
    }
    
    init()
    {
        self.userID = ""
        self.userName =         ""
        self.gender =           ""
        self.emailID =          ""
        self.phoneNo =          ""
        self.password =         ""
        self.isActive =         false
        self.homeDetails =      DKUserHomeDetails.init()
        self.tranID =           0
        self.tokenID =          nil
        self.lastDate =         nil
        self.statusCode =       0
        self.versionNo =        nil
        self.successMessage =   ""
        self.errorMessage =     ""
        self.userGroup = ""
    }
    
//    init(userID: String, userName: String, gender: String, emailID: String, phoneNo: String, password: String, isActive: Bool, homeDetails: DKUserHomeDetails, tranID: Int, tokenID: String, lastDate: String, statusCode: Int, versionNo: String, successMessage: [String], errorMessage: [String]) {
//        self.userID = userID
//        self.userName = userName
//        self.gender = gender
//        self.emailID = emailID
//        self.phoneNo = phoneNo
//        self.password = password
//        self.isActive = isActive
//        self.homeDetails = homeDetails
//        self.tranID = tranID
//        self.tokenID = tokenID
//        self.lastDate = lastDate
//        self.statusCode = statusCode
//        self.versionNo = versionNo
//        self.successMessage = successMessage
//        self.errorMessage = errorMessage
//    }
    
    init(userID: String, userName: String, gender: String, emailID: String, phoneNo: String, password: String, isActive: Bool, homeDetails: DKUserHomeDetails, tranID: Int, tokenID: JSONNull?, lastDate: JSONNull?, statusCode: Int, versionNo: JSONNull?, successMessage: String, errorMessage: String,userGroup: String) {
        self.userID = userID
        self.userName = userName
        self.gender = gender
        self.emailID = emailID
        self.phoneNo = phoneNo
        self.password = password
        self.isActive = isActive
        self.homeDetails = homeDetails
        self.tranID = tranID
        self.tokenID = tokenID
        self.lastDate = lastDate
        self.statusCode = statusCode
        self.versionNo = versionNo
        self.successMessage = successMessage
        self.errorMessage = errorMessage
        self.userGroup = userGroup
    }
}

class DKUserHomeDetails: Codable {
    var homeID: String
    var homeTitle: String
    var homeDesc: String
    var isActive: Bool
    var tranID: Int
    var tokenID: JSONNull?
    var lastDate: JSONNull?
    var statusCode: Int
    var versionNo: JSONNull?
    var successMessage: String
    var errorMessage: String
    var homeTitle_ar : String
    var homeDesc_ar: String
    var location_ar: String
    var location: String
    
    enum CodingKeys: String, CodingKey {
        case homeID = "HomeID"
        case homeTitle = "HomeTitle"
        case homeDesc = "HomeDesc"
        case isActive = "IsActive"
        case tranID = "TranID"
        case tokenID = "TokenID"
        case lastDate = "LastDate"
        case statusCode = "StatusCode"
        case versionNo = "VersionNo"
        case successMessage = "SuccessMessage"
        case homeTitle_ar = "HomeTitle_ar"
        case errorMessage = "ErrorMessage"
        case homeDesc_ar = "HomeDesc_ar"
        case location_ar = "Location_ar"
        case location = "Location"
    }
    init() {
        self.homeID =       ""
        self.homeTitle =    ""
        self.homeDesc =     ""
        self.isActive =     true
        self.tranID =       0
        self.tokenID =      nil
        self.lastDate =     nil
        self.statusCode =   0
        self.versionNo =    nil
        self.successMessage = ""
        self.errorMessage = ""
        self.homeDesc_ar = ""
        self.homeTitle_ar = ""
        self.location_ar = ""
        self.location = ""
    }
    
//    init(homeID: String, homeTitle: String, homeDesc: String, isActive: Bool, tranID: Int, tokenID: String, lastDate: String, statusCode: Int, versionNo: String, successMessage: [String], errorMessage: [String]) {
//        self.homeID = homeID
//        self.homeTitle = homeTitle
//        self.homeDesc = homeDesc
//        self.isActive = isActive
//        self.tranID = tranID
//        self.tokenID = tokenID
//        self.lastDate = lastDate
//        self.statusCode = statusCode
//        self.versionNo = versionNo
//        self.successMessage = successMessage
//        self.errorMessage = errorMessage
//    }
    
    init(homeID: String, homeTitle: String, homeDesc: String, isActive: Bool, tranID: Int, tokenID: JSONNull?, lastDate: JSONNull?, statusCode: Int, versionNo: JSONNull?, successMessage: String, errorMessage: String ,homeTitle_ar: String,homeDesc_ar: String,location_ar: String,location: String) {
        self.homeID = homeID
        self.homeTitle = homeTitle
        self.homeDesc = homeDesc
        self.isActive = isActive
        self.tranID = tranID
        self.tokenID = tokenID
        self.lastDate = lastDate
        self.statusCode = statusCode
        self.versionNo = versionNo
        self.successMessage = successMessage
        self.errorMessage = errorMessage
        self.homeTitle_ar = homeTitle_ar
        self.homeDesc_ar = homeDesc_ar
        self.location_ar = location_ar
         self.location = location
    }
}



// To parse the JSON, add this file to your project and do:
//
//   let dKMenuAddMenuModel = try? newJSONDecoder().decode(DKMenuAddMenuModel.self, from: jsonData)


class DKMenuAddMenuModel: Codable {
   var menuItemID: String
   var menuItemName: String
   var menuItemDesc: String
    var menuItemName_AR: String
    var menuItemDesc_AR: String
   var isActive: Bool
   var imageFileName: String
   var imageExtension: String
   var image: String
   var catDetails: DKMenuCatDetails
   var tranID: Int
   var tokenID: String
   var lastDate: String
   var statusCode: Int
   var versionNo: String
   var successMessage: [String]
   var errorMessage: [String]
    
    enum CodingKeys: String, CodingKey {
        case menuItemID = "MenuItemID"
        case menuItemName = "MenuItemName"
        case menuItemDesc = "MenuItemDesc"
        case menuItemName_AR = "MenuItemName_ar"
        case menuItemDesc_AR = "MenuItemDesc_ar"
        case isActive = "IsActive"
        case imageFileName = "ImageFileName"
        case imageExtension = "ImageExtension"
        case image = "Image"
        case catDetails = "CatDetails"
        case tranID = "TranID"
        case tokenID = "TokenID"
        case lastDate = "LastDate"
        case statusCode = "StatusCode"
        case versionNo = "VersionNo"
        case successMessage = "SuccessMessage"
        case errorMessage = "ErrorMessage"
    }
    
    init() {
        
        self.menuItemID             = ""
        self.menuItemName           = ""
        self.menuItemDesc           = ""
        self.menuItemName_AR           = ""
        self.menuItemDesc_AR           = ""
        self.isActive               = false
        self.imageFileName          = ""
        self.imageExtension         = ""
        self.image                  = ""
        self.catDetails             = DKMenuCatDetails.init()
        self.tranID                 = 0
        self.tokenID                = ""
        self.lastDate               = ""
        self.statusCode             = 0
        self.versionNo              = ""
        self.successMessage         = [String]()
        self.errorMessage           = [String]()
    }
    
    init(menuItemID: String, menuItemName: String, menuItemDesc: String, menuItemName_AR: String, menuItemDesc_AR: String, isActive: Bool, imageFileName: String, imageExtension: String, image: String, catDetails: DKMenuCatDetails, tranID: Int, tokenID: String, lastDate: String, statusCode: Int, versionNo: String, successMessage: [String], errorMessage: [String]) {
        self.menuItemID = menuItemID
        self.menuItemName = menuItemName
        self.menuItemDesc = menuItemDesc
        self.menuItemName_AR = menuItemName_AR
        self.menuItemDesc_AR = menuItemDesc_AR
        self.isActive = isActive
        self.imageFileName = imageFileName
        self.imageExtension = imageExtension
        self.image = image
        self.catDetails = catDetails
        self.tranID = tranID
        self.tokenID = tokenID
        self.lastDate = lastDate
        self.statusCode = statusCode
        self.versionNo = versionNo
        self.successMessage = successMessage
        self.errorMessage = errorMessage
    }
}

class DKMenuCatDetails: Codable {
    var catID: String
    var catName: String
    var catDesc: String
    var isActive: Bool
    var tranID: Int
    var tokenID: String
    var lastDate: String
    var statusCode: Int
    var versionNo: String
    var successMessage: [String]
    var errorMessage: [String]
    
    enum CodingKeys: String, CodingKey {
        case catID = "CatID"
        case catName = "CatName"
        case catDesc = "CatDesc"
        case isActive = "IsActive"
        case tranID = "TranID"
        case tokenID = "TokenID"
        case lastDate = "LastDate"
        case statusCode = "StatusCode"
        case versionNo = "VersionNo"
        case successMessage = "SuccessMessage"
        case errorMessage = "ErrorMessage"
    }
    
    init() {
        
        self.catID          = ""
        self.catName        = ""
        self.catDesc        = ""
        self.isActive       = false
        self.tranID         = 0
        self.tokenID        = ""
        self.lastDate       = ""
        self.statusCode     = 0
        self.versionNo      = ""
        self.successMessage = [String]()
        self.errorMessage   = [String]()
    }
    
    init(catID: String, catName: String, catDesc: String, isActive: Bool, tranID: Int, tokenID: String, lastDate: String, statusCode: Int, versionNo: String, successMessage: [String], errorMessage: [String]) {
        self.catID = catID
        self.catName = catName
        self.catDesc = catDesc
        self.isActive = isActive
        self.tranID = tranID
        self.tokenID = tokenID
        self.lastDate = lastDate
        self.statusCode = statusCode
        self.versionNo = versionNo
        self.successMessage = successMessage
        self.errorMessage = errorMessage
    }
}



// To parse the JSON, add this file to your project and do:
//
//   let dKCatagoryAddCreate = try? newJSONDecoder().decode(DKCatagoryAddCreate.self, from: jsonData)


class DKCatagoryAddCreate: Codable {
    let catID: String
    var catName: String
    var catName_AR: String
    var catDesc: String
    var catDesc_AR: String
    var catImage: String
    var catImageExt: String
    var catImageName: String
    var catImageFullPath:String
    var isActive: Bool
    let tranID: Int
    let tokenID: String
    let lastDate: String
    let statusCode: Int
    let versionNo: String
    let successMessage: [String]
    let errorMessage: [String]
    
    enum CodingKeys: String, CodingKey {
        case catID = "CatID"
        case catName = "CatName"
        case catName_AR = "CatName_ar"
        case catDesc = "CatDesc"
        case catDesc_AR = "CatDesc_ar"
        case isActive = "IsActive"
        case tranID = "TranID"
        case tokenID = "TokenID"
        case lastDate = "LastDate"
        case statusCode = "StatusCode"
        case versionNo = "VersionNo"
        case successMessage = "SuccessMessage"
        case errorMessage = "ErrorMessage"
         case catImage = "CatImage"
         case catImageExt = "CatImageExtension"
         case catImageName = "CatImageFileName"
         case catImageFullPath = "CatFullImagePath"
    }
    
    init()
    {
        self.catID          = ""
        self.catName        = ""
        self.catName_AR        = ""
        self.catDesc        = ""
        self.catDesc_AR        = ""
        self.tokenID        = ""
        self.lastDate       = ""
        self.statusCode     = 0
        self.versionNo      = ""
        self.successMessage = [String]()
        self.errorMessage   = [String]()
        self.isActive       = false
        self.tranID         = 0
        self.catImage      = ""
        self.catImageExt      = ""
        self.catImageName      = ""
        self.catImageFullPath      = ""
    }
    
    init(catID: String, catName: String, catName_AR: String, catDesc: String, catDesc_AR: String, isActive: Bool, tranID: Int, tokenID: String, lastDate: String, statusCode: Int, versionNo: String, successMessage: [String], errorMessage: [String], catImage: String, catImageExt: String, catImageName: String, catImageFullPath: String) {
        self.catID = catID
        self.catName = catName
        self.catName_AR = catName_AR
        self.catDesc = catDesc
        self.catDesc_AR = catDesc_AR
        self.tokenID = tokenID
        self.lastDate = lastDate
        self.statusCode = statusCode
        self.versionNo = versionNo
        self.successMessage = successMessage
        self.errorMessage = errorMessage
        self.isActive = isActive
        self.tranID = tranID
        self.catImage      = catImage
        self.catImageExt      = catImageExt
        self.catImageName      = catImageName
        self.catImageFullPath      = catImageFullPath
        
    }
}


// To parse the JSON, add this file to your project and do:
//
//   let dKHomeAddHouse = try? newJSONDecoder().decode(DKHomeAddHouse.self, from: jsonData)



class DKHomeAddHouse: Codable {
    var homeID: String
    var homeTitle: String
    var homeDesc: String
    var homeTitle_AR: String
    var homeDesc_AR: String
    var isActive: Bool
    var tranID: Int
    var tokenID: String
    var lastDate: String
    var statusCode: Int
    var versionNo: String
    var location: String
    var location_ar: String
    var successMessage: [String]
    var errorMessage: [String]
    
    enum CodingKeys: String, CodingKey {
        case homeID = "HomeID"
        case homeTitle = "HomeTitle"
        case homeDesc = "HomeDesc"
        case homeTitle_AR = "HomeTitle_ar"
        case homeDesc_AR = "HomeDesc_ar"
        case isActive = "IsActive"
        case tranID = "TranID"
        case tokenID = "TokenID"
        case lastDate = "LastDate"
        case statusCode = "StatusCode"
        case versionNo = "VersionNo"
        case successMessage = "SuccessMessage"
        case errorMessage = "ErrorMessage"
        case location = "Location"
        case location_ar = "Location_ar"
        
    }
    
    init() {
        self.homeID             = ""
        self.homeTitle          = ""
        self.homeDesc           = ""
        self.homeTitle_AR          = ""
        self.homeDesc_AR           = ""
        self.isActive           = false
        self.tranID             = 0
        self.tokenID            = ""
        self.lastDate           = ""
        self.statusCode         = 0
        self.versionNo          = ""
        self.successMessage     = [String]()
        self.errorMessage       = [String]()
        self.location             = ""
        self.location_ar             = ""
    }
    init(homeID: String, homeTitle: String, homeDesc: String, isActive: Bool, homeTitle_AR: String, homeDesc_AR: String, tranID: Int, tokenID: String, lastDate: String, statusCode: Int, versionNo: String, successMessage: [String], errorMessage: [String],location: String,location_ar: String) {
        self.homeID = homeID
        self.homeTitle = homeTitle
        self.homeDesc = homeDesc
        self.homeTitle_AR = homeTitle_AR
        self.homeDesc_AR = homeDesc_AR
        self.isActive = isActive
        self.tranID = tranID
        self.tokenID = tokenID
        self.lastDate = lastDate
        self.statusCode = statusCode
        self.versionNo = versionNo
        self.successMessage = successMessage
        self.errorMessage = errorMessage
        self.location = location
        self.location_ar = location_ar
    }
}

// To parse the JSON, add this file to your project and do:
//
//   let dKCookAddCook = try? newJSONDecoder().decode(DKCookAddCook.self, from: jsonData)


class DKCookAddCook: Codable {
    var cookID: String
    var cookName: String
    var cookDesc: String
    var cookName_AR: String
    var cookDesc_AR: String
    var isActive: Bool
    var kitchenDetails: DKCookKitchenDetails
    var tranID: Int
    var tokenID: String
    var lastDate: String
    var statusCode: Int
    var versionNo: String
    var successMessage: [String]
    var errorMessage: [String]
    var emailID:String
    var phoneNo:String
    var password:String
    var gender:String
    
    enum CodingKeys: String, CodingKey {
        case cookID = "CookID"
        case cookName = "CookName"
        case cookDesc = "CookDesc"
        case cookName_AR = "CookName_ar"
        case cookDesc_AR = "CookDesc_ar"
        case isActive = "IsActive"
        case kitchenDetails = "KitchenDetails"
        case tranID = "TranID"
        case tokenID = "TokenID"
        case lastDate = "LastDate"
        case statusCode = "StatusCode"
        case versionNo = "VersionNo"
        case successMessage = "SuccessMessage"
        case errorMessage = "ErrorMessage"
        case emailID = "EmailID"
        case password = "Password"
        case phoneNo = "PhoneNo"
        case gender = "Gender"
    }
    
    init() {
     
        self.cookID         = ""
        self.cookName       = ""
        self.cookDesc       = ""
        self.cookName_AR       = ""
        self.cookDesc_AR       = ""
        
        self.versionNo      = ""
        self.tokenID        = ""
        self.lastDate       = ""
        
        self.kitchenDetails = DKCookKitchenDetails.init()
        self.tranID = 0
        self.statusCode = 0
        self.successMessage = [String]()
        self.errorMessage = [String]()
        self.isActive       = false
        self.emailID         = ""
        self.password       = ""
        self.phoneNo       = ""
        self.gender      = ""
    }
    
    init(cookID: String, cookName: String, cookDesc: String, cookName_AR: String, cookDesc_AR: String, isActive: Bool, kitchenDetails: DKCookKitchenDetails, tranID: Int, tokenID: String, lastDate: String, statusCode: Int, versionNo: String, successMessage: [String], errorMessage: [String]) {
        self.cookID = cookID
        self.cookName = cookName
        self.cookDesc = cookDesc
        self.cookName_AR = cookName
        self.cookDesc_AR = cookDesc
        self.isActive = isActive
        self.kitchenDetails = kitchenDetails
        self.tranID = tranID
        self.tokenID = tokenID
        self.lastDate = lastDate
        self.statusCode = statusCode
        self.versionNo = versionNo
        self.successMessage = successMessage
        self.errorMessage = errorMessage
        self.emailID         = ""
        self.password       = ""
        self.phoneNo       = ""
        self.gender      = ""
    }
}

class DKCookKitchenDetails: Codable {
    var kitchenID: String
    var kitchenTitle: String
    var kitchenDesc: String
    var password: String
    var isActive: Bool
    var tranID: Int
    var tokenID: String
    var lastDate: String
    var statusCode: Int
    var versionNo: String
    var successMessage: [String]
    var errorMessage: [String]
    
    enum CodingKeys: String, CodingKey {
        case kitchenID = "KitchenID"
        case kitchenTitle = "KitchenTitle"
        case kitchenDesc = "KitchenDesc"
        case password = "Password"
        case isActive = "IsActive"
        case tranID = "TranID"
        case tokenID = "TokenID"
        case lastDate = "LastDate"
        case statusCode = "StatusCode"
        case versionNo = "VersionNo"
        case successMessage = "SuccessMessage"
        case errorMessage = "ErrorMessage"
    }
    
    init() {
        self.kitchenID          = ""
        self.kitchenTitle       = ""
        self.kitchenDesc        = ""
        self.password           = ""
        self.isActive           = false
        self.tranID             = 0
        self.tokenID            = ""
        self.lastDate           = ""
        self.statusCode         = 0
        self.versionNo          = ""
        self.successMessage     = [String]()
        self.errorMessage       = [String]()
    }
    
    init(kitchenID: String, kitchenTitle: String, kitchenDesc: String, password: String, isActive: Bool, tranID: Int, tokenID: String, lastDate: String, statusCode: Int, versionNo: String, successMessage: [String], errorMessage: [String]) {
        self.kitchenID = kitchenID
        self.kitchenTitle = kitchenTitle
        self.kitchenDesc = kitchenDesc
        self.password = password
        self.isActive = isActive
        self.tranID = tranID
        self.tokenID = tokenID
        self.lastDate = lastDate
        self.statusCode = statusCode
        self.versionNo = versionNo
        self.successMessage = successMessage
        self.errorMessage = errorMessage
    }
}

// To parse the JSON, add this file to your project and do:
//
//   let dKKitchenAdd = try? newJSONDecoder().decode(DKKitchenAdd.self, from: jsonData)


class DKKitchenAdd: Codable {
   var kitchenID: String
   var kitchenTitle: String
   var kitchenDesc: String
    var kitchenTitle_AR: String
    var kitchenDesc_AR: String
   var password: String
   var isActive: Bool
   var tranID: Int
   var tokenID: String
   var lastDate: String
   var statusCode: Int
   var versionNo: String
   var successMessage: [String]
   var errorMessage: [String]
    
    enum CodingKeys: String, CodingKey {
        case kitchenID = "KitchenID"
        case kitchenTitle = "KitchenTitle"
        case kitchenDesc = "KitchenDesc"
        case kitchenTitle_AR = "KitchenTitle_ar"
        case kitchenDesc_AR = "KitchenDesc_ar"
        case password = "Password"
        case isActive = "IsActive"
        case tranID = "TranID"
        case tokenID = "TokenID"
        case lastDate = "LastDate"
        case statusCode = "StatusCode"
        case versionNo = "VersionNo"
        case successMessage = "SuccessMessage"
        case errorMessage = "ErrorMessage"
    }
    
    init() {
        
        self.kitchenID          = ""
        self.kitchenTitle       = ""
        self.kitchenDesc        = ""
        self.kitchenTitle_AR       = ""
        self.kitchenDesc_AR        = ""
        self.password           = ""
        self.versionNo          = ""
        self.tokenID            = ""
        self.lastDate           = ""
        self.isActive           = false
        self.tranID             = 0
        self.statusCode         = 0
        self.successMessage     = [String]()
        self.errorMessage       = [String]()
    }
    
    init(kitchenID: String, kitchenTitle: String, kitchenDesc: String, kitchenTitle_AR: String, kitchenDesc_AR: String, password: String, isActive: Bool, tranID: Int, tokenID: String, lastDate: String, statusCode: Int, versionNo: String, successMessage: [String], errorMessage: [String]) {
        self.kitchenID = kitchenID
        self.kitchenTitle = kitchenTitle
        self.kitchenDesc = kitchenDesc
        self.kitchenTitle_AR = kitchenTitle_AR
        self.kitchenDesc_AR = kitchenDesc_AR
        self.password = password
        self.isActive = isActive
        self.tranID = tranID
        self.tokenID = tokenID
        self.lastDate = lastDate
        self.statusCode = statusCode
        self.versionNo = versionNo
        self.successMessage = successMessage
        self.errorMessage = errorMessage
    }
}

class JSONNull: Codable, Hashable {
    
    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }
    
    public var hashValue: Int {
        return 0
    }
    
    public init() {}
    
    public required init(from decoder: Decoder) throws
    {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil()
        {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}


fileprivate func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

fileprivate func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}

// MARK: - URLSession response handlers

extension URLSession {
    fileprivate func codableTask<T: Codable>(with url: URL, completionHandler: @escaping (T?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else {
                completionHandler(nil, response, error)
                return
            }
            completionHandler(try? newJSONDecoder().decode(T.self, from: data), response, nil)
        }
    }
    
    func dKAddUserModelTask(with url: URL, completionHandler: @escaping (DKAddUserModel?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.codableTask(with: url, completionHandler: completionHandler)
    }
}


typealias DKAPIUserResponse = [DKAPIUserResponseElement]

class DKAPIUserResponseElement: Codable {
    let userID: String
    let userName: String
    let gender: String
    let emailID: String
    let phoneNo: String
    let password: String?
    let isActive: Bool
    let homeDetails: DKAPIHomeDetails
    let tranID: Int
    let tokenID: JSONNull?
    let lastDate: JSONNull?
    let statusCode: Int
    let versionNo: JSONNull?
    let successMessage: JSONNull?
    let errorMessage: JSONNull?
    let userGroup:String
    
    enum CodingKeys: String, CodingKey {
        case userID = "UserID"
        case userName = "UserName"
        case gender = "Gender"
        case emailID = "EmailID"
        case phoneNo = "PhoneNo"
        case password = "Password"
        case isActive = "IsActive"
        case homeDetails = "HomeDetails"
        case tranID = "TranID"
        case tokenID = "TokenID"
        case lastDate = "LastDate"
        case statusCode = "StatusCode"
        case versionNo = "VersionNo"
        case successMessage = "SuccessMessage"
        case errorMessage = "ErrorMessage"
        case userGroup = "UserGroup"
    }
    
    init(userID: String, userName: String, gender: String, emailID: String, phoneNo: String, password: String?, isActive: Bool, homeDetails: DKAPIHomeDetails, tranID: Int, tokenID: JSONNull?, lastDate: JSONNull?, statusCode: Int, versionNo: JSONNull?, successMessage: JSONNull?, errorMessage: JSONNull?,userGroup:String) {
        self.userID = userID
        self.userName = userName
        self.gender = gender
        self.emailID = emailID
        self.phoneNo = phoneNo
        self.password = password
        self.isActive = isActive
        self.homeDetails = homeDetails
        self.tranID = tranID
        self.tokenID = tokenID
        self.lastDate = lastDate
        self.statusCode = statusCode
        self.versionNo = versionNo
        self.successMessage = successMessage
        self.errorMessage = errorMessage
        self.userGroup = userGroup
    }
}

class DKAPIEditUserResponse: Codable {
    let userID: String
    var userName: String
    var gender: String
    var emailID: String
    var phoneNo: String
    var userGroup: String
    var password: String
    var isActive: Bool
    let homeDetails: DKAPIHomeDetails
    let tranID: Int
    let tokenID: JSONNull?
    let lastDate: JSONNull?
    let statusCode: Int
    let versionNo: JSONNull?
    let successMessage: JSONNull?
    let errorMessage: JSONNull?
    
    
    enum CodingKeys: String, CodingKey {
        case userID = "UserID"
        case userName = "UserName"
        case gender = "Gender"
        case emailID = "EmailID"
        case phoneNo = "PhoneNo"
        case password = "Password"
        case isActive = "IsActive"
        case homeDetails = "HomeDetails"
        case tranID = "TranID"
        case tokenID = "TokenID"
        case lastDate = "LastDate"
        case statusCode = "StatusCode"
        case versionNo = "VersionNo"
        case successMessage = "SuccessMessage"
        case errorMessage = "ErrorMessage"
        case userGroup  = "UserGroup"
    }
    
    init(userID: String, userName: String, gender: String, emailID: String, phoneNo: String, password: String, isActive: Bool, homeDetails: DKAPIHomeDetails, tranID: Int, tokenID: JSONNull?, lastDate: JSONNull?, statusCode: Int, versionNo: JSONNull?, successMessage: JSONNull?, errorMessage: JSONNull?,userGroup:String) {
        self.userID = userID
        self.userName = userName
        self.gender = gender
        self.emailID = emailID
        self.phoneNo = phoneNo
        self.password = password
        self.isActive = isActive
        self.homeDetails = homeDetails
        self.tranID = tranID
        self.tokenID = tokenID
        self.lastDate = lastDate
        self.statusCode = statusCode
        self.versionNo = versionNo
        self.successMessage = successMessage
        self.errorMessage = errorMessage
        self.userGroup = userGroup
    }
}

class DKAPIEditCookResponse: Codable {
    var cookID, cookName, cookDesc,cookName_AR, cookDesc_AR, gender: String?
    var emailID, phoneNo, password: String?
    var isActive: Bool?
    let kitchenDetails: KitchenDetails?
    let tranID: Int?
    let tokenID, lastDate: JSONNull?
    let statusCode: Int?
    let versionNo, successMessage, errorMessage: JSONNull?
    let successDate: String?
    
    enum CodingKeys: String, CodingKey {
        case cookID = "CookID"
        case cookName = "CookName"
        case cookDesc = "CookDesc"
        case cookName_AR = "CookName_ar"
        case cookDesc_AR = "CookDesc_ar"
        case gender = "Gender"
        case emailID = "EmailID"
        case phoneNo = "PhoneNo"
        case password = "Password"
        case isActive = "IsActive"
        case kitchenDetails = "KitchenDetails"
        case tranID = "TranID"
        case tokenID = "TokenID"
        case lastDate = "LastDate"
        case statusCode = "StatusCode"
        case versionNo = "VersionNo"
        case successMessage = "SuccessMessage"
        case errorMessage = "ErrorMessage"
        case successDate = "SuccessDate"
    }
    
    init(cookID: String?, cookName: String?, cookDesc: String?, cookName_AR: String?, cookDesc_AR: String?, gender: String?, emailID: String?, phoneNo: String?, password: String?, isActive: Bool?, kitchenDetails: KitchenDetails?, tranID: Int?, tokenID: JSONNull?, lastDate: JSONNull?, statusCode: Int?, versionNo: JSONNull?, successMessage: JSONNull?, errorMessage: JSONNull?, successDate: String?) {
        self.cookID = cookID
        self.cookName = cookName
        self.cookDesc = cookDesc
        self.cookName_AR = cookName_AR
        self.cookDesc_AR = cookDesc_AR
        self.gender = gender
        self.emailID = emailID
        self.phoneNo = phoneNo
        self.password = password
        self.isActive = isActive
        self.kitchenDetails = kitchenDetails
        self.tranID = tranID
        self.tokenID = tokenID
        self.lastDate = lastDate
        self.statusCode = statusCode
        self.versionNo = versionNo
        self.successMessage = successMessage
        self.errorMessage = errorMessage
        self.successDate = successDate
    }
}
class KitchenDetails: Codable {
    let kitchenID, kitchenTitle, kitchenDesc: String?
    let password, emailID: JSONNull?
    let isActive: Bool?
    let tranID: Int?
    let tokenID, lastDate: JSONNull?
    let statusCode: Int?
    let versionNo, successMessage, errorMessage: JSONNull?
    let successDate: String?
    
    enum CodingKeys: String, CodingKey {
        case kitchenID = "KitchenID"
        case kitchenTitle = "KitchenTitle"
        case kitchenDesc = "KitchenDesc"
        case password = "Password"
        case emailID = "EmailID"
        case isActive = "IsActive"
        case tranID = "TranID"
        case tokenID = "TokenID"
        case lastDate = "LastDate"
        case statusCode = "StatusCode"
        case versionNo = "VersionNo"
        case successMessage = "SuccessMessage"
        case errorMessage = "ErrorMessage"
        case successDate = "SuccessDate"
    }
    
    init(kitchenID: String?, kitchenTitle: String?, kitchenDesc: String?, password: JSONNull?, emailID: JSONNull?, isActive: Bool?, tranID: Int?, tokenID: JSONNull?, lastDate: JSONNull?, statusCode: Int?, versionNo: JSONNull?, successMessage: JSONNull?, errorMessage: JSONNull?, successDate: String?) {
        self.kitchenID = kitchenID
        self.kitchenTitle = kitchenTitle
        self.kitchenDesc = kitchenDesc
        self.password = password
        self.emailID = emailID
        self.isActive = isActive
        self.tranID = tranID
        self.tokenID = tokenID
        self.lastDate = lastDate
        self.statusCode = statusCode
        self.versionNo = versionNo
        self.successMessage = successMessage
        self.errorMessage = errorMessage
        self.successDate = successDate
    }
}
class DKAPIHomeDetails: Codable {
    let homeID: String
    let homeTitle: String
    let homeDesc: String
    let isActive: Bool
    let tranID: Int
    let tokenID: JSONNull?
    let lastDate: JSONNull?
    let statusCode: Int
    let versionNo: JSONNull?
    let successMessage: JSONNull?
    let errorMessage: JSONNull?
    
    enum CodingKeys: String, CodingKey {
        case homeID = "HomeID"
        case homeTitle = "HomeTitle"
        case homeDesc = "HomeDesc"
        case isActive = "IsActive"
        case tranID = "TranID"
        case tokenID = "TokenID"
        case lastDate = "LastDate"
        case statusCode = "StatusCode"
        case versionNo = "VersionNo"
        case successMessage = "SuccessMessage"
        case errorMessage = "ErrorMessage"
    }
    
    init(homeID: String, homeTitle: String, homeDesc: String, isActive: Bool, tranID: Int, tokenID: JSONNull?, lastDate: JSONNull?, statusCode: Int, versionNo: JSONNull?, successMessage: JSONNull?, errorMessage: JSONNull?) {
        self.homeID = homeID
        self.homeTitle = homeTitle
        self.homeDesc = homeDesc
        self.isActive = isActive
        self.tranID = tranID
        self.tokenID = tokenID
        self.lastDate = lastDate
        self.statusCode = statusCode
        self.versionNo = versionNo
        self.successMessage = successMessage
        self.errorMessage = errorMessage
    }
}


class DKAPICookResponseElement : NSObject, NSCoding {
    
    var cookDesc : String!
    var cookDesc_AR : String!
    var cookID : String!
    var cookName : String!
    var cookName_AR : String!
    var errorMessage : AnyObject!
    var isActive : Bool!
    var kitchenDetails : DKAPIKitchenDetail!
    var lastDate : AnyObject!
    var statusCode : Int!
    var successMessage : AnyObject!
    var tokenID : AnyObject!
    var tranID : Int!
    var versionNo : AnyObject!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        cookDesc = dictionary["CookDesc"] as? String
        cookID = dictionary["CookID"] as? String
        cookName = dictionary["CookName"] as? String
        cookDesc_AR = dictionary["CookDesc_ar"] as? String
        cookName_AR = dictionary["CookName_ar"] as? String
        errorMessage = dictionary["ErrorMessage"] as? AnyObject
        isActive = dictionary["IsActive"] as? Bool
        if let kitchenDetailsData = dictionary["KitchenDetails"] as? [String:Any]{
            kitchenDetails = DKAPIKitchenDetail(fromDictionary: kitchenDetailsData)
        }
        lastDate = dictionary["LastDate"] as? AnyObject
        statusCode = dictionary["StatusCode"] as? Int
        successMessage = dictionary["SuccessMessage"] as? AnyObject
        tokenID = dictionary["TokenID"] as? AnyObject
        tranID = dictionary["TranID"] as? Int
        versionNo = dictionary["VersionNo"] as? AnyObject
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if cookDesc != nil{
            dictionary["CookDesc"] = cookDesc
        }
        if cookID != nil{
            dictionary["CookID"] = cookID
        }
        if cookName != nil{
            dictionary["CookName"] = cookName
        }
        if cookDesc_AR != nil{
            dictionary["CookDesc_ar"] = cookDesc_AR
        }
        
        if cookName_AR != nil{
            dictionary["CookName_ar"] = cookName_AR
        }
        if errorMessage != nil{
            dictionary["ErrorMessage"] = errorMessage
        }
        if isActive != nil{
            dictionary["IsActive"] = isActive
        }
        if kitchenDetails != nil{
            dictionary["KitchenDetails"] = kitchenDetails.toDictionary()
        }
        if lastDate != nil{
            dictionary["LastDate"] = lastDate
        }
        if statusCode != nil{
            dictionary["StatusCode"] = statusCode
        }
        if successMessage != nil{
            dictionary["SuccessMessage"] = successMessage
        }
        if tokenID != nil{
            dictionary["TokenID"] = tokenID
        }
        if tranID != nil{
            dictionary["TranID"] = tranID
        }
        if versionNo != nil{
            dictionary["VersionNo"] = versionNo
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        cookDesc = aDecoder.decodeObject(forKey: "CookDesc") as? String
        cookID = aDecoder.decodeObject(forKey: "CookID") as? String
        cookName = aDecoder.decodeObject(forKey: "CookName") as? String
        cookDesc_AR = aDecoder.decodeObject(forKey: "CookDesc") as? String
        cookName_AR = aDecoder.decodeObject(forKey: "CookName") as? String
        errorMessage = aDecoder.decodeObject(forKey: "ErrorMessage") as? AnyObject
        isActive = aDecoder.decodeObject(forKey: "IsActive") as? Bool
        kitchenDetails = aDecoder.decodeObject(forKey: "KitchenDetails") as? DKAPIKitchenDetail
        lastDate = aDecoder.decodeObject(forKey: "LastDate") as? AnyObject
        statusCode = aDecoder.decodeObject(forKey: "StatusCode") as? Int
        successMessage = aDecoder.decodeObject(forKey: "SuccessMessage") as? AnyObject
        tokenID = aDecoder.decodeObject(forKey: "TokenID") as? AnyObject
        tranID = aDecoder.decodeObject(forKey: "TranID") as? Int
        versionNo = aDecoder.decodeObject(forKey: "VersionNo") as? AnyObject
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if cookDesc != nil{
            aCoder.encode(cookDesc, forKey: "CookDesc")
        }
        if cookID != nil{
            aCoder.encode(cookID, forKey: "CookID")
        }
        if cookName != nil{
            aCoder.encode(cookName, forKey: "CookName")
        }
        if cookDesc_AR != nil{
            aCoder.encode(cookDesc_AR, forKey: "CookDesc_ar")
        }
       
        if cookName_AR != nil{
            aCoder.encode(cookName_AR, forKey: "CookName_ar")
        }
        if errorMessage != nil{
            aCoder.encode(errorMessage, forKey: "ErrorMessage")
        }
        if isActive != nil{
            aCoder.encode(isActive, forKey: "IsActive")
        }
        if kitchenDetails != nil{
            aCoder.encode(kitchenDetails, forKey: "KitchenDetails")
        }
        if lastDate != nil{
            aCoder.encode(lastDate, forKey: "LastDate")
        }
        if statusCode != nil{
            aCoder.encode(statusCode, forKey: "StatusCode")
        }
        if successMessage != nil{
            aCoder.encode(successMessage, forKey: "SuccessMessage")
        }
        if tokenID != nil{
            aCoder.encode(tokenID, forKey: "TokenID")
        }
        if tranID != nil{
            aCoder.encode(tranID, forKey: "TranID")
        }
        if versionNo != nil{
            aCoder.encode(versionNo, forKey: "VersionNo")
        }
        
    }
    
}

class DKAPIKitchenDetail : NSObject, NSCoding{
    
    var errorMessage : AnyObject!
    var isActive : Bool!
    var kitchenDesc : String!
    var kitchenID : String!
    var kitchenTitle : String!
    var lastDate : AnyObject!
    var password : AnyObject!
    var statusCode : Int!
    var successMessage : AnyObject!
    var tokenID : AnyObject!
    var tranID : Int!
    var versionNo : AnyObject!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        errorMessage = dictionary["ErrorMessage"] as? AnyObject
        isActive = dictionary["IsActive"] as? Bool
        kitchenDesc = dictionary["KitchenDesc"] as? String
        kitchenID = dictionary["KitchenID"] as? String
        kitchenTitle = dictionary["KitchenTitle"] as? String
        lastDate = dictionary["LastDate"] as? AnyObject
        password = dictionary["Password"] as? AnyObject
        statusCode = dictionary["StatusCode"] as? Int
        successMessage = dictionary["SuccessMessage"] as? AnyObject
        tokenID = dictionary["TokenID"] as? AnyObject
        tranID = dictionary["TranID"] as? Int
        versionNo = dictionary["VersionNo"] as? AnyObject
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if errorMessage != nil{
            dictionary["ErrorMessage"] = errorMessage
        }
        if isActive != nil{
            dictionary["IsActive"] = isActive
        }
        if kitchenDesc != nil{
            dictionary["KitchenDesc"] = kitchenDesc
        }
        if kitchenID != nil{
            dictionary["KitchenID"] = kitchenID
        }
        if kitchenTitle != nil{
            dictionary["KitchenTitle"] = kitchenTitle
        }
        if lastDate != nil{
            dictionary["LastDate"] = lastDate
        }
        if password != nil{
            dictionary["Password"] = password
        }
        if statusCode != nil{
            dictionary["StatusCode"] = statusCode
        }
        if successMessage != nil{
            dictionary["SuccessMessage"] = successMessage
        }
        if tokenID != nil{
            dictionary["TokenID"] = tokenID
        }
        if tranID != nil{
            dictionary["TranID"] = tranID
        }
        if versionNo != nil{
            dictionary["VersionNo"] = versionNo
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        errorMessage = aDecoder.decodeObject(forKey: "ErrorMessage") as? AnyObject
        isActive = aDecoder.decodeObject(forKey: "IsActive") as? Bool
        kitchenDesc = aDecoder.decodeObject(forKey: "KitchenDesc") as? String
        kitchenID = aDecoder.decodeObject(forKey: "KitchenID") as? String
        kitchenTitle = aDecoder.decodeObject(forKey: "KitchenTitle") as? String
        lastDate = aDecoder.decodeObject(forKey: "LastDate") as? AnyObject
        password = aDecoder.decodeObject(forKey: "Password") as? AnyObject
        statusCode = aDecoder.decodeObject(forKey: "StatusCode") as? Int
        successMessage = aDecoder.decodeObject(forKey: "SuccessMessage") as? AnyObject
        tokenID = aDecoder.decodeObject(forKey: "TokenID") as? AnyObject
        tranID = aDecoder.decodeObject(forKey: "TranID") as? Int
        versionNo = aDecoder.decodeObject(forKey: "VersionNo") as? AnyObject
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if errorMessage != nil{
            aCoder.encode(errorMessage, forKey: "ErrorMessage")
        }
        if isActive != nil{
            aCoder.encode(isActive, forKey: "IsActive")
        }
        if kitchenDesc != nil{
            aCoder.encode(kitchenDesc, forKey: "KitchenDesc")
        }
        if kitchenID != nil{
            aCoder.encode(kitchenID, forKey: "KitchenID")
        }
        if kitchenTitle != nil{
            aCoder.encode(kitchenTitle, forKey: "KitchenTitle")
        }
        if lastDate != nil{
            aCoder.encode(lastDate, forKey: "LastDate")
        }
        if password != nil{
            aCoder.encode(password, forKey: "Password")
        }
        if statusCode != nil{
            aCoder.encode(statusCode, forKey: "StatusCode")
        }
        if successMessage != nil{
            aCoder.encode(successMessage, forKey: "SuccessMessage")
        }
        if tokenID != nil{
            aCoder.encode(tokenID, forKey: "TokenID")
        }
        if tranID != nil{
            aCoder.encode(tranID, forKey: "TranID")
        }
        if versionNo != nil{
            aCoder.encode(versionNo, forKey: "VersionNo")
        }
        
    }
    
}




class DKAPIHomeResponseElement: Codable {
    let homeID: String
    let homeTitle: String
    let homeDesc: String
    let homeTitle_AR: String
    let homeDesc_AR: String
    let location_ar: String
    let location: String
    let isActive: Bool
    let tranID: Int
    let tokenID: JSONNull?
    let lastDate: JSONNull?
    let statusCode: Int
    let versionNo: JSONNull?
    let successMessage: JSONNull?
    let errorMessage: JSONNull?
    
    enum CodingKeys: String, CodingKey {
        case homeID = "HomeID"
        case homeTitle = "HomeTitle"
        case homeDesc = "HomeDesc"
        case homeTitle_AR = "HomeTitle_ar"
        case homeDesc_AR = "HomeDesc_ar"
        case isActive = "IsActive"
        case tranID = "TranID"
        case tokenID = "TokenID"
        case lastDate = "LastDate"
        case statusCode = "StatusCode"
        case versionNo = "VersionNo"
        case successMessage = "SuccessMessage"
        case errorMessage = "ErrorMessage"
        case location_ar = "Location_ar"
        case location = "Location"
    }
    
    init(homeID: String, homeTitle: String, homeDesc: String, homeTitle_AR: String, homeDesc_AR: String, isActive: Bool, tranID: Int, tokenID: JSONNull?, lastDate: JSONNull?, statusCode: Int, versionNo: JSONNull?, successMessage: JSONNull?, errorMessage: JSONNull?,location_ar:String,location:String) {
        self.homeID = homeID
        self.homeTitle = homeTitle
        self.homeDesc = homeDesc
        self.homeTitle_AR = homeTitle_AR
        self.homeDesc_AR = homeDesc_AR
        self.isActive = isActive
        self.tranID = tranID
        self.tokenID = tokenID
        self.lastDate = lastDate
        self.statusCode = statusCode
        self.versionNo = versionNo
        self.successMessage = successMessage
        self.errorMessage = errorMessage
        self.location_ar = location_ar
        self.location = location
    }
}

/*class DKAPIMenuResponseElement: Codable {
    let menuItemID: JSONNull?
    let menuItemName: JSONNull?
    let menuItemDesc: JSONNull?
    let isActive: Bool
    let imageFileName: JSONNull?
    let imageExtension: JSONNull?
    let image: JSONNull?
    let catDetails: JSONNull?
    let tranID: Int
    let tokenID: JSONNull?
    let lastDate: JSONNull?
    let statusCode: Int
    let versionNo: JSONNull?
    let successMessage: JSONNull?
    let errorMessage: [String]
    
    enum CodingKeys: String, CodingKey {
        case menuItemID = "MenuItemID"
        case menuItemName = "MenuItemName"
        case menuItemDesc = "MenuItemDesc"
        case isActive = "IsActive"
        case imageFileName = "ImageFileName"
        case imageExtension = "ImageExtension"
        case image = "Image"
        case catDetails = "CatDetails"
        case tranID = "TranID"
        case tokenID = "TokenID"
        case lastDate = "LastDate"
        case statusCode = "StatusCode"
        case versionNo = "VersionNo"
        case successMessage = "SuccessMessage"
        case errorMessage = "ErrorMessage"
    }
    
    init(menuItemID: JSONNull?, menuItemName: JSONNull?, menuItemDesc: JSONNull?, isActive: Bool, imageFileName: JSONNull?, imageExtension: JSONNull?, image: JSONNull?, catDetails: JSONNull?, tranID: Int, tokenID: JSONNull?, lastDate: JSONNull?, statusCode: Int, versionNo: JSONNull?, successMessage: JSONNull?, errorMessage: [String]) {
        self.menuItemID = menuItemID
        self.menuItemName = menuItemName
        self.menuItemDesc = menuItemDesc
        self.isActive = isActive
        self.imageFileName = imageFileName
        self.imageExtension = imageExtension
        self.image = image
        self.catDetails = catDetails
        self.tranID = tranID
        self.tokenID = tokenID
        self.lastDate = lastDate
        self.statusCode = statusCode
        self.versionNo = versionNo
        self.successMessage = successMessage
        self.errorMessage = errorMessage
    }
}*/



class DKAPIAdminDashboradResponse: Codable {
    let totalOrderRequest: Int
    let totalOrder: Int
    let totalPendingorder: Int
    let totalAccecptedorder: Int
    let totalCancelledorder: Int
    let totalUser: Int
    let totalCook: Int
    let totalHouse: Int
    let menuList: Int
    let tranID: Int
    let tokenID:  JSONNull?
    let lastDate:  JSONNull?
    let statusCode: Int
    let versionNo:  JSONNull?
    let successMessage: JSONNull?
    let errorMessage:  JSONNull?
    
    enum CodingKeys: String, CodingKey {
        case totalOrderRequest = "TotalOrderRequest"
        case totalOrder = "TotalOrder"
        case totalPendingorder = "TotalPendingorder"
        case totalAccecptedorder = "TotalAccecptedorder"
        case totalCancelledorder = "TotalCancelledorder"
        case totalUser = "TotalUser"
        case totalCook = "TotalCook"
        case totalHouse = "TotalHouse"
        case menuList = "MenuList"
        case tranID = "TranID"
        case tokenID = "TokenID"
        case lastDate = "LastDate"
        case statusCode = "StatusCode"
        case versionNo = "VersionNo"
        case successMessage = "SuccessMessage"
        case errorMessage = "ErrorMessage"
    }
    
    init(totalOrderRequest: Int, totalOrder: Int, totalPendingorder: Int, totalAccecptedorder: Int, totalCancelledorder: Int, totalUser: Int, totalCook: Int, totalHouse: Int, menuList: Int, tranID: Int, tokenID:  JSONNull?, lastDate:  JSONNull?, statusCode: Int, versionNo:  JSONNull?, successMessage:  JSONNull?, errorMessage:  JSONNull?) {
        self.totalOrderRequest = totalOrderRequest
        self.totalOrder = totalOrder
        self.totalPendingorder = totalPendingorder
        self.totalAccecptedorder = totalAccecptedorder
        self.totalCancelledorder = totalCancelledorder
        self.totalUser = totalUser
        self.totalCook = totalCook
        self.totalHouse = totalHouse
        self.menuList = menuList
        self.tranID = tranID
        self.tokenID = tokenID
        self.lastDate = lastDate
        self.statusCode = statusCode
        self.versionNo = versionNo
        self.successMessage = successMessage
        self.errorMessage = errorMessage
    }
}

class DKAPIKitchenDashboradResponse: Codable {
    let totalOrderRequest: Int
    let totalOrder: Int
    let totalPendingorder: Int
    let totalAccecptedorder: Int
    let totalCancelledorder: Int
    let totalUser: Int
    let totalHouse: Int
    let menuList: Int
    let tranID: Int
    let tokenID: JSONNull?
    let lastDate: JSONNull?
    let statusCode: Int
    let versionNo: JSONNull?
    let successMessage: JSONNull?
    let errorMessage: JSONNull?
    
    enum CodingKeys: String, CodingKey {
        case totalOrderRequest = "TotalOrderRequest"
        case totalOrder = "TotalOrder"
        case totalPendingorder = "TotalPendingorder"
        case totalAccecptedorder = "TotalAccecptedorder"
        case totalCancelledorder = "TotalCancelledorder"
        case totalUser = "TotalUser"
        case totalHouse = "TotalHouse"
        case menuList = "MenuList"
        case tranID = "TranID"
        case tokenID = "TokenID"
        case lastDate = "LastDate"
        case statusCode = "StatusCode"
        case versionNo = "VersionNo"
        case successMessage = "SuccessMessage"
        case errorMessage = "ErrorMessage"
    }
    
    init(totalOrderRequest: Int, totalOrder: Int, totalPendingorder: Int, totalAccecptedorder: Int, totalCancelledorder: Int, totalUser: Int, totalHouse: Int, menuList: Int, tranID: Int, tokenID: JSONNull?, lastDate: JSONNull?, statusCode: Int, versionNo: JSONNull?, successMessage: JSONNull?, errorMessage: JSONNull?) {
        self.totalOrderRequest = totalOrderRequest
        self.totalOrder = totalOrder
        self.totalPendingorder = totalPendingorder
        self.totalAccecptedorder = totalAccecptedorder
        self.totalCancelledorder = totalCancelledorder
        self.totalUser = totalUser
        self.totalHouse = totalHouse
        self.menuList = menuList
        self.tranID = tranID
        self.tokenID = tokenID
        self.lastDate = lastDate
        self.statusCode = statusCode
        self.versionNo = versionNo
        self.successMessage = successMessage
        self.errorMessage = errorMessage
    }
}
class DKAPIOrdersByKitchenID: Codable {
    let orderID, orderDate, userID, userHomeID: String
    let deliveryHomeID, deliveryDate, menuItemID: String
    let orderQty: Int
    let orderStatus, kitchenID: String
    let tranID: Int
    let tokenID, lastDate: String
    let statusCode: Int
    let versionNo: String
    let successMessage, errorMessage: [String]
    
    enum CodingKeys: String, CodingKey {
        case orderID = "OrderID"
        case orderDate = "OrderDate"
        case userID = "UserID"
        case userHomeID = "UserHomeID"
        case deliveryHomeID = "DeliveryHomeID"
        case deliveryDate = "DeliveryDate"
        case menuItemID = "MenuItemID"
        case orderQty = "OrderQty"
        case orderStatus = "OrderStatus"
        case kitchenID = "KitchenID"
        case tranID = "TranID"
        case tokenID = "TokenID"
        case lastDate = "LastDate"
        case statusCode = "StatusCode"
        case versionNo = "VersionNo"
        case successMessage = "SuccessMessage"
        case errorMessage = "ErrorMessage"
    }
    
    init(orderID: String, orderDate: String, userID: String, userHomeID: String, deliveryHomeID: String, deliveryDate: String, menuItemID: String, orderQty: Int, orderStatus: String, kitchenID: String, tranID: Int, tokenID: String, lastDate: String, statusCode: Int, versionNo: String, successMessage: [String], errorMessage: [String]) {
        self.orderID = orderID
        self.orderDate = orderDate
        self.userID = userID
        self.userHomeID = userHomeID
        self.deliveryHomeID = deliveryHomeID
        self.deliveryDate = deliveryDate
        self.menuItemID = menuItemID
        self.orderQty = orderQty
        self.orderStatus = orderStatus
        self.kitchenID = kitchenID
        self.tranID = tranID
        self.tokenID = tokenID
        self.lastDate = lastDate
        self.statusCode = statusCode
        self.versionNo = versionNo
        self.successMessage = successMessage
        self.errorMessage = errorMessage
    }
}


extension Date
{
    func isBetween(_ date1: Date, and date2: Date) -> Bool {
        return (min(date1, date2) ... max(date1, date2)).contains(self)
    }
    
    func isBetweenDates(beginDate: Date, endDate: Date) -> Bool
    {
        if self.compare(beginDate) == .orderedAscending
        {
            return false
        }
        
        if self.compare(endDate) == .orderedDescending
        {
            return false
        }
        
        return true
    }
    
    func getWeekDates() -> (thisWeek:[Date],nextWeek:[Date]) {
        var tuple: (thisWeek:[Date],nextWeek:[Date])
        var arrThisWeek: [Date] = []
        for i in 0..<7 {
            arrThisWeek.append(Calendar.current.date(byAdding: .day, value: i, to: startOfWeek)!)
        }
        var arrNextWeek: [Date] = []
        for i in 1...7 {
            arrNextWeek.append(Calendar.current.date(byAdding: .day, value: i, to: arrThisWeek.last!)!)
        }
        tuple = (thisWeek: arrThisWeek,nextWeek: arrNextWeek)
        return tuple
    }
    
    var tomorrowdate: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }
    
    var threedays: Date
    {
        return Calendar.current.date(byAdding: .day, value: 3, to: noon)!
    }
    var weekdate: Date
    {
        return Calendar.current.date(byAdding: .day, value: 7, to: noon)!
    }
    
    var monthdate: Date
    {
        return Calendar.current.date(byAdding: .day, value: 30, to: noon)!
    }
    
    var noondate: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    
    var startOfWeek: Date {
        let gregorian = Calendar(identifier: .gregorian)
        let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self))
        return gregorian.date(byAdding: .day, value: 1, to: sunday!)!
    }
    
    func toDate(format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
    
    func offsetFrom(date : Date) -> (Int ,Int ,Int , Int) {
        
        let dayHourMinuteSecond: Set<Calendar.Component> = [.day, .hour, .minute, .second]
        let difference = NSCalendar.current.dateComponents(dayHourMinuteSecond, from: date, to: self)
        return (difference.day!,difference.hour!,difference.minute!,difference.second!)
    }
}
