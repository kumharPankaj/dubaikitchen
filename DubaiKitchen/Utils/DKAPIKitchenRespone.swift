//
//	DKAPIKitchenRespone.swift
//
//	Create by Tilak Raj Verma on 6/3/2019
//	Copyright © 2019. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class DKAPIKitchenRespone : NSObject, NSCoding{

	var emailID : String!
	var errorMessage : AnyObject!
	var isActive : Bool!
	var kitchenDesc : String!
	var kitchenID : String!
	var kitchenTitle : String!
	var lastDate : AnyObject!
	var password : AnyObject!
	var statusCode : Int!
	var successMessage : AnyObject!
	var tokenID : AnyObject!
	var tranID : Int!
	var versionNo : AnyObject!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		emailID = dictionary["EmailID"] as? String
		errorMessage = dictionary["ErrorMessage"] as? AnyObject
		isActive = dictionary["IsActive"] as? Bool
		kitchenDesc = dictionary["KitchenDesc"] as? String
		kitchenID = dictionary["KitchenID"] as? String
		kitchenTitle = dictionary["KitchenTitle"] as? String
		lastDate = dictionary["LastDate"] as? AnyObject
		password = dictionary["Password"] as? AnyObject
		statusCode = dictionary["StatusCode"] as? Int
		successMessage = dictionary["SuccessMessage"] as? AnyObject
		tokenID = dictionary["TokenID"] as? AnyObject
		tranID = dictionary["TranID"] as? Int
		versionNo = dictionary["VersionNo"] as? AnyObject
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if emailID != nil{
			dictionary["EmailID"] = emailID
		}
		if errorMessage != nil{
			dictionary["ErrorMessage"] = errorMessage
		}
		if isActive != nil{
			dictionary["IsActive"] = isActive
		}
		if kitchenDesc != nil{
			dictionary["KitchenDesc"] = kitchenDesc
		}
		if kitchenID != nil{
			dictionary["KitchenID"] = kitchenID
		}
		if kitchenTitle != nil{
			dictionary["KitchenTitle"] = kitchenTitle
		}
		if lastDate != nil{
			dictionary["LastDate"] = lastDate
		}
		if password != nil{
			dictionary["Password"] = password
		}
		if statusCode != nil{
			dictionary["StatusCode"] = statusCode
		}
		if successMessage != nil{
			dictionary["SuccessMessage"] = successMessage
		}
		if tokenID != nil{
			dictionary["TokenID"] = tokenID
		}
		if tranID != nil{
			dictionary["TranID"] = tranID
		}
		if versionNo != nil{
			dictionary["VersionNo"] = versionNo
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         emailID = aDecoder.decodeObject(forKey: "EmailID") as? String
         errorMessage = aDecoder.decodeObject(forKey: "ErrorMessage") as? AnyObject
         isActive = aDecoder.decodeObject(forKey: "IsActive") as? Bool
         kitchenDesc = aDecoder.decodeObject(forKey: "KitchenDesc") as? String
         kitchenID = aDecoder.decodeObject(forKey: "KitchenID") as? String
         kitchenTitle = aDecoder.decodeObject(forKey: "KitchenTitle") as? String
         lastDate = aDecoder.decodeObject(forKey: "LastDate") as? AnyObject
         password = aDecoder.decodeObject(forKey: "Password") as? AnyObject
         statusCode = aDecoder.decodeObject(forKey: "StatusCode") as? Int
         successMessage = aDecoder.decodeObject(forKey: "SuccessMessage") as? AnyObject
         tokenID = aDecoder.decodeObject(forKey: "TokenID") as? AnyObject
         tranID = aDecoder.decodeObject(forKey: "TranID") as? Int
         versionNo = aDecoder.decodeObject(forKey: "VersionNo") as? AnyObject

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if emailID != nil{
			aCoder.encode(emailID, forKey: "EmailID")
		}
		if errorMessage != nil{
			aCoder.encode(errorMessage, forKey: "ErrorMessage")
		}
		if isActive != nil{
			aCoder.encode(isActive, forKey: "IsActive")
		}
		if kitchenDesc != nil{
			aCoder.encode(kitchenDesc, forKey: "KitchenDesc")
		}
		if kitchenID != nil{
			aCoder.encode(kitchenID, forKey: "KitchenID")
		}
		if kitchenTitle != nil{
			aCoder.encode(kitchenTitle, forKey: "KitchenTitle")
		}
		if lastDate != nil{
			aCoder.encode(lastDate, forKey: "LastDate")
		}
		if password != nil{
			aCoder.encode(password, forKey: "Password")
		}
		if statusCode != nil{
			aCoder.encode(statusCode, forKey: "StatusCode")
		}
		if successMessage != nil{
			aCoder.encode(successMessage, forKey: "SuccessMessage")
		}
		if tokenID != nil{
			aCoder.encode(tokenID, forKey: "TokenID")
		}
		if tranID != nil{
			aCoder.encode(tranID, forKey: "TranID")
		}
		if versionNo != nil{
			aCoder.encode(versionNo, forKey: "VersionNo")
		}

	}

}