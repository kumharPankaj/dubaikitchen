//
//	DKAPIGetMenuElement.swift
//
//	Create by Tilak Raj Verma on 4/3/2019
//	Copyright © 2019. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class DKAPIGetMenuElement : NSObject, NSCoding{

	var catDetails : DKAPICatDetail!
	var errorMessage : [String]!
	var image : String!
	var imageExtension : String!
	var imageFileName : String!
	var isActive : Bool!
	var lastDate : String!
	var menuItemDesc : String!
	var menuItemID : String!
	var menuItemName : String!
	var statusCode : Int!
	var successMessage : [String]!
	var tokenID : String!
	var tranID : Int!
	var versionNo : String!
    var fullImagePath:String!
    var MenuItemName_ar :String!
    var MenuItemDesc_ar:String!
    var CatDesc_ar:String!
    var CatName_ar:String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		if let catDetailsData = dictionary["CatDetails"] as? [String:Any]{
			catDetails = DKAPICatDetail(fromDictionary: catDetailsData)
		}
		errorMessage = dictionary["ErrorMessage"] as? [String]
		image = dictionary["Image"] as? String
		imageExtension = dictionary["ImageExtension"] as? String
		imageFileName = dictionary["ImageFileName"] as? String
		isActive = dictionary["IsActive"] as? Bool
		lastDate = dictionary["LastDate"] as? String
		menuItemDesc = dictionary["MenuItemDesc"] as? String
		menuItemID = dictionary["MenuItemID"] as? String
		menuItemName = dictionary["MenuItemName"] as? String
		statusCode = dictionary["StatusCode"] as? Int
		successMessage = dictionary["SuccessMessage"] as? [String]
		tokenID = dictionary["TokenID"] as? String
		tranID = dictionary["TranID"] as? Int
		versionNo = dictionary["VersionNo"] as? String
        fullImagePath = dictionary["FullImagePath"] as? String
        
          MenuItemName_ar = dictionary["MenuItemName_ar"] as? String
          MenuItemDesc_ar = dictionary["MenuItemDesc_ar"] as? String
          CatDesc_ar = dictionary["CatDesc_ar"] as? String
          CatName_ar = dictionary["CatName_ar"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if catDetails != nil{
			dictionary["CatDetails"] = catDetails.toDictionary()
		}
		if errorMessage != nil{
			dictionary["ErrorMessage"] = errorMessage
		}
		if image != nil{
			dictionary["Image"] = image
		}
		if imageExtension != nil{
			dictionary["ImageExtension"] = imageExtension
		}
		if imageFileName != nil{
			dictionary["ImageFileName"] = imageFileName
		}
		if isActive != nil{
			dictionary["IsActive"] = isActive
		}
		if lastDate != nil{
			dictionary["LastDate"] = lastDate
		}
		if menuItemDesc != nil{
			dictionary["MenuItemDesc"] = menuItemDesc
		}
		if menuItemID != nil{
			dictionary["MenuItemID"] = menuItemID
		}
		if menuItemName != nil{
			dictionary["MenuItemName"] = menuItemName
		}
		if statusCode != nil{
			dictionary["StatusCode"] = statusCode
		}
		if successMessage != nil{
			dictionary["SuccessMessage"] = successMessage
		}
		if tokenID != nil{
			dictionary["TokenID"] = tokenID
		}
		if tranID != nil{
			dictionary["TranID"] = tranID
		}
		if versionNo != nil{
			dictionary["VersionNo"] = versionNo
		}
        if fullImagePath != nil{
            dictionary["FullImagePath"] = fullImagePath
        }
        
        if MenuItemName_ar != nil{
            dictionary["MenuItemName_ar"] = MenuItemName_ar
        }
        
        if MenuItemDesc_ar != nil{
            dictionary["MenuItemDesc_ar"] = MenuItemDesc_ar
        }
        
        if CatDesc_ar != nil{
            dictionary["CatDesc_ar"] = CatDesc_ar
        }
        if CatName_ar != nil{
            dictionary["CatName_ar"] = CatName_ar
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         catDetails = aDecoder.decodeObject(forKey: "CatDetails") as? DKAPICatDetail
         errorMessage = aDecoder.decodeObject(forKey: "ErrorMessage") as? [String]
         image = aDecoder.decodeObject(forKey: "Image") as? String
         imageExtension = aDecoder.decodeObject(forKey: "ImageExtension") as? String
         imageFileName = aDecoder.decodeObject(forKey: "ImageFileName") as? String
         isActive = aDecoder.decodeObject(forKey: "IsActive") as? Bool
         lastDate = aDecoder.decodeObject(forKey: "LastDate") as? String
         menuItemDesc = aDecoder.decodeObject(forKey: "MenuItemDesc") as? String
         menuItemID = aDecoder.decodeObject(forKey: "MenuItemID") as? String
         menuItemName = aDecoder.decodeObject(forKey: "MenuItemName") as? String
         statusCode = aDecoder.decodeObject(forKey: "StatusCode") as? Int
         successMessage = aDecoder.decodeObject(forKey: "SuccessMessage") as? [String]
         tokenID = aDecoder.decodeObject(forKey: "TokenID") as? String
         tranID = aDecoder.decodeObject(forKey: "TranID") as? Int
         versionNo = aDecoder.decodeObject(forKey: "VersionNo") as? String
        fullImagePath = aDecoder.decodeObject(forKey: "FullImagePath") as? String
        
        MenuItemDesc_ar = aDecoder.decodeObject(forKey: "MenuItemDesc_ar") as? String
        
        MenuItemName_ar = aDecoder.decodeObject(forKey: "MenuItemName_ar") as? String
        
        CatName_ar = aDecoder.decodeObject(forKey: "CatName_ar") as? String
        
        CatDesc_ar = aDecoder.decodeObject(forKey: "CatDesc_ar") as? String
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if catDetails != nil{
			aCoder.encode(catDetails, forKey: "CatDetails")
		}
		if errorMessage != nil{
			aCoder.encode(errorMessage, forKey: "ErrorMessage")
		}
		if image != nil{
			aCoder.encode(image, forKey: "Image")
		}
		if imageExtension != nil{
			aCoder.encode(imageExtension, forKey: "ImageExtension")
		}
		if imageFileName != nil{
			aCoder.encode(imageFileName, forKey: "ImageFileName")
		}
		if isActive != nil{
			aCoder.encode(isActive, forKey: "IsActive")
		}
		if lastDate != nil{
			aCoder.encode(lastDate, forKey: "LastDate")
		}
		if menuItemDesc != nil{
			aCoder.encode(menuItemDesc, forKey: "MenuItemDesc")
		}
		if menuItemID != nil{
			aCoder.encode(menuItemID, forKey: "MenuItemID")
		}
		if menuItemName != nil{
			aCoder.encode(menuItemName, forKey: "MenuItemName")
		}
		if statusCode != nil{
			aCoder.encode(statusCode, forKey: "StatusCode")
		}
		if successMessage != nil{
			aCoder.encode(successMessage, forKey: "SuccessMessage")
		}
		if tokenID != nil{
			aCoder.encode(tokenID, forKey: "TokenID")
		}
		if tranID != nil{
			aCoder.encode(tranID, forKey: "TranID")
		}
		if versionNo != nil{
			aCoder.encode(versionNo, forKey: "VersionNo")
		}
        if fullImagePath != nil{
            aCoder.encode(fullImagePath, forKey: "FullImagePath")
        }

        if MenuItemName_ar != nil{
            aCoder.encode(MenuItemName_ar, forKey: "MenuItemName_ar")
        }
        
        if MenuItemDesc_ar != nil{
            aCoder.encode(MenuItemDesc_ar, forKey: "MenuItemDesc_ar")
        }
        
        if CatName_ar != nil{
            aCoder.encode(CatName_ar, forKey: "CatName_ar")
        }
        
        if CatDesc_ar != nil{
            aCoder.encode(CatDesc_ar, forKey: "CatDesc_ar")
        }
	}

}
