//
//	DKAPICatDetail.swift
//
//	Create by Tilak Raj Verma on 4/3/2019
//	Copyright © 2019. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class DKAPICatDetail : NSObject, NSCoding{

	var catDesc : String!
	var catID : String!
	var catName : String!
    var catDesc_AR : String!
    var catName_AR : String!
	var errorMessage : [String]!
	var isActive : Bool!
	var lastDate : String!
	var statusCode : Int!
	var successMessage : [String]!
	var tokenID : String!
	var tranID : Int!
	var versionNo : String!
    var catImageFullPath:String!
    

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		catDesc = dictionary["CatDesc"] as? String
		catID = dictionary["CatID"] as? String
		catName = dictionary["CatName"] as? String
        catDesc_AR = dictionary["CatDesc_ar"] as? String
        catName_AR = dictionary["CatName_ar"] as? String
		errorMessage = dictionary["ErrorMessage"] as? [String]
		isActive = dictionary["IsActive"] as? Bool
		lastDate = dictionary["LastDate"] as? String
		statusCode = dictionary["StatusCode"] as? Int
		successMessage = dictionary["SuccessMessage"] as? [String]
		tokenID = dictionary["TokenID"] as? String
		tranID = dictionary["TranID"] as? Int
		versionNo = dictionary["VersionNo"] as? String
        catImageFullPath = dictionary["CatFullImagePath"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if catDesc != nil{
			dictionary["CatDesc"] = catDesc
		}
		if catID != nil{
			dictionary["CatID"] = catID
		}
		if catName != nil{
			dictionary["CatName"] = catName
		}
        if catDesc_AR != nil{
            dictionary["CatDesc_ar"] = catDesc
        }
       
        if catName_AR != nil{
            dictionary["CatName_ar"] = catName
        }
		if errorMessage != nil{
			dictionary["ErrorMessage"] = errorMessage
		}
		if isActive != nil{
			dictionary["IsActive"] = isActive
		}
		if lastDate != nil{
			dictionary["LastDate"] = lastDate
		}
		if statusCode != nil{
			dictionary["StatusCode"] = statusCode
		}
		if successMessage != nil{
			dictionary["SuccessMessage"] = successMessage
		}
		if tokenID != nil{
			dictionary["TokenID"] = tokenID
		}
		if tranID != nil{
			dictionary["TranID"] = tranID
		}
		if versionNo != nil{
			dictionary["VersionNo"] = versionNo
		}
        if catImageFullPath != nil{
            dictionary["CatFullImagePath"] = catImageFullPath
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         catDesc = aDecoder.decodeObject(forKey: "CatDesc") as? String
         catID = aDecoder.decodeObject(forKey: "CatID") as? String
         catName = aDecoder.decodeObject(forKey: "CatName") as? String
        catDesc_AR = aDecoder.decodeObject(forKey: "CatDesc_ar") as? String
        catName_AR = aDecoder.decodeObject(forKey: "CatName_ar") as? String
         errorMessage = aDecoder.decodeObject(forKey: "ErrorMessage") as? [String]
         isActive = aDecoder.decodeObject(forKey: "IsActive") as? Bool
         lastDate = aDecoder.decodeObject(forKey: "LastDate") as? String
         statusCode = aDecoder.decodeObject(forKey: "StatusCode") as? Int
         successMessage = aDecoder.decodeObject(forKey: "SuccessMessage") as? [String]
         tokenID = aDecoder.decodeObject(forKey: "TokenID") as? String
         tranID = aDecoder.decodeObject(forKey: "TranID") as? Int
         versionNo = aDecoder.decodeObject(forKey: "VersionNo") as? String
        catImageFullPath = aDecoder.decodeObject(forKey: "CatFullImagePath") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if catDesc != nil{
			aCoder.encode(catDesc, forKey: "CatDesc")
		}
		if catID != nil{
			aCoder.encode(catID, forKey: "CatID")
		}
		if catName != nil{
			aCoder.encode(catName, forKey: "CatName")
		}
        if catDesc_AR != nil{
            aCoder.encode(catDesc, forKey: "CatDesc_ar")
        }
        
        if catName_AR != nil{
            aCoder.encode(catName, forKey: "CatName_ar")
        }
		if errorMessage != nil{
			aCoder.encode(errorMessage, forKey: "ErrorMessage")
		}
		if isActive != nil{
			aCoder.encode(isActive, forKey: "IsActive")
		}
		if lastDate != nil{
			aCoder.encode(lastDate, forKey: "LastDate")
		}
		if statusCode != nil{
			aCoder.encode(statusCode, forKey: "StatusCode")
		}
		if successMessage != nil{
			aCoder.encode(successMessage, forKey: "SuccessMessage")
		}
		if tokenID != nil{
			aCoder.encode(tokenID, forKey: "TokenID")
		}
		if tranID != nil{
			aCoder.encode(tranID, forKey: "TranID")
		}
		if versionNo != nil{
			aCoder.encode(versionNo, forKey: "VersionNo")
		}
        if catImageFullPath != nil{
            aCoder.encode(versionNo, forKey: "CatFullImagePath")
        }
	}

}
