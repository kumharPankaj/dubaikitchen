//
//  Common.swift
//  Rockinap
//
//  Created by orionspica on 02/10/18.
//  Copyright © 2018 funndynamix. All rights reserved.
//

import UIKit
//import Firebase

public typealias WebCallbackHandler = (_ status :Bool,  _ data:AnyObject?)  -> Void
public typealias AlertCallbackHandler = (_ action:String?)  -> Void
public typealias CallbackHandler = (_ view : String)  -> Void
public typealias CallbackHandlerWithData = (_ view : String,_ data: AnyObject)  -> Void
public var selectedUserRole = 1 //for UI testing
public var selectedFooterOption = 0
var TABLE_LANGUAGE = "English"
var STORYBOARD_LANGUAGE = "en"
let COMMENT_HEADER = "DubaiKitchenComment"
let ARABIC  = "Arabic"
let ENGLISH = "English"
let ARABIC_STORYBOARD = "ar-AE"
let ENGLISH_STORYBOARD = "en"
var notificationOrderID = "-1"
class Common {
    private static var sharedManager: Common =
    {
        let tempSharedManager = Common.init()
        
        return tempSharedManager
    }()
    
    class var shared: Common
    {
        return sharedManager
    }
    


}

extension NSObject {
    
    func setNotificationToken(_ status:String)
    {
        let usD =  UserDefaults.standard
        usD.set(status, forKey: "NotificationsToken")
        usD.synchronize()
    }
    
    var notificationsToken:String?
    {
        let usD =  UserDefaults.standard
        return usD.string(forKey: "NotificationsToken")
    }
}


extension UIColor {
    //var color1 = hexStringToUIColor("#d3d3d3")
    func hexStringToUIColor (hex:String,_ alpha:Double = 1.0) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        if ((cString.count) != 6) {
            return UIColor.black
        }
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(alpha)
        )
    }
    convenience init?(hexString: String) {
        /*       guard let hex = hexString.hex else {
         return nil
         }
         self.init(hex: hex)*/
        let hexString:String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        let scanner = Scanner(string: hexString as String)
        
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        var color:UInt32 = 0
        scanner.scanHexInt32(&color)
        
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        
        self.init(red:red, green:green, blue:blue, alpha:1)
    }
}

struct ColorCodes  {
    
    static  let color_FCB315 = "#FCB315"
    static  let  color_c3a863 = "#c3a863"
    static  let  color_000000 = "#000000"
    static  let  color_e0e0e0 = "#e0e0e0"
    static let  color_5380D5 = "#5380D5"
    static let color_Out_Premise_Landing_Blue = "299FC0"
}
struct OutleCategory {
    static  let NightinOut = "nightin out"
    static  let  EatinOut = "eatin out"
    static  let  ChilinOut = "chilin out"
    static  let  ShopinOut = "shopin out"
}
enum API_PARAM_KEYS: String {
    case CATEGORY = "category"
    case ORGANISATION_ID = "organisation_id"
}
enum LINE_POSITION {
    case LINE_POSITION_TOP
    case LINE_POSITION_BOTTOM
}
enum FORM_TITLE: String {
     case ADD_USER = "Add User"
    case ADD_KITHCEN = "Add Kitchen"
    case ADD_MENU = "Add Menu"
    case ADD_HOUSE = "Add House"
    case ADD_COOK = "Add Cook"
    case ADD_CATEGORY = "Add Category"
}
enum FORM_FIELD: Int{
    
    case COOK_NAME = 0
    case ASSIGN_HOUSE
    case ASSIGN_KITCHEN
    case HOUSE_NAME
    case ADD_LOCATION
    case DESCRIPTION
    case ITEM_NAME
    case UPLOAD_IMAGE
    case ADD_CATEGORY
    case KITCHEN_NAME
    case USER_NAME
    case PHONE
    case ADULT
    case PASSWORD
    case ADD
    case CATEGORY_NAME
    case EMAIL
    case COOK_NAME_ARABIC
    case HOUSE_NAME_ARABIC
    case ITEM_NAME_ARABIC
    case KITCHEN_NAME_ARABIC
    case USER_NAME_ARABIC
    case CATEGORY_NAME_ARABIC
    case DESCRIPTION_ARABIC
    case ADD_LOCATION_AR
    
    static func getPlaceHolderOrButtonTitle(feildTag: Int) -> String{
        var strPlaceHolder = ""
        switch feildTag {
        case FORM_FIELD.COOK_NAME.rawValue:
            strPlaceHolder = "Cook Name"
        case FORM_FIELD.ASSIGN_HOUSE.rawValue:
            strPlaceHolder = NSLocalizedString("Assign House", comment:"")
        case FORM_FIELD.ASSIGN_KITCHEN.rawValue:
            strPlaceHolder = NSLocalizedString("Assign Kitchen", comment:"")
        case FORM_FIELD.HOUSE_NAME.rawValue:
            strPlaceHolder = "House Name"
        case FORM_FIELD.ADD_LOCATION.rawValue:
            strPlaceHolder = NSLocalizedString("Add Location", comment:"")
        case FORM_FIELD.ITEM_NAME.rawValue:
            strPlaceHolder = "Item Name"
        case FORM_FIELD.UPLOAD_IMAGE.rawValue:
            strPlaceHolder = NSLocalizedString("Upload Image", comment:"")
        case FORM_FIELD.ADD_CATEGORY.rawValue:
            strPlaceHolder = NSLocalizedString("Select Category", comment:"")
        case FORM_FIELD.KITCHEN_NAME.rawValue:
            strPlaceHolder = "Kitchen Name"
        case FORM_FIELD.USER_NAME.rawValue:
            strPlaceHolder = "User Email"
        case FORM_FIELD.PHONE.rawValue:
            strPlaceHolder = NSLocalizedString("Phone", comment:"")
        case FORM_FIELD.PASSWORD.rawValue:
            strPlaceHolder = NSLocalizedString("Password", comment:"")
        case FORM_FIELD.ADD.rawValue:
            strPlaceHolder = NSLocalizedString("Add", comment:"")
        case FORM_FIELD.DESCRIPTION.rawValue:
            strPlaceHolder = "Description"
        case FORM_FIELD.ADULT.rawValue:
            strPlaceHolder = NSLocalizedString("Adult", comment:"")
        case FORM_FIELD.CATEGORY_NAME.rawValue:
            strPlaceHolder = "Category Name"
        case FORM_FIELD.EMAIL.rawValue:
            strPlaceHolder = NSLocalizedString("Email ID", comment:"")
        case FORM_FIELD.CATEGORY_NAME_ARABIC.rawValue:
            strPlaceHolder = "اسم الفئة"
        case FORM_FIELD.HOUSE_NAME_ARABIC.rawValue:
            strPlaceHolder = "اسم المنزل"
        case FORM_FIELD.KITCHEN_NAME_ARABIC.rawValue:
            strPlaceHolder = "اسم المطبخ"
        case FORM_FIELD.USER_NAME_ARABIC.rawValue:
                strPlaceHolder = "اسم المستخدم"
        case FORM_FIELD.COOK_NAME_ARABIC.rawValue:
            strPlaceHolder = "اسم الطباخ"
        case FORM_FIELD.DESCRIPTION_ARABIC.rawValue:
            strPlaceHolder = "الوصف"
        case FORM_FIELD.ITEM_NAME_ARABIC.rawValue:
            strPlaceHolder = "اسم المادة"
        case FORM_FIELD.ADD_LOCATION_AR.rawValue:
            strPlaceHolder = "إضافة موقع"
            
        default:
            strPlaceHolder = ""
        }
        
        return strPlaceHolder
    }
    static func getRowHeight(feildTag: Int) -> Int{
        var rowHeight = 0
        switch feildTag {

        case FORM_FIELD.ADD_CATEGORY.rawValue, FORM_FIELD.UPLOAD_IMAGE.rawValue, FORM_FIELD.ADULT.rawValue, FORM_FIELD.ADD.rawValue:
            rowHeight = 50

        case FORM_FIELD.DESCRIPTION.rawValue, FORM_FIELD.DESCRIPTION_ARABIC.rawValue:
            rowHeight = 80
        default:
            rowHeight = 44
        }
        
        return rowHeight
    }
    
}
extension UIImage {
    func trim(trimRect :CGRect) -> UIImage {
        if CGRect(origin: CGPoint.zero, size: self.size).contains(trimRect) {
            if let imageRef = self.cgImage?.cropping(to: trimRect) {
                return UIImage(cgImage: imageRef)
            }
        }
        
        UIGraphicsBeginImageContextWithOptions(trimRect.size, true, self.scale)
        self.draw(in: CGRect(x: -trimRect.minX, y: -trimRect.minY, width: self.size.width, height: self.size.height))
        let trimmedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let image = trimmedImage else { return self }
        
        return image
    }
    
    public func maskWithColor(color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        let context = UIGraphicsGetCurrentContext()!
        let rect = CGRect(origin: CGPoint.zero, size: size)
        color.setFill()
        self.draw(in: rect)
        context.setBlendMode(.sourceIn)
        context.fill(rect)
        let resultImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return resultImage
    }
    func resize(_ image: UIImage,factor:Float) -> UIImage {
        var actualHeight = Float(image.size.height)
        var actualWidth = Float(image.size.width)
        let maxHeight: Float = 10*factor
        let maxWidth: Float = 10*factor
        var imgRatio: Float = actualWidth / actualHeight
        let maxRatio: Float = maxWidth / maxHeight
        let compressionQuality: Float = 0.0
        //50 percent compression
        if actualHeight > maxHeight || actualWidth > maxWidth {
            if imgRatio < maxRatio {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }
            else if imgRatio > maxRatio {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            }
            else {
                actualHeight = maxHeight
                actualWidth = maxWidth
            }
        }
        let rect = CGRect(x: 0.0, y: 0.0, width: CGFloat(actualWidth), height: CGFloat(actualHeight))
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        let imageData = img!.jpegData(compressionQuality: CGFloat(compressionQuality))
        UIGraphicsEndImageContext()
        return UIImage(data: imageData!) ?? UIImage()
    }
    public func maskWithColorNew(color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        let context = UIGraphicsGetCurrentContext()!
        let rect = CGRect(origin: CGPoint.zero, size: size)
        let rectSizeNew = CGSize.init(width: size.width/2, height: size.height)
        let rectNew = CGRect(origin: CGPoint.zero, size: rectSizeNew)
        color.setFill()
        self.draw(in: rect)
        context.setBlendMode(.sourceIn)
        context.fill(rectNew)
        let resultImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return resultImage
    }
}
extension UIView {
    
    // OUTPUT 1
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: -1, height: 1)
        layer.shadowRadius = 1
        
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    // OUTPUT 2
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true,cellWidth: Int) {
        self.layoutSubviews()
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        self.setNeedsLayout()
        self.layoutIfNeeded()
        let constant = Int(radius)
        layer.shadowPath = UIBezierPath.init(roundedRect: CGRect.init(x: -constant/2, y: -constant/2, width: cellWidth - 5, height: Int(self.frame.height) + constant*2), cornerRadius: 10).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
        
   
        
    }
    
    func viewBottomShadow() {
        // Shadow and Radius
        layer.shadowColor = UIColor.darkGray.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        layer.shadowOpacity = 1.0
        layer.shadowRadius = 0.0
        layer.masksToBounds = false
    }
    
    func addRatingStar(uptoCount : Double) -> NSMutableAttributedString {
        let starCount = NSMutableAttributedString()
        
        for i in stride(from: 1.0, to: 6.0, by: 1.0) {
            if uptoCount < i {
                let (wholePart, fractionalPart) = modf(uptoCount)
                let imgAttachment = NSTextAttachment()
                if i == (wholePart + 1) && (fractionalPart != 0.0){
                    imgAttachment.image = UIImage.init(named: "star_raing")?.maskWithColor(color: UIColor.white).maskWithColorNew(color: UIColor.orange)
                }
                else{
                    imgAttachment.image = UIImage.init(named: "star_raing")?.maskWithColor(color: UIColor.init(white: 1, alpha: 0.5))
                }
                let attachmentString:NSAttributedString = NSAttributedString(attachment: imgAttachment)
                starCount.append(attachmentString)
            }
            else{
                let imgAttachment = NSTextAttachment()
                imgAttachment.image = UIImage.init(named: "star_raing")?.maskWithColor(color: UIColor.orange)
                let attachmentString:NSAttributedString = NSAttributedString(attachment: imgAttachment)
                starCount.append(attachmentString)
            }
        }
        
        return starCount
    }
}
public extension UIImage
{
    public convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1))
    {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
    
    func getImageWithColor(color: UIColor) -> UIImage
    {
        let tintableImage = self.withRenderingMode(.alwaysTemplate)
        return tintableImage
    }
}
struct defaultsKeys {
    static var socialMediaType = "socialMediaType"
    static var langSelected = "language"
    static var sbSelected = "storyboard"
}
extension Bundle {
    
    @objc func specialLocalizedStringForKey(_ key: String, value: String?, table tableName: String?) -> String {
        
        //        if let keyGuest  = UserDefaults.standard.value(forKey: defaultsKeys.cartCount){
        //            if keyGuest as! Int  != 0{
        //                UserDefaults.standard.set((keyGuest as! Int) + 1, forKey: defaultsKeys.cartCount)
        //            }
        //        }
        
        
        if self == Bundle.main {
            var currentStoryBoard = STORYBOARD_LANGUAGE
            var currentLang = ENGLISH
            if let keyGuest  = UserDefaults.standard.value(forKey: defaultsKeys.langSelected){
                if keyGuest as! String == ARABIC{
                    currentStoryBoard = ARABIC_STORYBOARD
                    currentLang = ARABIC
                } else {
                    currentStoryBoard = ENGLISH_STORYBOARD
                    currentLang = ENGLISH
                }
            } else {
                currentStoryBoard = ENGLISH_STORYBOARD
                currentLang = ENGLISH
            }
            
            var bundle = Bundle();
            if let _path = Bundle.main.path(forResource: currentLang, ofType: "lproj") {
                bundle = Bundle(path: _path)!
            }else
                if let _path = Bundle.main.path(forResource: currentStoryBoard, ofType: "lproj") {
                    bundle = Bundle(path: _path)!
                } else {
                    let _path = Bundle.main.path(forResource: "Base", ofType: "lproj")!
                    bundle = Bundle(path: _path)!
            }
            return (bundle.specialLocalizedStringForKey(key, value: value, table: tableName))
        } else {
            return (self.specialLocalizedStringForKey(key, value: value, table: tableName))
        }
    }
}
func MethodSwizzleGivenClassName(cls: AnyClass, originalSelector: Selector, overrideSelector: Selector) {
    let origMethod: Method = class_getInstanceMethod(cls, originalSelector)!;
    let overrideMethod: Method = class_getInstanceMethod(cls, overrideSelector)!;
    if (class_addMethod(cls, originalSelector, method_getImplementation(overrideMethod), method_getTypeEncoding(overrideMethod))) {
        class_replaceMethod(cls, overrideSelector, method_getImplementation(origMethod), method_getTypeEncoding(origMethod));
    } else {
        method_exchangeImplementations(origMethod, overrideMethod);
    }
}
extension NSObject{
    
    class func DoTheMagic() {
        MethodSwizzleGivenClassName(cls: Bundle.self, originalSelector: #selector(Bundle.localizedString(forKey:value:table:)), overrideSelector: #selector(Bundle.specialLocalizedStringForKey(_:value:table:)))
        
    }
    
    var isApplanguageIsArabic:Bool
    {
        if let keyGuest  = UserDefaults.standard.value(forKey: defaultsKeys.langSelected)
        {
            if keyGuest as! String == ARABIC
            {
                return true
            }
        }
        return false
    }
}

extension UIImageView {
    
    func startBlink() {
        stopBlink()
        self.alpha = 0.0
        UIView.animate(withDuration: 0.7, delay: 0.0, options: [.curveLinear, .repeat, .autoreverse], animations: {self.alpha = 1.0}, completion: nil)
    }
    
    func stopBlink() {
        layer.removeAllAnimations()
        alpha = 1
    }
}
