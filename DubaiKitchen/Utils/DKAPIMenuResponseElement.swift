//
//	DKAPIMenuResponseElement.swift
//
//	Create by Tilak Raj Verma on 4/3/2019
//	Copyright © 2019. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class DKAPIMenuResponseElement : NSObject, NSCoding{

	var catDetails : DKAPICatDetail!
	var errorMessage : AnyObject!
	var fullImagePath : String!
	var image : AnyObject!
	var imageExtension : AnyObject!
	var imageFileName : AnyObject!
	var isActive : Bool!
	var lastDate : AnyObject!
	var menuItemDesc : String!
	var menuItemID : String!
	var menuItemName : String!
    var menuItemDesc_AR : String!
    var menuItemName_AR : String!
	var statusCode : Int!
	var successMessage : AnyObject!
	var tokenID : AnyObject!
	var tranID : Int!
	var versionNo : AnyObject!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		if let catDetailsData = dictionary["CatDetails"] as? [String:Any]{
			catDetails = DKAPICatDetail(fromDictionary: catDetailsData)
		}
		errorMessage = dictionary["ErrorMessage"] as? AnyObject
		fullImagePath = dictionary["FullImagePath"] as? String
		image = dictionary["Image"] as? AnyObject
		imageExtension = dictionary["ImageExtension"] as? AnyObject
		imageFileName = dictionary["ImageFileName"] as? AnyObject
		isActive = dictionary["IsActive"] as? Bool
		lastDate = dictionary["LastDate"] as? AnyObject
		menuItemDesc = dictionary["MenuItemDesc"] as? String
		menuItemID = dictionary["MenuItemID"] as? String
		menuItemName = dictionary["MenuItemName"] as? String
        menuItemDesc_AR = dictionary["MenuItemDesc_ar"] as? String
        menuItemName_AR = dictionary["MenuItemName_ar"] as? String
		statusCode = dictionary["StatusCode"] as? Int
		successMessage = dictionary["SuccessMessage"] as? AnyObject
		tokenID = dictionary["TokenID"] as? AnyObject
		tranID = dictionary["TranID"] as? Int
		versionNo = dictionary["VersionNo"] as? AnyObject
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if catDetails != nil{
			dictionary["CatDetails"] = catDetails.toDictionary()
		}
		if errorMessage != nil{
			dictionary["ErrorMessage"] = errorMessage
		}
		if fullImagePath != nil{
			dictionary["FullImagePath"] = fullImagePath
		}
		if image != nil{
			dictionary["Image"] = image
		}
		if imageExtension != nil{
			dictionary["ImageExtension"] = imageExtension
		}
		if imageFileName != nil{
			dictionary["ImageFileName"] = imageFileName
		}
		if isActive != nil{
			dictionary["IsActive"] = isActive
		}
		if lastDate != nil{
			dictionary["LastDate"] = lastDate
		}
		if menuItemDesc != nil{
			dictionary["MenuItemDesc"] = menuItemDesc
		}
		if menuItemID != nil{
			dictionary["MenuItemID"] = menuItemID
		}
		if menuItemName != nil{
			dictionary["MenuItemName"] = menuItemName
		}
        if menuItemDesc_AR != nil{
            dictionary["MenuItemDesc_ar"] = menuItemDesc
        }
       
        if menuItemName_AR != nil{
            dictionary["MenuItemName_ar"] = menuItemName
        }
		if statusCode != nil{
			dictionary["StatusCode"] = statusCode
		}
		if successMessage != nil{
			dictionary["SuccessMessage"] = successMessage
		}
		if tokenID != nil{
			dictionary["TokenID"] = tokenID
		}
		if tranID != nil{
			dictionary["TranID"] = tranID
		}
		if versionNo != nil{
			dictionary["VersionNo"] = versionNo
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         catDetails = aDecoder.decodeObject(forKey: "CatDetails") as? DKAPICatDetail
         errorMessage = aDecoder.decodeObject(forKey: "ErrorMessage") as? AnyObject
         fullImagePath = aDecoder.decodeObject(forKey: "FullImagePath") as? String
         image = aDecoder.decodeObject(forKey: "Image") as? AnyObject
         imageExtension = aDecoder.decodeObject(forKey: "ImageExtension") as? AnyObject
         imageFileName = aDecoder.decodeObject(forKey: "ImageFileName") as? AnyObject
         isActive = aDecoder.decodeObject(forKey: "IsActive") as? Bool
         lastDate = aDecoder.decodeObject(forKey: "LastDate") as? AnyObject
         menuItemDesc = aDecoder.decodeObject(forKey: "MenuItemDesc") as? String
         menuItemID = aDecoder.decodeObject(forKey: "MenuItemID") as? String
         menuItemName = aDecoder.decodeObject(forKey: "MenuItemName") as? String
        menuItemDesc_AR = aDecoder.decodeObject(forKey: "MenuItemDesc_ar") as? String
        menuItemName_AR = aDecoder.decodeObject(forKey: "MenuItemName_ar") as? String
         statusCode = aDecoder.decodeObject(forKey: "StatusCode") as? Int
         successMessage = aDecoder.decodeObject(forKey: "SuccessMessage") as? AnyObject
         tokenID = aDecoder.decodeObject(forKey: "TokenID") as? AnyObject
         tranID = aDecoder.decodeObject(forKey: "TranID") as? Int
         versionNo = aDecoder.decodeObject(forKey: "VersionNo") as? AnyObject

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if catDetails != nil{
			aCoder.encode(catDetails, forKey: "CatDetails")
		}
		if errorMessage != nil{
			aCoder.encode(errorMessage, forKey: "ErrorMessage")
		}
		if fullImagePath != nil{
			aCoder.encode(fullImagePath, forKey: "FullImagePath")
		}
		if image != nil{
			aCoder.encode(image, forKey: "Image")
		}
		if imageExtension != nil{
			aCoder.encode(imageExtension, forKey: "ImageExtension")
		}
		if imageFileName != nil{
			aCoder.encode(imageFileName, forKey: "ImageFileName")
		}
		if isActive != nil{
			aCoder.encode(isActive, forKey: "IsActive")
		}
		if lastDate != nil{
			aCoder.encode(lastDate, forKey: "LastDate")
		}
		if menuItemDesc != nil{
			aCoder.encode(menuItemDesc, forKey: "MenuItemDesc")
		}
		if menuItemID != nil{
			aCoder.encode(menuItemID, forKey: "MenuItemID")
		}
		if menuItemName != nil{
			aCoder.encode(menuItemName, forKey: "MenuItemName")
		}
		if statusCode != nil{
			aCoder.encode(statusCode, forKey: "StatusCode")
		}
        if menuItemDesc_AR != nil{
            aCoder.encode(menuItemDesc, forKey: "MenuItemDesc_ar")
        }
       
        if menuItemName_AR != nil{
            aCoder.encode(menuItemName, forKey: "MenuItemName_ar")
        }
        if statusCode != nil{
            aCoder.encode(statusCode, forKey: "StatusCode")
        }
		if successMessage != nil{
			aCoder.encode(successMessage, forKey: "SuccessMessage")
		}
		if tokenID != nil{
			aCoder.encode(tokenID, forKey: "TokenID")
		}
		if tranID != nil{
			aCoder.encode(tranID, forKey: "TranID")
		}
		if versionNo != nil{
			aCoder.encode(versionNo, forKey: "VersionNo")
		}

	}

}
