//
//  DashboardCVCell.swift
//  DubaiKitchen
//
//  Created by Pankaj Kumhar on 2/27/19.
//  Copyright © 2019 Pankaj Kumhar. All rights reserved.
//

import UIKit

class DashboardCVCell: UICollectionViewCell {

    @IBOutlet weak var rightimageView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgViewOptionLogo: UIImageView!
    @IBOutlet weak var lblCount: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
