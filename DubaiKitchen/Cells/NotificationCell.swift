//
//  NotificationCell.swift
//  DubaiKitchen
//
//  Created by Pankaj Kumhar on 2/23/19.
//  Copyright © 2019 Pankaj Kumhar. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {

    
    
    
    @IBOutlet weak var unread_status: UIImageView!
    @IBOutlet weak var lblOrderStatus: UILabel!
    @IBOutlet weak var lblOrderNo: UILabel!
    @IBOutlet weak var lblOrderTime: UILabel!
    @IBOutlet weak var imgViewUserType: UIImageView!
    @IBOutlet weak var imgViewright: UIImageView!
    @IBOutlet weak var lblOrderTitle: UILabel!
    @IBOutlet weak var viewMenuItems: UIView!
    @IBOutlet weak var lblMenuItemName: UILabel!
    @IBOutlet weak var lblMenuItemQty: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.IsReadStatusHidden()
        //self.unread_status.stopBlink()
    }

   
    @IBOutlet weak var btnAcceptorderWidthConstraints: NSLayoutConstraint! // 80
     @IBOutlet weak var btnAcceptorderpaddingConstraints: NSLayoutConstraint! //8
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
//    func StatusButton(_ isEnable:Bool = false)
//    {
//        btnAcceptorderWidthConstraints.constant = 0
//        btnAcceptorderpaddingConstraints.constant = 0
//        if(isEnable)
//        {
//            btnAcceptorderWidthConstraints.constant = 80
//            btnAcceptorderpaddingConstraints.constant = 8
//        }
//    }
    
    func IsOrderStatusHidden(_ isStaus:Bool = true)
    {
        self.lblOrderStatus.isHidden = isStaus
    }
    
    func IsReadStatusHidden(_ isStaus:Bool = true)
    {
        self.unread_status.isHidden = isStaus
        if(!isStaus)
        {
            DispatchQueue.main.async {
                self.unread_status.startBlink()
            }
        }
    }
    
}
