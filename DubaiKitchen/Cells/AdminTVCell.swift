//
//  AdminTVCell.swift
//  DubaiKitchen
//
//  Created by Pankaj Kumhar on 2/22/19.
//  Copyright © 2019 Pankaj Kumhar. All rights reserved.
//

import UIKit

class AdminTVCell: UITableViewCell {

    @IBOutlet weak var imgViewLogo: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgBubble: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
