//
//  OrderCell.swift
//  DubaiKitchen
//
//  Created by Pankaj Kumhar on 3/4/19.
//  Copyright © 2019 Pankaj Kumhar. All rights reserved.
//

import UIKit

class OrderCell: UITableViewCell {

    @IBOutlet weak var btnConstraints: NSLayoutConstraint!
    @IBOutlet weak var deliverydate: UILabel!
    @IBOutlet weak var orderdate: UILabel!
    @IBOutlet weak var imgViewMenuItemImage: UIImageView!
    @IBOutlet weak var lblMenuItemName: UILabel!
    @IBOutlet weak var btnstatus: UIButton!
    @IBAction func btnStatusChnageClick(_ sender: Any)
    {
        if let handler = callbackHandler
        {
            handler("order","order" as AnyObject)
        }
        changeStatus("Preparing", UIColor.init(named: "orangeColor")!)
    }
    
    var callbackHandler:CallbackHandlerWithData? = nil
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func changeStatus(_ title:String = "", _ color:UIColor = .white)
    {
        btnstatus.setTitle(title, for: .normal)
        btnstatus.backgroundColor = color
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setImageRemote(_ imagename:String = "", placeholder:String = "")
    {
        //imageright.image = UIImage.init(named: imagename)
        if(!imagename.isEmpty)
        {
            imgViewMenuItemImage.sd_setImage(with: URL(string: imagename), placeholderImage: UIImage(named: placeholder))
        } else {
            imgViewMenuItemImage.image = UIImage.init(named: placeholder)
        }
        
    }
    
    func updateWidthConstraints()
    {
        self.btnConstraints.constant = 0
        self.updateConstraintsIfNeeded()
    }
    
}
