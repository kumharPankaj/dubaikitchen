//
//  CommonView.swift
//  QRTicketing_iOS
//
//  Created by tilak.raj on 05/10/18.
//  Copyright © 2018 tilak.raj. All rights reserved.
//

import UIKit

@IBDesignable
class CommonView: UIView {
    
    public var complitionHandler:CallbackHandler? = nil
    public var complitionHandlerWithData:CallbackHandlerWithData? = nil
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup() {
        let  view = loadViewFromNib() as UIView
        view.frame = bounds
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth,
                                 UIView.AutoresizingMask.flexibleHeight]
        addSubview(view)
        self.SetupUI()
    }
    
    func loadViewFromNib() -> UIView! {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    func SetupUI()
    {
        
    }
    
}


