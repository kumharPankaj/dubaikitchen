//
//  HomeView.swift
//  DubaiKitchen
//
//  Created by tilak raj verma on 25/02/19.
//  Copyright © 2019 Pankaj Kumhar. All rights reserved.
//

import UIKit

class UserCartView: CommonView
{
    @IBOutlet weak var lblcounter: UIButton!
    @IBAction func btnminusClick(_ sender: UIButton)
    {
        
        let obj = arrUserCartItems.filter { (objCart) -> Bool in
            return objCart.menuItemID == addCartModel.menuItemID
        }
        if obj.count > 0
        {
            if obj[0].orderQty > 1
            {
                obj[0].orderQty = obj[0].orderQty-1
                //addCartModel.orderQty = addCartModel.orderQty-1
                //(convertNumberToLanguage("\(self.addCartModel.orderQty ?? 0)","ar_SA"), for: .normal)
                lblcounter.setTitle(convertNumberToLanguage("\(obj[0].orderQty ?? 0)","ar_SA"), for: .normal)
                
            }
            else
            {
                if let index = arrUserCartItems.index(where: { $0.menuItemID == addCartModel.menuItemID}){
                    arrUserCartItems.remove(at: index)
                    addCartModel.orderQty = addCartModel.orderQty-1
                }
                lblcounter.setTitle(convertNumberToLanguage("\(0)","ar_SA"), for: .normal)
                
                if let handle = complitionHandler
                {
                    handle("zero")
                }
            }
            
        }
    }
    @IBAction func btnPlusClick(_ sender: UIButton)
    {
        let obj = arrUserCartItems.filter { (objCart) -> Bool in
            return objCart.menuItemID == addCartModel.menuItemID
        }
        if obj.count > 0
        {
            if arrUserCartItems.contains(where: {$0.menuItemID == addCartModel.menuItemID})
            {
                obj[0].orderQty = obj[0].orderQty+1
                //addCartModel.orderQty = addCartModel.orderQty+1
            }
            else
            {
                arrUserCartItems.append(addCartModel)
                addCartModel.orderQty = addCartModel.orderQty+1
            }
            lblcounter.setTitle(convertNumberToLanguage("\(addCartModel.orderQty ?? 0)","ar_SA"), for: .normal)
        } else {
            arrUserCartItems.append(addCartModel)
            addCartModel.orderQty = addCartModel.orderQty+1
            lblcounter.setTitle(convertNumberToLanguage("\(addCartModel.orderQty ?? 0)","ar_SA"), for: .normal)
        }
        
    }
    
    @IBOutlet weak var viewSep: UIView!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var lbldatetime: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    var addCartModel:DKAPIOrderElement = DKAPIOrderElement()
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    @IBOutlet weak var imageright: UIImageView!
    
    func UpdateUi(_ title:String = "", _ imageName:String = "")
    {
        lblTitle.text = title
        
        if imageName.isEmpty
        {
            imageView.image = UIImage.init(named: "Cook")
        } else
        
        {
             imageView.image = UIImage.init(named: imageName)
        }
    }
    
    func setImageRight(_ imagename:String)
    {
        imageright.image = UIImage.init(named: imagename)
    }
    
    func setImageRemote(_ imagename:String = "", placeholder:String = "")
    {
        //imageright.image = UIImage.init(named: imagename)
        if(!imagename.isEmpty)
        {
            imageView.sd_setImage(with: URL(string: imagename), placeholderImage: UIImage(named: placeholder))
        } else {
            imageView.image = UIImage.init(named: placeholder)
        }
        
    }
    private func convertNumberToLanguage(_ num: String,_ locals:String = "en_US")->String
    {
        let format = NumberFormatter()
        format.locale = Locale(identifier: locals)//"ar_SA"
        if isApplanguageIsArabic{
            let number = NSNumber(value: Int(num)!)
            let faNumber = format.string(from: number)
            return faNumber!
        } else {
            return num
        }
        /*if(locals == "en_US")
        {
            let final = format.number(from: num)
            let faNumber = final//Int(final!)
            return "\(faNumber!)"
        } else if(locals == "ar_SA") {
            let number = NSNumber(value: Int(num)!)
            let faNumber = format.string(from: number)
            return faNumber!
        } else {
            return num
        }*/
    }
    func setdataObject(_ data:DKAPIOrderElement? = nil)
    {
        if let dai = data {
//            let obj = arrUserCartItems.filter { (objCart) -> Bool in
//                //return objCart.menuItemID.
//            }
            self.addCartModel = dai
//            if obj.count > 0{
//                lblcounter.setTitle("\(obj[0].orderQty ?? 0)", for: .normal)
//            } else {
                lblcounter.setTitle(convertNumberToLanguage("\(self.addCartModel.orderQty ?? 0)","ar_SA"), for: .normal)
            //}
            
        }
    }

}

class DKAPIOrderElement : NSObject, NSCoding{
    
    var deliveryDate : String!
    var deliveryHomeID : String!
    var kitchenID : String!
    var menuItemID : String!
    var orderDate : String!
    var orderID : String!
    var orderQty : Int! = 0
    var orderStatus : String!
    var userHomeID : String!
    var userID : String!
    var menuItemname : String = ""
    var menuItemname_AR : String = ""
    var userComment : String!
    var fullImagePath:String!
    var cookComment : String!
    
    override init()
    {
    
    }
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        deliveryDate = dictionary["DeliveryDate"] as? String
        deliveryHomeID = dictionary["DeliveryHomeID"] as? String
        kitchenID = dictionary["KitchenID"] as? String
        menuItemID = dictionary["MenuItemID"] as? String
        orderDate = dictionary["OrderDate"] as? String
        orderID = dictionary["OrderID"] as? String
        orderQty = dictionary["OrderQty"] as? Int
        orderStatus = dictionary["OrderStatus"] as? String
        userHomeID = dictionary["UserHomeID"] as? String
        userID = dictionary["UserID"] as? String
        fullImagePath = dictionary["FullImagePath"] as? String
         userComment = dictionary["UserComment"] as? String
        cookComment = dictionary["CookComment"] as? String
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if deliveryDate != nil{
            dictionary["DeliveryDate"] = deliveryDate
        }
        if deliveryHomeID != nil{
            dictionary["DeliveryHomeID"] = deliveryHomeID
        }
        if kitchenID != nil{
            dictionary["KitchenID"] = kitchenID
        }
        if menuItemID != nil{
            dictionary["MenuItemID"] = menuItemID
        }
        if orderDate != nil{
            dictionary["OrderDate"] = orderDate
        }
        if orderID != nil{
            dictionary["OrderID"] = orderID
        }
        if orderQty != nil{
            dictionary["OrderQty"] = orderQty
        }
        if orderStatus != nil{
            dictionary["OrderStatus"] = orderStatus
        }
        if userHomeID != nil{
            dictionary["UserHomeID"] = userHomeID
        }
        if userID != nil{
            dictionary["UserID"] = userID
        }
        if fullImagePath != nil{
            dictionary["FullImagePath"] = userID
        }
        if userComment != nil{
            dictionary["UserComment"] = userComment
        }
        if cookComment != nil{
            dictionary["CookComment"] = cookComment
        }
        
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        deliveryDate = aDecoder.decodeObject(forKey: "DeliveryDate") as? String
        deliveryHomeID = aDecoder.decodeObject(forKey: "DeliveryHomeID") as? String
        kitchenID = aDecoder.decodeObject(forKey: "KitchenID") as? String
        menuItemID = aDecoder.decodeObject(forKey: "MenuItemID") as? String
        orderDate = aDecoder.decodeObject(forKey: "OrderDate") as? String
        orderID = aDecoder.decodeObject(forKey: "OrderID") as? String
        orderQty = aDecoder.decodeObject(forKey: "OrderQty") as? Int
        orderStatus = aDecoder.decodeObject(forKey: "OrderStatus") as? String
        userHomeID = aDecoder.decodeObject(forKey: "UserHomeID") as? String
        userID = aDecoder.decodeObject(forKey: "UserID") as? String
        fullImagePath = aDecoder.decodeObject(forKey: "FullImagePath") as? String
        userComment = aDecoder.decodeObject(forKey: "UserComment") as? String
        cookComment = aDecoder.decodeObject(forKey: "CookComment") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if deliveryDate != nil{
            aCoder.encode(deliveryDate, forKey: "DeliveryDate")
        }
        if deliveryHomeID != nil{
            aCoder.encode(deliveryHomeID, forKey: "DeliveryHomeID")
        }
        if kitchenID != nil{
            aCoder.encode(kitchenID, forKey: "KitchenID")
        }
        if menuItemID != nil{
            aCoder.encode(menuItemID, forKey: "MenuItemID")
        }
        if orderDate != nil{
            aCoder.encode(orderDate, forKey: "OrderDate")
        }
        if orderID != nil{
            aCoder.encode(orderID, forKey: "OrderID")
        }
        if orderQty != nil{
            aCoder.encode(orderQty, forKey: "OrderQty")
        }
        if orderStatus != nil{
            aCoder.encode(orderStatus, forKey: "OrderStatus")
        }
        if userHomeID != nil{
            aCoder.encode(userHomeID, forKey: "UserHomeID")
        }
        if userID != nil{
            aCoder.encode(userID, forKey: "UserID")
        }
        if fullImagePath != nil{
            aCoder.encode(userID, forKey: "FullImagePath")
        }
        if userComment != nil{
            aCoder.encode(userComment, forKey: "UserComment")
        }
        if cookComment != nil{
            aCoder.encode(cookComment, forKey: "CookComment")
        }
    }
    
}
