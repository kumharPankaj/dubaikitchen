//
//  GloabelHeader.swift
//  DubaiKitchen
//
//  Created by tilak raj verma on 26/02/19.
//  Copyright © 2019 Pankaj Kumhar. All rights reserved.
//

import UIKit

class GloabelHeader: CommonView
{

    @IBAction func btnrightClick(_ sender: UIButton)
    {
        
        if let handle = complitionHandler
        {
            handle("right")
        }
    }
    
    @IBOutlet weak var btnRight: UIButton!
    @IBAction func btnBackClick(_ sender: UIButton)
    {
        
        if let handle = complitionHandler
        {
            handle("Back")
        }
    }
    
    @IBOutlet weak var btnRightPadding: NSLayoutConstraint!
    @IBOutlet weak var btnrightConstraints: NSLayoutConstraint!
    @IBOutlet weak var btnpaddingConatraints: NSLayoutConstraint! //8
    @IBOutlet weak var widthConstraints: NSLayoutConstraint! //46
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnbackClick: UIButton!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func SetupUI() {
        
        btnpaddingConatraints.constant = 0
        widthConstraints.constant = 0
        
        btnRightPadding.constant = 0
        btnrightConstraints.constant = 0
    }
    
    func updateUI(_ title:String = "",_ isbackbutton:Bool = false)
    {
        DispatchQueue.main.async {
            self.btnpaddingConatraints.constant = 0
            self.widthConstraints.constant = 0
            if (isbackbutton)
            {
                self.btnpaddingConatraints.constant = 8
                self.widthConstraints.constant = 35
            }
            self.layoutIfNeeded()
            self.lblTitle.text = title.uppercased()
        }
        
    }
    
    @IBInspectable var Headtitle: String = ""
        {
        didSet {
            lblTitle.text  = Headtitle.uppercased()
        }
    }
    
    @IBInspectable var IsbackEnable: Bool = false
        {
        didSet
        {
            btnpaddingConatraints.constant = 0
            widthConstraints.constant = 0
            if (IsbackEnable)
            {
                btnpaddingConatraints.constant = 8
                widthConstraints.constant = 35
            }
            self.updateConstraintsIfNeeded()
        }
    }
    
    
    @IBInspectable var IsRightEnable: Bool = false
        {
        didSet
        {
            btnRightPadding.constant = 0
            btnrightConstraints.constant = 0
            if (IsRightEnable)
            {
                btnRightPadding.constant = 8
                btnrightConstraints.constant = 35
            }
            self.updateConstraintsIfNeeded()
        }
    }
    
    @IBInspectable var rightImage: UIImage?
        {
        didSet {
            btnRight.setImage(rightImage, for: UIControl.State.normal)
        }
    }
}
