//
//  HomeView.swift
//  DubaiKitchen
//
//  Created by tilak raj verma on 25/02/19.
//  Copyright © 2019 Pankaj Kumhar. All rights reserved.
//

import UIKit

class HomeView: CommonView {

    @IBOutlet weak var btnWidthConstraints: NSLayoutConstraint! // 30
    @IBOutlet weak var lblHeaderTopConstraint: NSLayoutConstraint!
    @IBAction func btnDeleteClick(_ sender: UIButton)
    {
        if let handle = complitionHandlerWithData
        {
            handle(isdelete,dataObjet!)
        }
    }
    
    @IBOutlet weak var viewSep: UIView!
    
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var lbldatetime: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    var dataObjet:AnyObject? = nil
    var isdelete:String = "delete"
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func SetupUI() {
        
        self.IsActive(false)
    }
    @IBOutlet weak var imageright: UIImageView!
    
    func UpdateUi(_ title:String = "", _ imageName:String = "")
    {
        lblTitle.text = title
        
        if imageName.isEmpty
        {
            imageView.image = UIImage.init(named: "Cook")
        }
        else
        {
            if let image = UIImage.init(named: imageName)
            {
                imageView.image = image
            }
            else
            {
                imageView.image = UIImage.init(named: "placeholder_sq")
            }
            
        }
    }
    
    func setImageRight(_ imagename:String)
    {
        //imageright.isHidden = true
        //imageright.image = UIImage.init(named: imagename)
    }
    
    func setData(_ datain:AnyObject? = nil)
    {
        dataObjet = datain
    }

    func setImageRemote(_ imagename:String = "", placeholder:String = "placeholder_sq")
    {
        //imageright.image = UIImage.init(named: imagename)
        if(!imagename.isEmpty)
        {
            imageView.sd_setImage(with: URL(string: imagename), placeholderImage: UIImage(named: placeholder))
        } else {
            imageView.image = UIImage.init(named: placeholder)
        }
        
    }
    
    func setLabeldateTime(_ string:String = "")
    {
        self.lbldatetime.text = string
    }
    
    func IsActive(_ active:Bool)
    {
        
        if(active)
        {
            isdelete = "active"
            self.btnWidthConstraints.constant = 100
            self.btnDelete.setTitle(NSLocalizedString("Active", comment:""), for: .normal)
            self.btnDelete.setImage(nil, for: .normal)
            self.btnDelete.backgroundColor = UIColor.init(named: "blueColor")
        }
        else{
            isdelete = "delete"
            self.btnWidthConstraints.constant = 30
            self.btnDelete.backgroundColor = UIColor.clear
            self.btnDelete.setTitle(NSLocalizedString("Active", comment:""), for: .normal)
            self.btnDelete.setImage(UIImage.init(named: "delete"), for: .normal)
        }
        self.updateConstraintsIfNeeded()
        //
    }
}
