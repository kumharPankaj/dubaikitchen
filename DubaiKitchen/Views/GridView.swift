//
//  GridView.swift
//  TMC_APP
//
//  Created by tilak raj verma on 09/02/19.
//  Copyright © 2019 tilak.raj. All rights reserved.
//

import UIKit


class GridView: CommonView
{

    @IBAction func btngridclick(_ sender: Any)
    {
        if(complitionHandlerWithData != nil)
        {
            complitionHandlerWithData!("",dataIn as AnyObject)
        }
    }
    
    @IBOutlet weak var lbltotle: UILabel!
    @IBOutlet weak var lbltext: UILabel!
    @IBOutlet weak var imageview: UIImageView!
    @IBOutlet weak var btngrid: UIButton!
    var dataIn:MenuListItem?
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func SetupUI()
    {
        
    }
    
    func addimage(_ data:MenuListItem)
    {
        dataIn = data
        if(!data.imagename.isEmpty)
        {
            imageview.image = UIImage.init(named: data.imagename)
        }
     
        imageview.image = UIImage.init(named: "Management")
        lbltotle.text =  data.name
    }
    
    @IBInspectable var image: UIImage?
        {
        didSet {
            
             imageview.image = image
        }
    }
    
    @IBInspectable var titletext: String = ""
        {
        didSet {
            lbltotle.text  = titletext
        }
    }
}
