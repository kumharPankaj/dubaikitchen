//
//  ViewDescription.swift
//  DubaiKitchen
//
//  Created by tilak raj verma on 03/03/19.
//  Copyright © 2019 Pankaj Kumhar. All rights reserved.
//

import UIKit
import UITextView_Placeholder
class ViewDescription: CommonView
{
    @IBOutlet weak var txtView: UITextView!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func SetupUI() {
        txtView.placeholder = NSLocalizedString("Special Instruction", comment:"")
        txtView.placeholderColor = UIColor.lightGray
        txtView.text = ""
    }
    
    func gettext() ->String?
    {
        return txtView.text.length > 0 ? txtView.text : "''"
    }
    
    func setuserintraction(_ isemable:Bool = true )
    {
        self.txtView.isUserInteractionEnabled = isemable
    }
    func settextstr(_ isemable:String = "" )
    {
        self.txtView.text = isemable
    }
    
    func setplaceHolder(_ isemable:String = "")
    {
         txtView.placeholder = isemable
    }

}

class ViewDateTime: CommonView
{
    
    @IBOutlet weak var txtAM_PM: UITextField!
    @IBOutlet weak var txtMM: UITextField!
    @IBOutlet weak var txtHH: UITextField!
    @IBOutlet weak var txtMonth: UITextField!
    @IBOutlet weak var txtDD: UITextField!
    var datepicker = UIDatePicker()
    var selectedDate:String = ""
    var orderdate:String = ""
    override func SetupUI()
    {
       datepicker.datePickerMode = .dateAndTime
        txtAM_PM.inputView = datepicker
        txtMM.inputView = datepicker
        txtHH.inputView = datepicker
        txtMonth.inputView = datepicker
        txtDD.inputView = datepicker
         datepicker.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
        datepicker.minimumDate = Date()
        //handleDatePicker(sender: datepicker)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        orderdate = dateFormatter.string(from: Date())
        
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
       // selectedDate = dateFormatter.string(from: Date())
        txtDD.text = ""
        txtDD.text = ""
        txtMonth.text = ""
        txtHH.text = ""
        txtMM.text = ""
        txtAM_PM.text = ""
    }
    
    @objc func handleDatePicker(sender: UIDatePicker)
    {
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        selectedDate = dateFormatter.string(from: sender.date)
        
        dateFormatter.dateFormat = "dd"
        txtDD.text = dateFormatter.string(from: sender.date)
        
        dateFormatter.dateFormat = "MMM"
        txtMonth.text = dateFormatter.string(from: sender.date)
        
        dateFormatter.dateFormat = "hh"
        txtHH.text = dateFormatter.string(from: sender.date)
        
        
        dateFormatter.dateFormat = "mm"
        txtMM.text = dateFormatter.string(from: sender.date)
        
        dateFormatter.dateFormat = "a"
        txtAM_PM.text = (dateFormatter.string(from: sender.date)).uppercased()
    }
}
