//
//  GridContainerView.swift
//  TMC_APP
//
//  Created by tilak raj verma on 09/02/19.
//  Copyright © 2019 tilak.raj. All rights reserved.
//

import UIKit

class GridContainerView: CommonView {

    @IBOutlet weak var gview1: GridView!
    @IBOutlet weak var gview2: GridView!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func SetupUI()
    {
        gview1.complitionHandlerWithData = { name,data in
            if(self.complitionHandlerWithData != nil)
            {
                self.complitionHandlerWithData!("",data)
            }
        }
        
        gview2.complitionHandlerWithData = { name,data in
            if(self.complitionHandlerWithData != nil)
            {
                self.complitionHandlerWithData!("",data as AnyObject)
            }
        }
    }
    
    func updateUI(_ param:MenuListItem, _ param1:MenuListItem)
    {
        gview1.addimage(param)
        gview2.addimage(param1)
        gview2.isHidden = false
        if(param1.imagename.isEmpty) {gview2.isHidden = true}
    }

}
