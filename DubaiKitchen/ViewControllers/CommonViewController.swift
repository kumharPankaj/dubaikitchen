//
//  CommonViewController.swift
//  EaseMyStay
//
//  Created by tilak raj verma on 04/09/18.
//  Copyright © 2018 Tilak Raj Verma. All rights reserved.
//

import UIKit

class CommonViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    
    @IBOutlet weak var gloablHeaderView: GloabelHeader!
    @IBOutlet weak var tblView: UITableView!
    var userType : USERTYPE = .DEFAULT
    private var dictmenues = [String:DKAPIMenuResponseElement]()
    @IBAction func onMenuClick(_ sender: UIButton)
    {
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        //self.view.layer.contents = UIImage(named: "Layer.jpg")?.cgImage
        // Do any additional setup after loading the view.
        self.navigationItem.backBarButtonItem?.title = ""
        self.navigationController?.navigationBar.tintColor = UIColor.black
        self.navigationController?.navigationItem.backBarButtonItem?.title = ""
        if tblView != nil{
            tblView.indicatorStyle = .black
            tblView.showsVerticalScrollIndicator = true
        }
    }

    func isNavigationBarRequire(_ isNeed:Bool = true, _ isLeftBtnNeeded:Bool = true)
    {
        if(isNeed)
        {
            self.UpdateNavigationBar()
            self.customizeLeftNavigationBar(false, isLeftBtnNeeded)
        }
        else
        {
        }
    }
    
    func updateNavTitle(_ title:String = "", _ isback:Bool=false, _ handle:CallbackHandler? = nil)
    {
        DispatchQueue.main.async {
            if let head = self.gloablHeaderView
            {
                head.updateUI(title,isback)
                head.complitionHandler = handle
            }
        }
        
    }
    
    func UpdateNavigationBar()
    {
        addGradientColors_Nav()
    }
    
    func UpdateNavigationBarAdmin()
    {
        addGradientColors_Nav()
    }
    
    func customizeLeftNavigationBarAdmin(){
        let image = #imageLiteral(resourceName: "logout_1")
      let leftBarButtonItem = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(clickadminBarButtonItem(_:)))
        //leftBarButtonItem. = UIColor.white
        self.navigationItem.rightBarButtonItem  = leftBarButtonItem
    }
    
    func customizeLeftNavigationBar(_ isback:Bool = false, _ isLeftBtnNeeded:Bool = true)
    {
         var leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_icon"), style: .plain, target: self, action: #selector(clickleftBarButtonItem(_:)))
        if(isback)
        {
            leftBarButtonItem = UIBarButtonItem(image:#imageLiteral(resourceName: "back_nav"), style: .plain, target: self, action: #selector(clickleftBarButtonItem(_:)))
        }
        if isLeftBtnNeeded{
            self.navigationItem.leftBarButtonItem  = leftBarButtonItem
        }
        
    }
    
   
    @objc func clickleftBarButtonItem(_ isMenu:Bool = true)
    {
    }
    
    @objc func clickadminBarButtonItem(_ isMenu:Bool = true)
    {
            print("Logout click")
        
        let alertController = UIAlertController(title:NSLocalizedString("Alert!", comment:"") , message: NSLocalizedString("Are you sure you want to logout?", comment:""), preferredStyle:.alert)
        let firstAction = UIAlertAction(title: NSLocalizedString("Ok", comment:""), style: .default) { (alert: UIAlertAction!) -> Void in
            UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
            UserDefaults.standard.synchronize()
            alertController.dismiss(animated: true, completion: nil)
        }
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment:""), style: .cancel) { (alert: UIAlertAction!) -> Void in
            alertController.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(firstAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension UIViewController
{
    func getAllOrderByOrderId(callback:WebCallbackHandler? = nil)
    {
        NewtworkManager.shared.GetAllOrderByOrderId(notificationOrderID)
        { (status, dataIn) in
            if(status)
            {
                var arra = dataIn as! [DKAPIOrderElement]
                
            }
            callback!(status,dataIn)
        }
    }
    
    func GetallmenusNames(_ callback:WebCallbackHandler? = nil)
    {
        self.showLoader()
        NewtworkManager.shared.GetAllActiveInactiveMenuDetails { (status, dataIn) in
            self.hideloader()
            if (status)
            {
                let menuArray = (dataIn as? [DKAPIMenuResponseElement])!
                var dictmenues = [String:DKAPIMenuResponseElement]()
                for objOrder in menuArray
                {
                    if(dictmenues[objOrder.menuItemID] == nil)
                    {
                        dictmenues[objOrder.menuItemID] = objOrder
                    }
                }
                
                callback!(status,dictmenues as AnyObject)
            }
            else
            {
                callback!(status,dataIn)
            }
        }
    }
}
