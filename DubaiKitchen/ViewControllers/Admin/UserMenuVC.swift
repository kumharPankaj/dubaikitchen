//
//  OrderVC.swift
//  DubaiKitchen
//
//  Created by Pankaj Kumhar on 2/28/19.
//  Copyright © 2019 Pankaj Kumhar. All rights reserved.
//

import UIKit

class UserMenuVC: CommonViewController {

    private var categoryArray:[DKAPICatDetail] = [DKAPICatDetail]()
    var arrPickerContect = [NSLocalizedString("Main Course", comment:""), NSLocalizedString("Starters", comment:""), NSLocalizedString("Salad", comment:""), NSLocalizedString("Drinks", comment:""), NSLocalizedString("Deserts", comment:"")]
    var arrPickerContectId = [1, 2, 3, 4, 5]
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = NSLocalizedString("Menu", comment:"")
        //addGradientColors_Nav()
        self.updateNavTitle(NSLocalizedString("Menu", comment:""), true) { (name) in
            if(name == "right")
            {
                return
            }
             DispatchQueue.main.async {
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: USerVC.self) {
                        _ =  self.navigationController!.popToViewController(controller, animated: false)
                        break
                    }
                }
                        //self.navigationController?.popViewController(animated: false)
                    }
        }
        tblView.register(UINib(nibName: "AdminTVCell", bundle: nil), forCellReuseIdentifier: "cellAdminCommon")
        tblView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
        apicall()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.categoryArray.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:AdminTVCell  = (tableView.dequeueReusableCell(withIdentifier: "cellAdminCommon", for: indexPath) as? AdminTVCell)!
        let model =  self.categoryArray[indexPath.row]
        //cell.imgBubble.image = UIImage.init(named: "layer_\(indexPath.row+1)")
      cell.imgBubble.isHidden = true
        if let keyGuest  = UserDefaults.standard.value(forKey: defaultsKeys.langSelected) {
            if keyGuest as! String == ARABIC {
                if model.catName_AR.length > 0{
                    cell.lblTitle.text = model.catName_AR
                } else {
                     cell.lblTitle.text = model.catName
                }
                
            } else {
                 cell.lblTitle.text = model.catName
            }
        } else {
             cell.lblTitle.text = model.catName
        }
        if let iimage = UIImage.init(named: "cat_\(model.catID!)")
        {
          cell.imgViewLogo.image = iimage
        }
        else
        {
            cell.imgViewLogo.sd_setImage(with: URL(string: model.catImageFullPath), placeholderImage: UIImage(named: "placeholder_sq"))
            //viewCOnatiner.setImageRemote(model.fullImagePath, placeholder: "Menu")
            //cell.imgViewLogo.image = UIImage.init(named: "placeholder_sq")
        }
        
        return cell
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        //let text = arrPickerContect[indexPath.row]
        //let ids = arrPickerContectId[indexPath.row]
        let model =  self.categoryArray[indexPath.row]
        let notiVc = self.storyboard?.instantiateViewController(withIdentifier: "UserMycartDetailsVC") as! UserMycartDetailsVC
        
        notiVc.catagoryId = Int(model.catID)!
        if isApplanguageIsArabic{
            notiVc.catagoryname = model.catName_AR.isEmptyAndNil == false ? model.catName_AR : model.catName
        } else {
            notiVc.catagoryname = model.catName
        }
        
        self.navigationController?.pushViewController(notiVc, animated: false)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    func apicall()
    {
        self.showLoader()
        NewtworkManager.shared.GetAllCategoryDetails() { (status, dataIn) in
            self.hideloader()
            if (status)
            {
                self.categoryArray = (dataIn as? [DKAPICatDetail])!
                DispatchQueue.main.async {
                    self.tblView.reloadData()
                }
            }
        }
        
    }
}
