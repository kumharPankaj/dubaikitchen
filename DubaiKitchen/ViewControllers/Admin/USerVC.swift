//
//  AdminVC.swift
//  DubaiKitchen
//
//  Created by Pankaj Kumhar on 2/22/19.
//  Copyright © 2019 Pankaj Kumhar. All rights reserved.
//

import UIKit

class USerVC: CommonViewController {
    var gameTimer: Timer!
    override func viewDidLoad() {
        super.viewDidLoad()
        getAllUsers()
        //addGradientColors_Nav()
        //self.navigationController?.navigationBar.isHidden = false
        switch selectedUserRole{
        case 1:
            self.updateNavTitle(NSLocalizedString("Admin", comment:""))
        case 2:
            self.updateNavTitle(NSLocalizedString("User", comment:""))
        case 3:
            self.updateNavTitle(NSLocalizedString("Cook", comment:""))
        default:
            print("")
        }
        //self.title = "Admin"
        
        tblView.register(UINib(nibName: "AdminTVCell", bundle: nil), forCellReuseIdentifier: "cellAdminCommon")
        tblView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
        let dispatchQueue = DispatchQueue(label: "QueueIdentification", qos: .background)
        dispatchQueue.async
            {
            self.apicall()
        }
        
        DispatchQueue.global(qos: .background).async {
           
            self.getReadunreadMessages()
//            DispatchQueue.main.async {
//                print("This is run on the main queue, after the previous code in outer block")
//
//            }
        }
        
        gameTimer = Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(runTimedCode), userInfo: nil, repeats: true)
    }
    
    @objc func runTimedCode()
    {
        DispatchQueue.global(qos: .background).async {
            
            self.getReadunreadMessages()
            //            DispatchQueue.main.async {
            //                print("This is run on the main queue, after the previous code in outer block")
            //
            //            }
        }
    }
    
    func getReadunreadMessages()
    {
        arrCookComment.removeAll()
        //self.showLoader()
        NewtworkManager.shared.GetAllUnreadComments() { (status, datain) in
           // self.hideloader()
            if(status)
            {
                let arr = datain as? [DKAPIOrderElement]
                if let arrrorder = arr
                {
                   arrCookComment = [String]()
                    for orderIn in arrrorder
                    {
                        if(!arrCookComment.contains(orderIn.orderID))
                       {arrCookComment.append(orderIn.orderID)}
                    }
                    
                    NotificationCenter.default.post(name: .didReceiveData, object: nil)
                }
            }
        }
    }
    
    func apicall()
    {
        //
        self.showLoader()
        NewtworkManager.shared.GetUserDetailsByID(NewtworkManager.shared.logoinUserid) { (status, dataIn) in
            self.hideloader()
            if (status)
            {
                if (Int(notificationOrderID) != -1)
                {
                    self.Navigate()
                }
            }
        }
    }
    
    func Navigate()
    {
        let topController = self
        
            topController.showLoader()
            topController.getAllOrderByOrderId(callback: { (status, datain) in
                topController.hideloader()
                if status
                {
                    let arr  = datain as! [DKAPIOrderElement]
                    let orderByKitchen = arr[0]
                    if orderByKitchen.orderStatus == "Order Placed"
                    {
                        arrUserCartItems = arr
                        topController.showLoader()
                        topController.GetallmenusNames({ (status1, datain1) in
                            topController.hideloader()
                            
                            if(status1)
                            {
                                var dictmenues = datain1 as![String:DKAPIMenuResponseElement]
                                for model in arrUserCartItems
                                {
                                    let menuitem = dictmenues[model.menuItemID]
                                    if (menuitem != nil)
                                    {
                                        model.menuItemname = (menuitem?.menuItemName)!
                                        model.menuItemname_AR = (menuitem?.menuItemName_AR)!
                                    }
                                }
                                
                                DispatchQueue.main.async
                                    {
                                        /*notificationOrderID = "-1"
                                        let notiVc = topController.storyboard?.instantiateViewController(withIdentifier: "UserMycartVC") as! UserMycartVC
                                        notiVc.IsComeFromNotifications = true
                                        topController.navigationController?.pushViewController(notiVc, animated: false)*/
                                        let notiVc = topController.storyboard?.instantiateViewController(withIdentifier: "OrderDetailsVC") as! OrderDetailsVC
                                        notiVc.arrOrderByKitchen = arr
                                        notiVc.isfromuser = true
                                        notiVc.isfromallOrder = true
                                        topController.navigationController?.pushViewController(notiVc, animated: false)
                                }
                            }
                        })
                    }
                    else
                    {
                        DispatchQueue.main.async
                            {
                                notificationOrderID = "-1"
                                let notiVc = topController.storyboard?.instantiateViewController(withIdentifier: "OrderDetailsVC") as! OrderDetailsVC
                                notiVc.arrOrderByKitchen = arr
                                notiVc.isfromuser = true
                                topController.navigationController?.pushViewController(notiVc, animated: false)
                        }
                    }
                    
                }
                else {
                    notificationOrderID = "-1"
                }
            })
            
            
        
    }
    
    private func getAllUsers()
    {
        self.showLoader()
        NewtworkManager.shared.GetAllActiveInActiveUserDetails { (status, dataIn) in
            self.hideloader()
            if (status)
            {
                arrGlobalActiveInactiveUsers = (dataIn as? [DKAPIUserResponseElement])!
                if NewtworkManager.shared.logoinUserType == NewtworkManager.shared.CookLogOn ||  NewtworkManager.shared.logoinUserType == NewtworkManager.shared.KichtenLogOn
                {
                    if (Int(notificationOrderID) != -1)
                    {
                        self.Navigate()
                    }
                }
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:AdminTVCell  = (tableView.dequeueReusableCell(withIdentifier: "cellAdminCommon", for: indexPath) as? AdminTVCell)!
        
        //cell.imgBubble.image = UIImage.init(named: "layer_\(indexPath.row+1)")
        cell.imgBubble.isHidden = true
        switch indexPath.row
        {
        case 3:
            
            cell.lblTitle.text = NSLocalizedString("Logout", comment:"")
            cell.imgViewLogo.image = UIImage.init(named: "Logout")?.maskWithColor(color: UIColor.init(hexString: "#5E5E5E")!)
        case 0:
                cell.lblTitle.text = NSLocalizedString("Menu", comment:"")
                cell.imgViewLogo.image = UIImage.init(named: "Menu")?.maskWithColor(color: UIColor.init(hexString: "#5E5E5E")!)
          
        case 2:

            cell.lblTitle.text = NSLocalizedString("Notification", comment:"")
            cell.imgViewLogo.image = UIImage.init(named: "notification")?.maskWithColor(color: UIColor.init(hexString: "#5E5E5E")!)
            
        case 1:
            cell.lblTitle.text = NSLocalizedString("My Cart", comment:"")
            cell.imgViewLogo.image = UIImage.init(named: "MyCart")?.maskWithColor(color: UIColor.init(hexString: "#5E5E5E")!)
        
        default:
            print("")
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedFooterOption = indexPath.row + 1
        switch indexPath.row {
        case 3:
            print("logout")
            DispatchQueue.main.async
                {
                selectedFooterOption = -1
                    isUserLoggedIn = false
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                
                let initialViewController = storyboard.instantiateViewController(withIdentifier: "initialnavigationviewcontroller")
                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                appdelegate.window?.rootViewController = initialViewController
            }
        case 0:
            let notiVc = self.storyboard?.instantiateViewController(withIdentifier: "UserMenuVC") as! UserMenuVC
            self.navigationController?.pushViewController(notiVc, animated: false)
            
        case 1:
            let notiVc = self.storyboard?.instantiateViewController(withIdentifier: "UserMycartVC") as! UserMycartVC
            self.navigationController?.pushViewController(notiVc, animated: false)
           
        case 2:
            let notiVc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
            notiVc.titleNavigation = "user"
            self.navigationController?.pushViewController(notiVc, animated: false)
        default:
            print("")
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
