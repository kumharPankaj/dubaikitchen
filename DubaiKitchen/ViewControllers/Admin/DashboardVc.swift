//
//  DashboardVc.swift
//  DubaiKitchen
//
//  Created by Pankaj Kumhar on 2/22/19.
//  Copyright © 2019 Pankaj Kumhar. All rights reserved.
//

import UIKit
//

enum TYPEOFNAVIGATION
{
    case Daily_Collection
    case Prabhagwise_Demand
    case Prabhagwise_Collection
    case Top_Defaulters
    case per_wise_Collection
    case Collection_Graph
    case Legal_Illegal
    case Collection_Summary
    case Res_and_NonRes
    case complaint_summary
    case complaint_info
    case complaint_info_details
    case complaint_graph
    case complaint_per_graph
    case escalation_chart
    case Default
}
struct MenuListItem{
    var itemIndex : Int
    var name : String
    var imagename : String
    var dataCount:Int = 0
    var navtype:TYPEOFNAVIGATION = .Default
    init(itemIndex : Int,name : String,imagename : String,_ dataCount:Int = 0,_ navtype:TYPEOFNAVIGATION = .Default)
    {
        self.itemIndex = itemIndex
        self.name = name
        self.imagename = imagename
        self.dataCount = dataCount
        self.navtype = navtype
    }
}
class DashboardVc: CommonViewController
{
    var menuList2: [MenuListItem]?
    @IBOutlet weak var cvDashboard: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        cvDashboard.register(UINib(nibName: "DashboardCVCell", bundle: nil), forCellWithReuseIdentifier: "cellDashboard")
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        //layout.itemSize = CGSize(width: screenWidth/3, height: screenWidth/3)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        cvDashboard.collectionViewLayout = layout
        self.updateNavTitle(NSLocalizedString("Dashboard", comment:""), true) { (name) in
            if(name == "right")
            {
                return
            }
             DispatchQueue.main.async {
                
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: AdminVC.self) {
                        _ =  self.navigationController!.popToViewController(controller, animated: false)
                        break
                    }
                }
//                        self.navigationController?.popViewController(animated: false)
                    }
        }
        
        if selectedUserRole == 3 {
            menuList2 = [
            MenuListItem(itemIndex: 1, name: NSLocalizedString("Total Orders", comment:"") ,imagename : "MyCart",0,TYPEOFNAVIGATION.complaint_summary)
            , MenuListItem(itemIndex: 2, name: NSLocalizedString("Pending Orders", comment:""),imagename : "MyCart",0,TYPEOFNAVIGATION.complaint_info)
            , MenuListItem(itemIndex: 3, name: NSLocalizedString("Order Accepted", comment:""),imagename : "MyCart",0,TYPEOFNAVIGATION.complaint_graph) ,
            MenuListItem(itemIndex: 4, name: NSLocalizedString("Cancelled Orders", comment:""),imagename : "MyCart",0,TYPEOFNAVIGATION.complaint_per_graph)
            , MenuListItem(itemIndex: 5, name: NSLocalizedString("Total Users", comment:""),imagename : "user_icon",0,TYPEOFNAVIGATION.escalation_chart),
            MenuListItem(itemIndex: 6, name: NSLocalizedString("Total Houses", comment:""),imagename : "house")
            ]
        } else if selectedUserRole == 1{
            menuList2 = [
            MenuListItem(itemIndex: 1, name: NSLocalizedString("Total Users", comment:"") ,imagename : "user_icon",0,TYPEOFNAVIGATION.complaint_summary)
            , MenuListItem(itemIndex: 2, name: NSLocalizedString("Total Cook", comment:""),imagename : "Cook",0,TYPEOFNAVIGATION.complaint_info)
            , MenuListItem(itemIndex: 3, name: NSLocalizedString("Total Houses", comment:""),imagename : "house",0,TYPEOFNAVIGATION.complaint_graph) ,
            MenuListItem(itemIndex: 4, name: NSLocalizedString("Menu List", comment:""),imagename : "MenuList",0,TYPEOFNAVIGATION.complaint_per_graph)
            , MenuListItem(itemIndex: 5, name: NSLocalizedString("Order Request", comment:""),imagename : "order_requested",0,TYPEOFNAVIGATION.escalation_chart),
            MenuListItem(itemIndex: 6, name: NSLocalizedString("Order Accept", comment:""),imagename : "Order-accepted")
            ]
            
        }
        getDashboardData()
        //tblView.reloadData()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        cvDashboard.reloadData()
    }


    private func getDashboardData(){
        self.showLoader()
        if selectedUserRole == 3 {
            NewtworkManager.shared.GetDashBoardForAdmin { (status, dataIn) in
                self.hideloader()
                if (status)
                {
                    if let dataObj = dataIn as? DKAPIAdminDashboradResponse{
                        
                        
                        self.menuList2?[0].dataCount = dataObj.totalOrder
                        self.menuList2?[1].dataCount = dataObj.totalPendingorder
                        self.menuList2?[2].dataCount = dataObj.totalAccecptedorder
                        self.menuList2?[3].dataCount = dataObj.totalCancelledorder
                        self.menuList2?[4].dataCount = dataObj.totalUser
                        self.menuList2?[5].dataCount = dataObj.totalHouse
                        DispatchQueue.main.async {
                            self.cvDashboard.reloadData()
                        }
                    }
                }
            }
        } else {
            NewtworkManager.shared.GetDashBoardForAdmin { (status, dataIn) in
                self.hideloader()
                if (status)
                {
                    if let dataObj = dataIn as? DKAPIAdminDashboradResponse {
                        self.menuList2?[0].dataCount = dataObj.totalUser
                        self.menuList2?[1].dataCount = dataObj.totalCook
                        self.menuList2?[2].dataCount = dataObj.totalHouse
                        self.menuList2?[3].dataCount = dataObj.menuList
                        self.menuList2?[4].dataCount = dataObj.totalOrderRequest
                        self.menuList2?[5].dataCount = dataObj.totalAccecptedorder
                        DispatchQueue.main.async {
                            self.cvDashboard.reloadData()
                        }
                        
                    }
                    
                }
            }
        }

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension DashboardVc: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menuList2?.count ?? 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellDashboard", for: indexPath) as! DashboardCVCell
        cell.lblTitle.text = menuList2?[indexPath.item].name
        cell.imgViewOptionLogo.image = UIImage.init(named: (menuList2?[indexPath.item].imagename)!)
        
        if isApplanguageIsArabic{
            cell.lblCount.text = convertNumberToLanguage("\(menuList2?[indexPath.item].dataCount ?? 0)","ar_SA") 
        } else {
            cell.lblCount.text = "\(menuList2?[indexPath.item].dataCount ?? 0)"
        }
        
        //cell.rightimageView.image = UIImage.init(named: "dslayer_\(indexPath.item)")
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
            return CGSize.init(width: cvDashboard.frame.width/2, height: (cvDashboard.frame.height/3))
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
}
