//
//  AddHouseViewController.swift
//  DubaiKitchen
//
//  Created by tilak raj verma on 26/02/19.
//  Copyright © 2019 Pankaj Kumhar. All rights reserved.
//

import UIKit

class AddHouseViewController: CommonViewController
    {
    var arritems:DKAPIOrderElement!
    var tappedMenuItem: DKAPIMenuResponseElement!
    var isFromAdmin = false
    override func viewDidLoad()
    {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        
        self.view.addGestureRecognizer(tap)
        
        self.view.isUserInteractionEnabled = true
        self.view.backgroundColor = UIColor.init().hexStringToUIColor(hex: "#000000",0.5)
        self.tblView.tableFooterView = UIView()
       
        // Do any additional setup after loading the view.
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 2
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if(indexPath.row == 0)
        {
            return 180
        }
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if(indexPath.row == 0)
        {
            let celldes = tblView.dequeueReusableCell(withIdentifier: "cellimage") as? UITableViewCell
            let imageView = celldes?.viewWithTag(100) as! UIImageView
            if(arritems.fullImagePath != nil)
            {
                imageView.sd_setImage(with: URL(string: arritems.fullImagePath!), placeholderImage: UIImage(named: "placeholder_rec"))
            }
            else {
                imageView.image = UIImage.init(named: "placeholder_rec")
            }
            return celldes!
        }
        else
        {
            let celldes = tblView.dequeueReusableCell(withIdentifier: "celldes") as? UITableViewCell
            let label = celldes?.viewWithTag(100) as! UILabel
            
            label.text = arritems.userComment
            if isApplanguageIsArabic {
                label.text = arritems.cookComment
            }
            return celldes!
        }
        return UITableViewCell()
    }
    // function which is triggered when handleTap is called
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        self.dismiss(animated: true) {
            
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
