//
//  ManagementVC.swift
//  DubaiKitchen
//
//  Created by Pankaj Kumhar on 2/23/19.
//  Copyright © 2019 Pankaj Kumhar. All rights reserved.
//

import UIKit

class ManagementVC: CommonViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

       
        //self.navigationController?.navigationBar.isHidden = false
        self.title = NSLocalizedString("Management", comment:"")
        self.updateNavTitle(NSLocalizedString("Management", comment:""), true) { (name) in
            if(name == "right")
            {
                return
            }
             DispatchQueue.main.async {
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: AdminVC.self) {
                        _ =  self.navigationController!.popToViewController(controller, animated: false)
                        break
                    }
                }
                        //self.navigationController?.popViewController(animated: false)
                    }
        }
        tblView.register(UINib(nibName: "AdminTVCell", bundle: nil), forCellReuseIdentifier: "cellAdminCommon")
        tblView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
   
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:AdminTVCell  = (tableView.dequeueReusableCell(withIdentifier: "cellAdminCommon", for: indexPath) as? AdminTVCell)!
        //cell.imgBubble.image = UIImage.init(named: "layer_\(indexPath.row+1)")
        cell.imgBubble.isHidden = true
        switch indexPath.row {
        case 3:
            cell.lblTitle.text = NSLocalizedString("Menu", comment:"")
            cell.imgViewLogo.image = UIImage.init(named: "MenuList")
        case 4:
            cell.lblTitle.text = NSLocalizedString("Category", comment:"")
            cell.imgViewLogo.image = UIImage.init(named: "menu_cat")
        case 0:
            cell.lblTitle.text = NSLocalizedString("House", comment:"")
            cell.imgViewLogo.image = UIImage.init(named: "house")
        case 1:
            cell.lblTitle.text = NSLocalizedString("User", comment:"")
            cell.imgViewLogo.image = UIImage.init(named: "user_icon")
        case 2:
            cell.lblTitle.text = NSLocalizedString("Cook", comment:"")
            cell.imgViewLogo.image = UIImage.init(named: "Cook")
            
        default:
            print("")
        }
        return cell
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 80
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var userType:USERTYPE = .DEFAULT
        switch indexPath.row {
        case 3:
          userType = .MENU
        case 4:
            userType = .CATEGORY
        case 0:
            userType = .HOUSE
        case 1:
            userType = .USER
        case 2:
           userType = .COOK
        default:
            print("")
        }
        
        let notiVc = self.storyboard?.instantiateViewController(withIdentifier: "HouseDetailsVC") as! HouseDetailsVC
        notiVc.userType = userType
        self.navigationController?.pushViewController(notiVc, animated: false)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
