//
//  EditFormVC.swift
//  DubaiKitchen
//
//  Created by Pankaj Kumhar on 3/10/19.
//  Copyright © 2019 Pankaj Kumhar. All rights reserved.
//

import UIKit

class EditFormVC: CommonViewController , UIImagePickerControllerDelegate, UINavigationControllerDelegate{

    public var complitionHandler:WebCallbackHandler? = nil
    private let toolbar = UIToolbar()
    private var strFormTitle = ""
    private var arrFormFields = [FORM_FIELD.ADD]
    private var currentTextField =  UITextField()
    private let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(CommonFormVC.dismissKeyboard))
    private var name = ""
    private var nameArabic = ""
    private var house = ""
    private var kitchen = ""
    private var location = ""
    private var phone = ""
    private var password = ""
    private var email = ""
    private var dropDownValue = ""
    private var descriptionText = ""
    private var descriptionTextArabic = ""
    private var arrPickerContect = ["Main Course", "Starters", "Salad", "Drinks", "Deserts"]
    private var arrPickerContectId = [1, 2, 3, 4, 5]
    private var selectedCategoryID = 1
    private var selectedButtonTag = 0
    @IBOutlet weak var selectionPickerView: UIPickerView!
    @IBOutlet weak var constPickerViewHeight: NSLayoutConstraint!
    private let imagePicker = UIImagePickerController()
    private var houseArray:[DKAPIHomeResponseElement] = [DKAPIHomeResponseElement]()
    private var selectedHouseObj = DKUserHomeDetails()
    private var selectedImageString = ""
    private var kitchenArray:[DKAPIKitchenRespone] = [DKAPIKitchenRespone]()
    var selectedDetailsID = ""
    var selectedUseDetails: DKAPIEditUserResponse!
    var selectedCookDetails: DKAPIEditCookResponse!
    let dispatchGroup = DispatchGroup()
    override func viewDidLoad() {
        super.viewDidLoad()
        addToolBar()
        tblView.beginWatchingForKeyboardStateChanges()
        selectionPickerView.showsSelectionIndicator = true
        imagePicker.delegate = self
        tblView.tableHeaderView = UIView.init(frame: CGRect.init(x: 0.0, y: 0.0, width: 20, height: 44))
        //self.navigationController?.navigationBar.isHidden = false
        
        
        switch userType {
        case .COOK:
            self.title = NSLocalizedString("Update Cook", comment:"")
            arrFormFields = [FORM_FIELD.COOK_NAME, FORM_FIELD.COOK_NAME_ARABIC, FORM_FIELD.ASSIGN_KITCHEN, FORM_FIELD.EMAIL,FORM_FIELD.PHONE, FORM_FIELD.PASSWORD]
            // arrFormFields = [FORM_FIELD.COOK_NAME, FORM_FIELD.ASSIGN_HOUSE, FORM_FIELD.ASSIGN_KITCHEN]
            dispatchGroup.enter()
            self.getDetailsByID()
            
            dispatchGroup.enter()
            getallKitchen()
            
        case .USER:
            self.title = NSLocalizedString("Update User", comment:"")
            arrFormFields = [FORM_FIELD.USER_NAME, FORM_FIELD.PHONE, FORM_FIELD.ADULT, FORM_FIELD.PASSWORD]
            
            dispatchGroup.enter()
            self.getDetailsByID()
            
            dispatchGroup.enter()
            self.getAllHouses()
            
            dispatchGroup.notify(queue: .main) {
                print("Both functions complete 👍")
                DispatchQueue.main.async {
                    self.tblView.reloadData()
                }
            }
        
        default:
            print("")
        }
        self.updateNavTitle(self.title ?? "", true) { (name) in
            if(name == "right")
            {
                return
            } else {
                DispatchQueue.main.async {
                    self.navigationController?.popViewController(animated: false)
                }
            }
            
        }
        tblView.reloadData()
        // Do any additional setup after loading the view.
    }
    

    
    //MARK:- button actions
    
    
   private func inputValidation() ->(Bool,String){
        var success = false
        var errorMsg = ""
        
        switch userType {
        case .COOK:
            if name.length > 0{
                success = true
                if email.isValidEmail(){
                    success = true
                    
                    if phone.length > 8 && phone.length < 11{
                        success = true
                        
                        if password.length > 0{
                            success = true
                            if password.length < 6{
                                success = false
                                errorMsg = NSLocalizedString("Password must be of minimum 6 characters length", comment:"")
                            }
                        } else {
                            success = false
                            errorMsg = NSLocalizedString("Please enter password", comment:"")
                        }
                        
                    } else {
                        success = false
                        errorMsg = NSLocalizedString("Phone number must be in the range of 8-10 digits", comment:"")
                    }
                } else {
                    success = false
                    errorMsg = NSLocalizedString("Please enter a valid email id", comment:"")
                }
            } else {
                errorMsg = NSLocalizedString("Please enter cook name", comment:"")
            }
        case .USER:
            if name.isValidEmail(){
                success = true
                if phone.length > 0{
                    success = true
                    
                    if phone.length > 8 && phone.length < 11{
                        success = true
                        if house.length > 0{
                            success = true
                            
                            if password.length > 0{
                                success = true
                                if password.length < 6{
                                    success = false
                                    errorMsg = NSLocalizedString("Password must be of minimum 6 characters length", comment:"")
                                }
                            } else {
                                success = false
                                errorMsg = NSLocalizedString("Please enter password", comment:"")
                            }
                        } else {
                            success = false
                            errorMsg = NSLocalizedString("Please select house", comment:"")
                        }
                    } else {
                        success = false
                        errorMsg = NSLocalizedString("Phone number must be in the range of 8-10 digits", comment:"")
                    }
                    
                } else {
                    success = false
                    errorMsg = NSLocalizedString("Please enter phone number", comment:"")
                }
                
            } else {
                success = false
                errorMsg = NSLocalizedString("Please enter valid EmailId", comment:"")
            }
       
        default:
            print("")
        }
        return (success,errorMsg)
    }
    private func generateRandomDigits(_ digitNumber: Int) -> String {
        var number = ""
        for i in 0..<digitNumber {
            var randomNumber = arc4random_uniform(10)
            while randomNumber == 0 && i == 0 {
                randomNumber = arc4random_uniform(10)
            }
            number += "\(randomNumber)"
        }
        return number
    }
    
    //MARK:- Api Calls
    private func getAllHouses()
    {
        self.showLoader()
        NewtworkManager.shared.GetAllHomeDetails { (status, dataIn) in
            self.hideloader()
            if (status)
            {
                self.houseArray = (dataIn as? [DKAPIHomeResponseElement])!
                if self.houseArray.count > 0{
                    self.house = self.houseArray[0].homeTitle
                }
            }
            self.dispatchGroup.leave()
        }
    }
    private func getallKitchen()
    {
        self.showLoader()
        NewtworkManager.shared.GetAllKitchenDetails { (status, dataIn) in
            self.hideloader()
            if(status)
            {
                self.kitchenArray = (dataIn  as? [DKAPIKitchenRespone])!
                DispatchQueue.main.async {
                    self.tblView.reloadData()
                }
            }
            
        }
    }
    
    func getDetailsByID()
    {
        
        self.showLoader()
        if userType == .USER {
            NewtworkManager.shared.GetUserDetailsByUserID(selectedDetailsID) { (status, dataIn) in
                self.hideloader()
                if (status)
                {
                    if let dataout = dataIn
                    {
                        self.selectedUseDetails = (dataout as? DKAPIEditUserResponse)!
                        self.name = self.selectedUseDetails.emailID
                        self.password = self.selectedUseDetails.password
                        self.email = self.selectedUseDetails.emailID
                        self.phone = self.selectedUseDetails.phoneNo
                        self.dropDownValue = self.selectedUseDetails.userGroup.isEmpty ? "Adult" :  self.selectedUseDetails.userGroup
                    }
                    
                    DispatchQueue.main.async
                        {
                            self.tblView.reloadData()
                    }
                }
                self.dispatchGroup.leave()
            }
        } else if userType == .COOK {
            NewtworkManager.shared.GetCookDetailsByCookID(selectedDetailsID) { (status, dataIn) in
                self.hideloader()
                if (status)
                {
                    if let dataout = dataIn
                    {
                        self.selectedCookDetails = (dataout as? DKAPIEditCookResponse)!
                        self.name = self.selectedCookDetails.cookName!
                        self.nameArabic = self.selectedCookDetails.cookName_AR!
                        self.password = self.selectedCookDetails.password!
                        self.email = self.selectedCookDetails.emailID!
                        self.phone = self.selectedCookDetails.phoneNo!
                        self.dropDownValue = ((self.selectedCookDetails.kitchenDetails!.kitchenTitle?.isEmpty)! ? self.kitchenArray[0].kitchenTitle : self.selectedCookDetails.kitchenDetails!.kitchenTitle)!  //self.selectedUseDetails.userGroup.isEmpty ? "Adult" : self.selectedUseDetails.userGroup
                        
                    }
                    
                    DispatchQueue.main.async
                        {
                            self.tblView.reloadData()
                    }
                }
                self.dispatchGroup.leave()
            }
        }

        
    }
    
    
    //Calls this function when the tap is recognized.
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
        currentTextField.resignFirstResponder()
    }
    //MARK: - Date Picker
    func addToolBar(){
        
        //ToolBar
        toolbar.sizeToFit()
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment:""), style: UIBarButtonItem.Style.done, target: self, action: #selector(CommonFormVC.donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Cancel", comment:""), style: UIBarButtonItem.Style.done, target: self, action: #selector(CommonFormVC.cancelDatePicker))
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
    }
    
    @objc func donedatePicker(){
        
        if currentTextField.inputView == UITextField() {
            self.textFieldDidEndEditing(currentTextField)
        } else {
            self.view.endEditing(true)
        }
        
    }
    @IBAction func btnCancelClicked(_ sender: Any) {
        self.constPickerViewHeight.constant = 0
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    @IBAction func btnUpdateClick(_ sender: UIButton) {
        
        self.view.endEditing(true)
        if self.inputValidation().0
        {
            if(userType == .USER)
            {
                self.showLoader()
                /*let userDetails = DKAddUserModel.init()
                 userDetails.userName = name
                 userDetails.gender = "Male"
                 userDetails.emailID = name
                 userDetails.phoneNo = phone
                 userDetails.password = password
                 userDetails.isActive = true
                 userDetails.homeDetails = selectedHouseObj*/
                
                
                self.selectedUseDetails.password = self.password
                self.selectedUseDetails.phoneNo = self.phone
                self.selectedUseDetails.userGroup = self.dropDownValue
                self.selectedUseDetails.isActive = true
                
                NewtworkManager.shared.ModifyUser(selectedUseDetails, callback: { (status1, dataout1) in
                    self.hideloader()
                    if(!status1)
                    {
                        self.showAlert(title: "", message: NSLocalizedString("Unable to add user ,Please try again", comment:""))
                        return
                    }
                    
                    do {
                        let json = try JSONSerialization.jsonObject(with: dataout1 as! Data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String:AnyObject]
                        
                        if let jsonresp = json
                        {
                            let errorMessage = jsonresp["ErrorMessage"] as? [String]
                            if let errMessage = errorMessage
                            {
                                self.showAlert(title: "", message: errMessage[0])
                                return
                            }
                        }
                        else {
                            self.showAlert(title: "", message: NSLocalizedString("Unable to add user ,Please try again", comment:""))
                            return
                        }
                        
                    }
                    catch {
                        self.showAlert(title: "", message: NSLocalizedString("Unable to add user ,Please try again", comment:""))
                        return
                    }
                    
                    DispatchQueue.main.async {
                        if self.complitionHandler != nil
                        {
                            self.complitionHandler!(true,"added" as AnyObject)
                        }
                        DispatchQueue.main.async {
                            self.navigationController?.popViewController(animated: false)
                        }
                    }
                })
            } else if (userType == .COOK)
            {
                self.showLoader()
                /*let userDetails = DKAddUserModel.init()
                 userDetails.userName = name
                 userDetails.gender = "Male"
                 userDetails.emailID = name
                 userDetails.phoneNo = phone
                 userDetails.password = password
                 userDetails.isActive = true
                 userDetails.homeDetails = selectedHouseObj*/
                
                
                self.selectedCookDetails.password = self.password
                self.selectedCookDetails.phoneNo = self.phone
                self.selectedCookDetails.cookName = self.name
                //self.selectedCookDetails.kitchenDetails = self.dropDownValue
                self.selectedCookDetails.isActive = true
                
                NewtworkManager.shared.ModifyCook(selectedCookDetails, callback: { (status1, dataout1) in
                    self.hideloader()
                    if(!status1)
                    {
                        self.showAlert(title: "", message: NSLocalizedString("Unable to update cook details ,Please try again", comment:""))
                        return
                    }
                    
                    do {
                        let json = try JSONSerialization.jsonObject(with: dataout1 as! Data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String:AnyObject]
                        
                        if let jsonresp = json
                        {
                            let errorMessage = jsonresp["ErrorMessage"] as? [String]
                            if let errMessage = errorMessage
                            {
                                self.showAlert(title: "", message: errMessage[0])
                                return
                            }
                        }
                        else {
                            self.showAlert(title: "", message: NSLocalizedString("Unable to update cook details ,Please try again", comment:""))
                            return
                        }
                        
                    }
                    catch {
                        self.showAlert(title: "", message: NSLocalizedString("Unable to add user ,Please try again", comment:""))
                        return
                    }
                    
                    DispatchQueue.main.async {
                        if self.complitionHandler != nil
                        {
                            self.complitionHandler!(true,"added" as AnyObject)
                        }
                        DispatchQueue.main.async {
                            self.navigationController?.popViewController(animated: false)
                        }
                    }
                })
            }
            
            
            
        } else {
            self.alert(message:inputValidation().1 , title: NSLocalizedString("Alert!", comment:""))
        }
        
        
        
    }
    @IBAction func btnDoneCliked(_ sender: Any) {
        self.constPickerViewHeight.constant = 0
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
        let row = self.selectionPickerView.selectedRow(inComponent: 0)
        self.selectionPickerView.selectRow(row, inComponent: 0, animated: false)
        dropDownValue = selectedButtonTag == FORM_FIELD.ASSIGN_HOUSE.rawValue ? houseArray[row].homeTitle : self.arrPickerContect[row]
        selectedCategoryID = arrPickerContectId[row]
        if selectedButtonTag == FORM_FIELD.ADD_CATEGORY.rawValue{
            let indexPath = IndexPath(item: 3, section: 0)
            tblView.reloadRows(at: [indexPath], with: .none)
        } else if selectedButtonTag == FORM_FIELD.ADULT.rawValue{
            let indexPath = IndexPath(item: 2, section: 0)
            tblView.reloadRows(at: [indexPath], with: .none)
        } else if selectedButtonTag == FORM_FIELD.ASSIGN_HOUSE.rawValue{
            house = dropDownValue
            selectedHouseObj.homeID = houseArray[row].homeID
            selectedHouseObj.homeTitle = houseArray[row].homeTitle
            selectedHouseObj.homeDesc = houseArray[row].homeDesc
            let indexPath = IndexPath(item: 3, section: 0)
            tblView.reloadRows(at: [indexPath], with: .none)
            
        } else if selectedButtonTag == FORM_FIELD.ASSIGN_KITCHEN.rawValue{
            kitchen = dropDownValue
            let indexPath = IndexPath(item: 1, section: 0)
            tblView.reloadRows(at: [indexPath], with: .none)
        }
    }
    @objc func openPicker(sender: UIButton){
        self.view.endEditing(true)
        selectedButtonTag = sender.tag
        if sender.tag == FORM_FIELD.UPLOAD_IMAGE.rawValue{
            let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            actionSheet.addAction(UIAlertAction(title: NSLocalizedString("Camera", comment:""), style: .default, handler: { (alert:UIAlertAction!) -> Void in
                self.camera()
            }))
            
            actionSheet.addAction(UIAlertAction(title: NSLocalizedString("Gallery", comment:""), style: .default, handler: { (alert:UIAlertAction!) -> Void in
                self.photoLibrary()
            }))
            
            actionSheet.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment:""), style: .cancel, handler: nil))
            
            present(actionSheet, animated: true, completion: nil)
            
        } else if sender.tag == FORM_FIELD.ADD_CATEGORY.rawValue{
            
            
            self.constPickerViewHeight.constant = 230
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        } else if sender.tag == FORM_FIELD.ADULT.rawValue{
            
            arrPickerContect = ["Kids", "Adult", "Senior"]
            selectedButtonTag = sender.tag
            self.constPickerViewHeight.constant = 230
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
        
        currentTextField.text = ""
        
    }
    
    func camera()
    {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .camera
            present(myPickerController, animated: true, completion: nil)
        }
        
    }
    func photoLibrary()
    {
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .photoLibrary
            present(myPickerController, animated: true, completion: nil)
        }
    }
    // MARK: - UIImagePickerControllerDelegate Methods
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            //            imageView.contentMode = .ScaleAspectFit
            //            imageView.image = pickedImage
            
            //Now use image to create into NSData format
            if let image = pickedImage.resizeWithPercentage(percentage: 0.5)
            {
                let imageData:NSData = image.pngData()! as NSData
                selectedImageString = imageData.base64EncodedString(options: .lineLength64Characters)
                if selectedButtonTag == FORM_FIELD.UPLOAD_IMAGE.rawValue{
                    let indexPath = IndexPath(item: 2, section: 0)
                    tblView.reloadRows(at: [indexPath], with: .none)
                }
            }
            
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    //MARK:- TableView Methods
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  arrFormFields.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var  cell:UITableViewCell!
        let formFieldValue = arrFormFields[indexPath.row]
        var txtFieldContent =  UITextField()
        var txtViewContent =  UITextView()
        switch formFieldValue.rawValue {
        case FORM_FIELD.ADD_CATEGORY.rawValue, FORM_FIELD.UPLOAD_IMAGE.rawValue, FORM_FIELD.ADULT.rawValue:
            cell = tableView.dequeueReusableCell(withIdentifier: "cellButton", for: indexPath) as UITableViewCell
            let btn = cell.subviews[0].subviews[0] as! UIButton
            
            btn.tag = formFieldValue.rawValue
            btn.setTitle(FORM_FIELD.getPlaceHolderOrButtonTitle(feildTag: formFieldValue.rawValue), for: .normal)
            if formFieldValue.rawValue == FORM_FIELD.ADULT.rawValue{
                btn.setTitle(dropDownValue, for: .normal)
            }
            btn.addTarget(self, action: #selector(openPicker(sender:)), for: .touchUpInside)
            let img = cell.viewWithTag(11) as! UIImageView
            if selectedButtonTag == FORM_FIELD.ADD_CATEGORY.rawValue{
                btn.setTitle(dropDownValue, for: .normal)
                img.image = nil
            } else if selectedButtonTag == FORM_FIELD.ADULT.rawValue{
                btn.setTitle(dropDownValue, for: .normal)
                img.image = nil
            } else {
                if selectedImageString.length > 0{
                    
                    if let decodedImageData = Data(base64Encoded: selectedImageString, options: .ignoreUnknownCharacters) {
                        let image = UIImage(data: decodedImageData)
                        img.image = image
                    }
                }
            }
        case FORM_FIELD.USER_NAME.rawValue:
            cell = tableView.dequeueReusableCell(withIdentifier: "cellTextField", for: indexPath) as UITableViewCell
            txtFieldContent = cell.subviews[0].subviews[0].subviews[0] as! UITextField
            txtFieldContent.tag = formFieldValue.rawValue
            let button = (cell.subviews[0].subviews[0]).viewWithTag(1001) as? UIButton
            button?.isHidden = true
            txtFieldContent.attributedPlaceholder = NSAttributedString(string: FORM_FIELD.getPlaceHolderOrButtonTitle(feildTag: formFieldValue.rawValue),
                                                                       attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
            txtFieldContent.text = self.email
            txtFieldContent.isUserInteractionEnabled = false
        case FORM_FIELD.COOK_NAME.rawValue:
            cell = tableView.dequeueReusableCell(withIdentifier: "cellTextField", for: indexPath) as UITableViewCell
            txtFieldContent = cell.subviews[0].subviews[0].subviews[0] as! UITextField
            txtFieldContent.tag = formFieldValue.rawValue
            let button = (cell.subviews[0].subviews[0]).viewWithTag(1001) as? UIButton
            button?.isHidden = true
            txtFieldContent.attributedPlaceholder = NSAttributedString(string: FORM_FIELD.getPlaceHolderOrButtonTitle(feildTag: formFieldValue.rawValue),
                                                                       attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
            txtFieldContent.text = self.name
            txtFieldContent.isUserInteractionEnabled = true
        case FORM_FIELD.EMAIL.rawValue:
            cell = tableView.dequeueReusableCell(withIdentifier: "cellTextField", for: indexPath) as UITableViewCell
            txtFieldContent = cell.subviews[0].subviews[0].subviews[0] as! UITextField
            txtFieldContent.tag = formFieldValue.rawValue
            let button = (cell.subviews[0].subviews[0]).viewWithTag(1001) as? UIButton
            button?.isHidden = true
            txtFieldContent.attributedPlaceholder = NSAttributedString(string: FORM_FIELD.getPlaceHolderOrButtonTitle(feildTag: formFieldValue.rawValue),
                                                                       attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
            txtFieldContent.text = self.email
            txtFieldContent.isUserInteractionEnabled = false
        case FORM_FIELD.PHONE.rawValue:
            cell = tableView.dequeueReusableCell(withIdentifier: "cellTextField", for: indexPath) as UITableViewCell
            txtFieldContent = cell.subviews[0].subviews[0].subviews[0] as! UITextField
            txtFieldContent.tag = formFieldValue.rawValue
            let button = (cell.subviews[0].subviews[0]).viewWithTag(1001) as? UIButton
            button?.isHidden = true
            txtFieldContent.attributedPlaceholder = NSAttributedString(string: FORM_FIELD.getPlaceHolderOrButtonTitle(feildTag: formFieldValue.rawValue),
                                                                       attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
            txtFieldContent.text = self.phone
            txtFieldContent.isUserInteractionEnabled = true
        case FORM_FIELD.PASSWORD.rawValue:
            cell = tableView.dequeueReusableCell(withIdentifier: "cellTextField", for: indexPath) as UITableViewCell
            txtFieldContent = cell.subviews[0].subviews[0].subviews[0] as! UITextField
            txtFieldContent.tag = formFieldValue.rawValue
            let button = (cell.subviews[0].subviews[0]).viewWithTag(1001) as? UIButton
            button?.isHidden = true
            txtFieldContent.attributedPlaceholder = NSAttributedString(string: FORM_FIELD.getPlaceHolderOrButtonTitle(feildTag: formFieldValue.rawValue),
                                                                       attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
            txtFieldContent.text = self.password
            txtFieldContent.isUserInteractionEnabled = true
            
        case FORM_FIELD.ASSIGN_KITCHEN.rawValue:
            cell = tableView.dequeueReusableCell(withIdentifier: "cellTextField", for: indexPath) as UITableViewCell
            txtFieldContent = cell.subviews[0].subviews[0].subviews[0] as! UITextField
            txtFieldContent.tag = formFieldValue.rawValue
            let button = (cell.subviews[0].subviews[0]).viewWithTag(1001) as? UIButton
            button?.isHidden = false
            
            if(self.kitchenArray.count > 0)
            {
                let kictch = self.kitchenArray[0].kitchenTitle
                txtFieldContent.text = kictch
            } else {
                txtFieldContent.text = "Kitchen 1"
            }
            
            txtFieldContent.isUserInteractionEnabled = false
        case FORM_FIELD.DESCRIPTION.rawValue, FORM_FIELD.DESCRIPTION_ARABIC.rawValue:
            cell = tableView.dequeueReusableCell(withIdentifier: "cellDescription", for: indexPath) as UITableViewCell
            txtViewContent = cell.subviews[0].subviews[0].subviews[0] as! UITextView
            txtViewContent.tag = formFieldValue.rawValue
            txtViewContent.text = FORM_FIELD.getPlaceHolderOrButtonTitle(feildTag: formFieldValue.rawValue)
        case FORM_FIELD.COOK_NAME_ARABIC.rawValue, FORM_FIELD.KITCHEN_NAME_ARABIC.rawValue, FORM_FIELD.CATEGORY_NAME_ARABIC.rawValue, FORM_FIELD.USER_NAME_ARABIC.rawValue, FORM_FIELD.HOUSE_NAME_ARABIC.rawValue:
            cell = tableView.dequeueReusableCell(withIdentifier: "cellTextField", for: indexPath) as UITableViewCell
            txtFieldContent = cell.subviews[0].subviews[0].subviews[0] as! UITextField
            txtFieldContent.tag = formFieldValue.rawValue
            let button = (cell.subviews[0].subviews[0]).viewWithTag(1001) as? UIButton
            button?.isHidden = true
            txtFieldContent.attributedPlaceholder = NSAttributedString(string: FORM_FIELD.getPlaceHolderOrButtonTitle(feildTag: formFieldValue.rawValue),
                                                                       attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
            txtFieldContent.text = self.nameArabic
            txtFieldContent.isUserInteractionEnabled = true
        default:
            cell = tableView.dequeueReusableCell(withIdentifier: "cellTextField", for: indexPath) as UITableViewCell
            txtFieldContent = cell.subviews[0].subviews[0].subviews[0] as! UITextField
            txtFieldContent.tag = formFieldValue.rawValue
            let button = (cell.subviews[0].subviews[0]).viewWithTag(1001) as? UIButton
            button?.isHidden = true
            txtFieldContent.attributedPlaceholder = NSAttributedString(string: FORM_FIELD.getPlaceHolderOrButtonTitle(feildTag: formFieldValue.rawValue),
                                                                       attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
            if selectedButtonTag == FORM_FIELD.ASSIGN_HOUSE.rawValue{
                txtFieldContent.text = house
            }
            
        }
        
        return cell
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let formFieldValue = arrFormFields[indexPath.row]
        return CGFloat(FORM_FIELD.getRowHeight(feildTag: formFieldValue.rawValue))
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension EditFormVC:UITextViewDelegate{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count // for Swift use count(newText)
        return numberOfChars < 250;
    }
    public func textViewDidBeginEditing(_ textView: UITextView) {
        textView.text = ""
        textView.textColor = UIColor.black
        textView.inputAccessoryView = toolbar
        
    }
    public func textViewDidEndEditing(_ textView: UITextView) {
        
        switch textView.tag {
        case FORM_FIELD.DESCRIPTION.rawValue:
            if textView.text.length > 0{
                
                descriptionText = textView.text
            } else {
                if textView.text != "Description" {
                    textView.textColor = UIColor.darkGray
                    textView.text = "Description"
                }
                
            }
        case FORM_FIELD.DESCRIPTION_ARABIC.rawValue:
            if textView.text.length > 0{
                descriptionTextArabic = textView.text
            } else {
                if textView.text != "الوصف" {
                    textView.textColor = UIColor.darkGray
                    textView.text = "الوصف"
                }
                
            }
        default:
            print("")
        }
        
    }
}
// MARK: - Textfield Delegate
extension EditFormVC : UITextFieldDelegate{
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if constPickerViewHeight.constant == 230{
            self.constPickerViewHeight.constant = 0
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        }
        currentTextField.resignFirstResponder()
        currentTextField = textField
        /*let cell = currentTextField.superview?.superview as! UITableViewCell
         cell.backgroundColor = UIColor.orange
         if((cell.contentView.subviews[0] as Any ) is UILabel){
         (cell.contentView.subviews[0] as! UILabel ).textColor = UIColor.init(hexString: "#fea049")
         }*/
        
        textField.inputAssistantItem.leadingBarButtonGroups = []
        textField.inputAssistantItem.trailingBarButtonGroups = []
        
        //textField.tintColor = UIColor.clear
        textField.keyboardType = .default
        if(textField.tag == FORM_FIELD.PHONE.rawValue)
        {
            textField.keyboardType = .phonePad
        }
        
        switch textField.tag
        {
        case FORM_FIELD.DESCRIPTION.rawValue:
            textField.inputAccessoryView = toolbar
            view.addGestureRecognizer(tap)
            break
        case FORM_FIELD.ASSIGN_HOUSE.rawValue:
            selectedButtonTag = FORM_FIELD.ASSIGN_HOUSE.rawValue
            self.constPickerViewHeight.constant = 230
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
            arrPickerContect = houseArray.map({$0.homeTitle + " - " + $0.homeTitle_AR })
            selectionPickerView.reloadAllComponents()
            //currentTextField.inputView = selectionPickerView
            view.addGestureRecognizer(tap)
            return false
        case FORM_FIELD.ASSIGN_KITCHEN.rawValue:
            selectedButtonTag = textField.tag
            self.constPickerViewHeight.constant = 230
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
            arrPickerContect = houseArray.map({$0.homeTitle + " - " + $0.homeTitle_AR })
            selectionPickerView.reloadAllComponents()
            //textField.inputView = selectionPickerView
            view.addGestureRecognizer(tap)
            return false
        default:
            //currentTextField.becomeFirstResponder()
            textField.inputAccessoryView = toolbar
            view.addGestureRecognizer(tap)
            break
        }
        
        return true;
        
    }
    public func textFieldDidEndEditing(_ textField: UITextField) {
        
        switch textField.tag {
        case FORM_FIELD.ASSIGN_HOUSE.rawValue:
            house = dropDownValue
            textField.resignFirstResponder()
            break
        case FORM_FIELD.ASSIGN_KITCHEN.rawValue:
            kitchen = textField.text!
            textField.resignFirstResponder()
            break
        case FORM_FIELD.ADD_LOCATION.rawValue:
            location = textField.text!
            textField.resignFirstResponder()
            break
        case FORM_FIELD.PHONE.rawValue:
            phone = textField.text!
            //textField.keyboardType = .phonePad
            textField.resignFirstResponder()
            break
        case FORM_FIELD.PASSWORD.rawValue:
            password = textField.text!
            textField.resignFirstResponder()
            break
        case FORM_FIELD.EMAIL.rawValue:
            email = textField.text!
            textField.resignFirstResponder()
            break
        case FORM_FIELD.COOK_NAME_ARABIC.rawValue, FORM_FIELD.KITCHEN_NAME_ARABIC.rawValue, FORM_FIELD.CATEGORY_NAME_ARABIC.rawValue, FORM_FIELD.USER_NAME_ARABIC.rawValue, FORM_FIELD.HOUSE_NAME_ARABIC.rawValue:
            nameArabic = textField.text!
            textField.resignFirstResponder()
            break
        default:
            name = textField.text!
            textField.resignFirstResponder()
            break
        }
        
        /*let cell = currentTextField.superview?.superview as! UITableViewCell
         cell.backgroundColor = UIColor.clear
         
         if((cell.contentView.subviews[0] as Any ) is UILabel){
         (cell.contentView.subviews[0] as! UILabel ).textColor = UIColor.init(hexString: "#3A566E")
         }*/
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.tag == FORM_FIELD.PHONE.rawValue {
            //if (textField.text?.length)! < 20 {
            let aSet = NSCharacterSet(charactersIn:"0123456789.").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return string == numberFiltered
            //                }
            //                else {
            //                    return false
            //                }
            
        }
        
        return true
    }
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
        
    }
}

extension EditFormVC: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        
        return selectedButtonTag == FORM_FIELD.ASSIGN_HOUSE.rawValue ? houseArray.count : arrPickerContect.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        var strTitle = ""
        
        strTitle  = selectedButtonTag == FORM_FIELD.ASSIGN_HOUSE.rawValue ? (houseArray[row].homeTitle + " - " + houseArray[row].homeTitle_AR) : arrPickerContect[row]
        return strTitle
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        dropDownValue = selectedButtonTag == FORM_FIELD.ASSIGN_HOUSE.rawValue ? (houseArray[row].homeTitle + " - " + houseArray[row].homeTitle_AR) : arrPickerContect[row]
        if selectedButtonTag == FORM_FIELD.ASSIGN_HOUSE.rawValue{
            house = houseArray[row].homeTitle
            selectedHouseObj.homeID = houseArray[row].homeID
            selectedHouseObj.homeTitle = houseArray[row].homeTitle
            selectedHouseObj.homeDesc = houseArray[row].homeDesc
        }
    }
    
}
