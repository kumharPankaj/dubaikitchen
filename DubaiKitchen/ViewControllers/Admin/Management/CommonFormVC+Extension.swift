//
//  CommonFormVC+Extension.swift
//  DubaiKitchen
//
//  Created by Pankaj Kumhar on 3/4/19.
//  Copyright © 2019 Pankaj Kumhar. All rights reserved.
//

import UIKit

extension CommonFormVC{
      func inputValidation() ->(Bool,String){
        var success = false
        var errorMsg = ""
        
        switch userType {
        case .COOK:
            if name.length > 0{
                success = true
                if email.isValidEmail(){
                    success = true
                    
                    if phone.length > 8 && phone.length < 11{
                        success = true
                        
                        if password.length > 0{
                            success = true
                            if password.length < 6{
                                success = false
                                errorMsg = NSLocalizedString("Password must be of minimum 6 characters length", comment:"")
                            }
                        } else {
                            success = false
                            errorMsg = NSLocalizedString("Please enter password", comment:"")
                        }
                        
                    } else {
                        success = false
                        errorMsg = NSLocalizedString("Phone number must be in the range of 8-10 digits", comment:"")
                    }
                } else {
                    success = false
                    errorMsg = NSLocalizedString("Please enter a valid email id", comment:"")
                }
            } else {
                errorMsg = NSLocalizedString("Please enter cook name.", comment:"")
            }
        case .HOUSE:
            if name.length > 0{
                success = true
                if location.length > 0{
                    success = true
                    if descriptionText.length > 0{
                        success = true
                    } else {
                        success = false
                        errorMsg = NSLocalizedString("Please enter description", comment:"")
                    }
                } else {
                    success = false
                    errorMsg = NSLocalizedString("Please enter location", comment:"")
                }
            } else {
                success = false
                errorMsg = NSLocalizedString("Please enter house name.", comment:"")
            }
            
            
        case .MENU:
            if name.length > 0{
                success = true
                if descriptionText.length > 0{
                    success = true
                    if selectedImageString != ""{
                        success = true
                        if dropDownValue.length > 0{
                            success = true
                        } else {
                            success = false
                            errorMsg = NSLocalizedString("Please select a category", comment:"")
                        }
                    } else {
                        success = false
                        errorMsg = NSLocalizedString("Please select food image", comment:"")
                    }
                } else {
                    success = false
                    errorMsg = NSLocalizedString("Please enter description", comment:"")
                }
                
            } else {
                success = false
                errorMsg = NSLocalizedString("Please enter menu item name.", comment:"")
            }
        case .KITHCEN:
            self.title = NSLocalizedString("Add Kitchen", comment:"")
        case .USER:
            if name.isValidEmail(){
                success = true
                if phone.length > 0{
                    success = true
                    
                    if phone.length > 8 && phone.length < 11{
                        success = true
                        if house.length > 0{
                            success = true
                            
                            if password.length > 0{
                                success = true
                                if password.length < 6{
                                    success = false
                                    errorMsg = NSLocalizedString("Password must be of minimum 6 characters length", comment:"")
                                }
                            } else {
                                success = false
                                errorMsg = NSLocalizedString("Please enter password", comment:"")
                            }
                        } else {
                            success = false
                            errorMsg = NSLocalizedString("Please select house", comment:"")
                        }
                    } else {
                        success = false
                        errorMsg = NSLocalizedString("Phone number must be in the range of 8-10 digits", comment:"")
                    }
                    
                } else {
                    success = false
                    errorMsg = NSLocalizedString("Please enter phone number", comment:"")
                }
                
            } else {
                success = false
                errorMsg = NSLocalizedString("Please enter valid EmailId", comment:"")
            }
        case .CATEGORY:
            if name.length > 0{
                success = true
                if descriptionText.length > 0{
                    success = true
                    if selectedImageString != ""{
                        success = true

                    } else {
                        success = false
                        errorMsg = NSLocalizedString("Please select category image", comment:"")
                    }
                } else {
                    success = false
                    errorMsg = NSLocalizedString("Please enter description", comment:"")
                }
            } else {
                errorMsg = NSLocalizedString("Please enter category name.", comment:"")
            }
        default:
            print("")
        }
        return (success,errorMsg)
    }
}
