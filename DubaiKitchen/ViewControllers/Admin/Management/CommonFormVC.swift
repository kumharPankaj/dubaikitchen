//
//  CommonFormVC.swift
//  DubaiKitchen
//
//  Created by Pankaj Kumhar on 3/2/19.
//  Copyright © 2019 Pankaj Kumhar. All rights reserved.
//

import UIKit

class CommonFormVC: CommonViewController , UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    var randomNumber = ""
    public var complitionHandler:WebCallbackHandler? = nil
    let toolbar = UIToolbar()
    var strFormTitle = ""
    var arrFormFields = [FORM_FIELD.ADD]
    var currentTextField =  UITextField()
    let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(CommonFormVC.dismissKeyboard))
    var name = ""
    var nameArabic = ""
    var house = ""
    var kitchen = ""
    var location = ""
    var location_ar = ""
    var phone = ""
    var password = ""
    var email = ""
    var dropDownValue = ""
    var descriptionText = ""
    private var descriptionTextArabic = ""
    var arrPickerContect = ["Main Course", "Starters", "Salad", "Drinks", "Deserts"]
    var arrPickerContectId = [1, 2, 3, 4, 5]
    var selectedCategoryID = 1
    var selectedButtonTag = 0
    @IBOutlet weak var selectionPickerView: UIPickerView!
    @IBOutlet weak var constPickerViewHeight: NSLayoutConstraint!
    let imagePicker = UIImagePickerController()
    private var houseArray:[DKAPIHomeResponseElement] = [DKAPIHomeResponseElement]()
     var selectedHouseObj = DKUserHomeDetails()
    var selectedImageString = ""
    private var categoryArray:[DKAPICatDetail] = [DKAPICatDetail]()
    private var kitchenArray:[DKAPIKitchenRespone] = [DKAPIKitchenRespone]()
   
    @IBAction func btnAddClick(_ sender: UIButton)
        
    {
     
        self.view.endEditing(true)
        if self.inputValidation().0
        {
            if(userType == .USER)
            {
                self.showLoader()
                let userDetails = DKAddUserModel.init()
                userDetails.userName = name
                userDetails.gender = "Male"
                userDetails.emailID = name
                userDetails.phoneNo = phone
                userDetails.password = password
                userDetails.isActive = true
                userDetails.userGroup = dropDownValue.lowercased()
                userDetails.homeDetails = selectedHouseObj
                NewtworkManager.shared.AddUser(userDetails, callback: { (status1, dataout1) in
                    self.hideloader()
                    if(!status1)
                    {
                         self.showAlert(title: "", message: NSLocalizedString("Unable to add user ,Please try again", comment:""))
                        return
                    }
                    
                    do {
                        let json = try JSONSerialization.jsonObject(with: dataout1 as! Data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String:AnyObject]
                       
                        if let jsonresp = json
                        {
                            let errorMessage = jsonresp["ErrorMessage"] as? [String]
                            if let errMessage = errorMessage
                            {
                                self.showAlert(title: "", message: errMessage[0])
                                return
                            }
                        }
                        else {
                            self.showAlert(title: "", message: NSLocalizedString("Unable to add user ,Please try again", comment:""))
                            return
                        }
                        
                    }
                    catch {
                         self.showAlert(title: "", message: NSLocalizedString("Unable to add user ,Please try again", comment:""))
                        return
                    }
                    
                    DispatchQueue.main.async {
                        if self.complitionHandler != nil
                        {
                            self.complitionHandler!(true,"added" as AnyObject)
                        }
                        DispatchQueue.main.async {
                            self.navigationController?.popViewController(animated: false)
                        }
                    }
                })
            }
            
            if(userType == .COOK)
            {
                self.showLoader()
                
                let dkCookAddCook = DKCookAddCook.init()
                dkCookAddCook.cookName = name
                dkCookAddCook.cookName_AR = nameArabic
                dkCookAddCook.kitchenDetails.kitchenTitle = "Kitchen 1"
                dkCookAddCook.kitchenDetails.kitchenID = "1"
                dkCookAddCook.kitchenDetails.kitchenDesc =  "This is kitchen"
                
                if self.kitchenArray.count>0
                {
                    dkCookAddCook.kitchenDetails.kitchenTitle = kitchenArray[0].kitchenTitle
                    dkCookAddCook.kitchenDetails.kitchenID = kitchenArray[0].kitchenID
                    dkCookAddCook.kitchenDetails.kitchenDesc =  kitchenArray[0].kitchenDesc
                }
                
                
                dkCookAddCook.cookDesc = descriptionText
                dkCookAddCook.cookDesc_AR = descriptionTextArabic
                 dkCookAddCook.emailID = email
                 dkCookAddCook.password = password
                 dkCookAddCook.phoneNo = phone
                 dkCookAddCook.gender = "Male"
                dkCookAddCook.isActive = true
                
                NewtworkManager.shared.CreateCook(dkCookAddCook) { (status, dataIn) in
                    self.hideloader()
                    if(!status)
                    {
                        self.showAlert(title: "", message: NSLocalizedString("Unable to add cook ,Please try again", comment:""))
                        return
                    }
                    
                    do {
                        let json = try JSONSerialization.jsonObject(with: dataIn as! Data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String:AnyObject]
                        
                        if let jsonresp = json
                        {
                            let errorMessage = jsonresp["ErrorMessage"] as? [String]
                            if let errMessage = errorMessage
                            {
                                self.showAlert(title: "", message: errMessage[0])
                                return
                            }
                        }
                        else {
                            self.showAlert(title: "", message: NSLocalizedString("Unable to add cook ,Please try again", comment:""))
                            return
                        }
                        
                    }
                    catch {
                        self.showAlert(title: "", message: NSLocalizedString("Unable to add cook ,Please try again", comment:""))
                        return
                    }

                    DispatchQueue.main.async {
                        
                        if self.complitionHandler != nil
                        {
                            self.complitionHandler!(true,"added" as AnyObject)
                        }
                        self.navigationController?.popViewController(animated: false)
                    }
                }
            }
            
            if(userType == .HOUSE)
            {
                self.showLoader()
                let dkCookAddCook = DKHomeAddHouse.init()
                dkCookAddCook.homeTitle = name
                dkCookAddCook.homeDesc = descriptionText
                dkCookAddCook.homeTitle_AR = nameArabic
                dkCookAddCook.homeDesc_AR = descriptionTextArabic
                dkCookAddCook.location = location
                dkCookAddCook.location_ar = location_ar
                dkCookAddCook.isActive = true
                NewtworkManager.shared.CreateHome(dkCookAddCook) { (status, dataIn) in
                    self.hideloader()
                    //if let obj = dataIn as? DKUserHomeDetails{
                        if status{
                           
                            DispatchQueue.main.async {
                                if self.complitionHandler != nil
                                {
                                    self.complitionHandler!(true,"added" as AnyObject)
                                }
                                 self.navigationController?.popViewController(animated: false)
                                 self.alert(message: NSLocalizedString("Home Record Created Successfully", comment:""), title: NSLocalizedString("Success", comment:""))
                            }
                        }
                    //}
                    
                }
            }
            
            if(userType == .MENU)
            {
                self.showLoader()
                let dkCookAddCook = DKMenuAddMenuModel.init()
                dkCookAddCook.menuItemName = name
                dkCookAddCook.menuItemDesc = descriptionText
                dkCookAddCook.menuItemName_AR = nameArabic
                dkCookAddCook.menuItemDesc_AR = descriptionTextArabic
                dkCookAddCook.image = selectedImageString
                dkCookAddCook.catDetails.catName = dropDownValue
                dkCookAddCook.catDetails.catID = "\(selectedCategoryID)"
                dkCookAddCook.imageFileName = randomNumber+"_MenuImage"
                dkCookAddCook.imageExtension = "png"
                dkCookAddCook.isActive = true
                
                NewtworkManager.shared.CreateMenu(dkCookAddCook) { (status, dataIn) in
                    self.hideloader()
                    if(!status)
                    {
                        self.showAlert(title: "", message: NSLocalizedString("Unable to add menu ,Please try again", comment:""))
                        return
                    }
                    
                    DispatchQueue.main.async {
                        self.selectedImageString = ""
                        if self.complitionHandler != nil
                        {
                            self.complitionHandler!(true,"added" as AnyObject)
                        }
                         self.navigationController?.popViewController(animated: false)
                    }
                }
            }
            
            if(userType == .KITHCEN)
            {
                self.showLoader()
                let dkCookAddCook = DKKitchenAdd.init()
                NewtworkManager.shared.CreateKitchen(dkCookAddCook) { (status, dataIn) in
                    self.hideloader()
                    if(!status)
                    {
                        self.showAlert(title: "", message: NSLocalizedString("Unable to add kitchen ,Please try again", comment:""))
                        return
                    }
                    DispatchQueue.main.async {
                        if self.complitionHandler != nil
                        {
                            self.complitionHandler!(true,"added" as AnyObject)
                        }
                         self.navigationController?.popViewController(animated: false)
                    }
                }
            }
            if(userType == .CATEGORY)
            {
                self.showLoader()
                let dkAddCategory = DKCatagoryAddCreate.init()
                dkAddCategory.catName = name
                dkAddCategory.catName_AR = nameArabic
                dkAddCategory.catDesc_AR = descriptionTextArabic
                dkAddCategory.catDesc = descriptionText
                dkAddCategory.isActive = true
                dkAddCategory.catImage = selectedImageString//"QEA="
                dkAddCategory.catImageName = randomNumber+"_catimage"
                dkAddCategory.catImageExt = "png"
                NewtworkManager.shared.CreateCategory(dkAddCategory) { (status, dataIn) in
                    self.hideloader()
                    if(!status)
                    {
                        self.showAlert(title: "", message: NSLocalizedString("Unable to add catagory ,Please try again", comment:""))
                        return
                    }
                    //if let obj = dataIn as? DKUserHomeDetails{
                    if status{
                        
                        DispatchQueue.main.async {
                            if self.complitionHandler != nil
                            {
                                self.complitionHandler!(true,"added" as AnyObject)
                            }
                            self.navigationController?.popViewController(animated: false)
                            
                            self.alert(message: NSLocalizedString("Category Created Successfully", comment:""), title: NSLocalizedString("Success", comment:""))
                        }
                        
                    }
                    //}
                    
                }
            }
        } else {
            self.alert(message:inputValidation().1 , title: NSLocalizedString("Alert!", comment:""))
        }
        
        
        
    }
    
    private func generateRandomDigits(_ digitNumber: Int = 0) -> String
    {
        var number = ""
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd_MM_yyyy_HH_mm_ss"
        number = dateFormatter.string(from: Date())
        return number
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addToolBar()
        tblView.beginWatchingForKeyboardStateChanges()
        selectionPickerView.showsSelectionIndicator = true
        imagePicker.delegate = self
        tblView.tableHeaderView = UIView.init(frame: CGRect.init(x: 0.0, y: 0.0, width: 20, height: 44))
        //self.navigationController?.navigationBar.isHidden = false
        
        
        switch userType {
        case .COOK:
            self.title = NSLocalizedString("Add Cook", comment:"")
            arrFormFields = [FORM_FIELD.COOK_NAME,FORM_FIELD.COOK_NAME_ARABIC, FORM_FIELD.ASSIGN_KITCHEN, FORM_FIELD.EMAIL,FORM_FIELD.PHONE, FORM_FIELD.PASSWORD]
            // arrFormFields = [FORM_FIELD.COOK_NAME, FORM_FIELD.ASSIGN_HOUSE, FORM_FIELD.ASSIGN_KITCHEN]
            getallKitchen()
            
        case .HOUSE:
            self.title = NSLocalizedString("Add House", comment:"")
            arrFormFields = [FORM_FIELD.HOUSE_NAME,FORM_FIELD.HOUSE_NAME_ARABIC, FORM_FIELD.ADD_LOCATION, FORM_FIELD.ADD_LOCATION_AR,  FORM_FIELD.DESCRIPTION, FORM_FIELD.DESCRIPTION_ARABIC]
        case .MENU:
            self.title = NSLocalizedString("Add Menu", comment:"")
            arrFormFields = [FORM_FIELD.ITEM_NAME,FORM_FIELD.ITEM_NAME_ARABIC, FORM_FIELD.DESCRIPTION,FORM_FIELD.DESCRIPTION_ARABIC, FORM_FIELD.UPLOAD_IMAGE, FORM_FIELD.ADD_CATEGORY]
            apicall()
        case .KITHCEN:
            self.title = NSLocalizedString("Add Kitchen", comment:"")
            arrFormFields = [FORM_FIELD.KITCHEN_NAME, FORM_FIELD.KITCHEN_NAME_ARABIC]
        case .USER:
            self.title = NSLocalizedString("Add User", comment:"")
            arrFormFields = [FORM_FIELD.USER_NAME, FORM_FIELD.PHONE, FORM_FIELD.ADULT, FORM_FIELD.PASSWORD]
            self.getAllHouses()
        case .CATEGORY:
            self.title = NSLocalizedString("Add Category", comment:"")
            randomNumber = "\(generateRandomDigits(4))"
            arrFormFields = [FORM_FIELD.CATEGORY_NAME,FORM_FIELD.CATEGORY_NAME_ARABIC, FORM_FIELD.DESCRIPTION,FORM_FIELD.DESCRIPTION_ARABIC, FORM_FIELD.UPLOAD_IMAGE]
        default:
            print("")
        }
        self.updateNavTitle(self.title ?? "", true) { (name) in
            if(name == "right")
            {
                return
            } else {
                 DispatchQueue.main.async {
                        self.navigationController?.popViewController(animated: false)
                    }
            }
            
        }
        tblView.reloadData()
        // Do any additional setup after loading the view.
    }
    
    //Calls this function when the tap is recognized.
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
        currentTextField.resignFirstResponder()
    }
    //MARK: - Date Picker
    func addToolBar(){
        
        //ToolBar
        toolbar.sizeToFit()
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment:""), style: UIBarButtonItem.Style.done, target: self, action: #selector(CommonFormVC.donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Cancel", comment:""), style: UIBarButtonItem.Style.done, target: self, action: #selector(CommonFormVC.cancelDatePicker))
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
    }
    
    @objc func donedatePicker(){
        
        if currentTextField.inputView == UITextField() {
            self.textFieldDidEndEditing(currentTextField)
        } else {
            self.view.endEditing(true)
        }
        
    }
    @IBAction func btnCancelClicked(_ sender: Any) {
        self.constPickerViewHeight.constant = 0
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    @IBAction func btnDoneCliked(_ sender: Any) {
        self.constPickerViewHeight.constant = 0
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
        let row = self.selectionPickerView.selectedRow(inComponent: 0)
        self.selectionPickerView.selectRow(row, inComponent: 0, animated: false)
        dropDownValue = selectedButtonTag == FORM_FIELD.ASSIGN_HOUSE.rawValue ? houseArray[row].homeTitle : self.arrPickerContect[row]
        selectedCategoryID = arrPickerContectId[row]
        if selectedButtonTag == FORM_FIELD.ADD_CATEGORY.rawValue{
            let indexPath = IndexPath(item: 5, section: 0)
            tblView.reloadRows(at: [indexPath], with: .none)
        } else if selectedButtonTag == FORM_FIELD.ADULT.rawValue{
            let indexPath = IndexPath(item: 2, section: 0)
            tblView.reloadRows(at: [indexPath], with: .none)
        } else if selectedButtonTag == FORM_FIELD.ASSIGN_HOUSE.rawValue{
            house = dropDownValue
            selectedHouseObj.homeID = houseArray[row].homeID
            selectedHouseObj.homeTitle = houseArray[row].homeTitle
            selectedHouseObj.homeDesc = houseArray[row].homeDesc
            let indexPath = IndexPath(item: 3, section: 0)
            tblView.reloadRows(at: [indexPath], with: .none)
            
        } else if selectedButtonTag == FORM_FIELD.ASSIGN_KITCHEN.rawValue{
            kitchen = dropDownValue
            let indexPath = IndexPath(item: 1, section: 0)
            tblView.reloadRows(at: [indexPath], with: .none)
        }
    }
    @objc func openPicker(sender: UIButton){
        self.view.endEditing(true)
        selectedButtonTag = sender.tag
        if sender.tag == FORM_FIELD.UPLOAD_IMAGE.rawValue{
            let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            actionSheet.addAction(UIAlertAction(title: NSLocalizedString("Camera", comment:""), style: .default, handler: { (alert:UIAlertAction!) -> Void in
                self.camera()
            }))
            
            actionSheet.addAction(UIAlertAction(title: NSLocalizedString("Gallery", comment:""), style: .default, handler: { (alert:UIAlertAction!) -> Void in
                self.photoLibrary()
            }))
            
            actionSheet.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment:""), style: .cancel, handler: nil))
            
            present(actionSheet, animated: true, completion: nil)
        
        } else if sender.tag == FORM_FIELD.ADD_CATEGORY.rawValue{
           
        
        self.constPickerViewHeight.constant = 230
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
        } else if sender.tag == FORM_FIELD.ADULT.rawValue{
            
            arrPickerContect = ["Kids", "Adult", "Senior"]
            selectedButtonTag = sender.tag
            self.constPickerViewHeight.constant = 230
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
        
        currentTextField.text = ""
        
    }
    
    func camera()
    {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .camera
            present(myPickerController, animated: true, completion: nil)
        }
        
    }
    func photoLibrary()
    {
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .photoLibrary
            present(myPickerController, animated: true, completion: nil)
        }
    }
    // MARK: - UIImagePickerControllerDelegate Methods
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
         dismiss(animated: true, completion: nil)
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            //Now use image to create into NSData format
           if let image = pickedImage.resizeWithPercentage(percentage: 0.5)
           {
            let imageData:NSData = image.pngData()! as NSData
            selectedImageString = imageData.base64EncodedString(options: .lineLength64Characters)
             if selectedButtonTag == FORM_FIELD.UPLOAD_IMAGE.rawValue{
                let indexPath = IndexPath(item: 4, section: 0)
                tblView.reloadRows(at: [indexPath], with: .none)
            }
            }
            
        }
    }
    
    //MARK:- API calls
    private func getAllHouses()
    {
        self.showLoader()
        NewtworkManager.shared.GetAllHomeDetails { (status, dataIn) in
            self.hideloader()
            if (status)
            {
                self.houseArray = (dataIn as? [DKAPIHomeResponseElement])!
                if self.houseArray.count > 0{
                self.house = self.houseArray[0].homeTitle
                }
            }
        }
    }
    
    //MARK:- TableView Methods
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrFormFields.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var  cell:UITableViewCell!
        let formFieldValue = arrFormFields[indexPath.row]
        var txtFieldContent =  UITextField()
        var txtViewContent =  UITextView()
        switch formFieldValue.rawValue {
        case FORM_FIELD.ADD_CATEGORY.rawValue, FORM_FIELD.UPLOAD_IMAGE.rawValue, FORM_FIELD.ADULT.rawValue:
            cell = tableView.dequeueReusableCell(withIdentifier: "cellButton", for: indexPath) as UITableViewCell
            let btn = cell.subviews[0].subviews[0] as! UIButton
            btn.tag = formFieldValue.rawValue
            btn.setTitle(FORM_FIELD.getPlaceHolderOrButtonTitle(feildTag: formFieldValue.rawValue), for: .normal)
            btn.addTarget(self, action: #selector(openPicker(sender:)), for: .touchUpInside)
            let img = cell.viewWithTag(11) as! UIImageView
            if selectedButtonTag == FORM_FIELD.ADD_CATEGORY.rawValue{
                btn.setTitle(dropDownValue, for: .normal)
                img.image = nil
            } else if selectedButtonTag == FORM_FIELD.ADULT.rawValue{
                btn.setTitle(dropDownValue, for: .normal)
                img.image = nil
            } else {
                DispatchQueue.main.async {
                    //if self.selectedImageString.length > 0{
                        
                        if let decodedImageData = Data(base64Encoded: self.selectedImageString, options: .ignoreUnknownCharacters) {
                            let image = UIImage(data: decodedImageData)
                            img.image = image
                        }
                   // }
                }
                
            }
        case FORM_FIELD.ASSIGN_KITCHEN.rawValue:
            cell = tableView.dequeueReusableCell(withIdentifier: "cellTextField", for: indexPath) as UITableViewCell
            txtFieldContent = cell.subviews[0].subviews[0].subviews[0] as! UITextField
            txtFieldContent.tag = formFieldValue.rawValue
            let button = (cell.subviews[0].subviews[0]).viewWithTag(1001) as? UIButton
            button?.isHidden = false
            button?.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
           // txtFieldContent.attributedPlaceholder = NSAttributedString(string: FORM_FIELD.getPlaceHolderOrButtonTitle(feildTag: formFieldValue.rawValue),
                                                                      // attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
            if(self.kitchenArray.count > 0)
            {
                let kictch = self.kitchenArray[0].kitchenTitle
                txtFieldContent.text = kictch
            } else {
                txtFieldContent.text = "Kitchen 1"
            }
            
            txtFieldContent.isUserInteractionEnabled = false
        case FORM_FIELD.DESCRIPTION.rawValue:
            cell = tableView.dequeueReusableCell(withIdentifier: "cellDescription", for: indexPath) as UITableViewCell
            txtViewContent = cell.subviews[0].subviews[0].subviews[0] as! UITextView
            txtViewContent.tag = formFieldValue.rawValue
            txtViewContent.text = FORM_FIELD.getPlaceHolderOrButtonTitle(feildTag: formFieldValue.rawValue)
            if descriptionText.length > 0{
                txtViewContent.text = descriptionText
            }
        case  FORM_FIELD.DESCRIPTION_ARABIC.rawValue:
            cell = tableView.dequeueReusableCell(withIdentifier: "cellDescription", for: indexPath) as UITableViewCell
            txtViewContent = cell.subviews[0].subviews[0].subviews[0] as! UITextView
            txtViewContent.tag = formFieldValue.rawValue
            txtViewContent.text = FORM_FIELD.getPlaceHolderOrButtonTitle(feildTag: formFieldValue.rawValue)
            if descriptionTextArabic.length > 0{
                txtViewContent.text = descriptionTextArabic
            }
        default:
            cell = tableView.dequeueReusableCell(withIdentifier: "cellTextField", for: indexPath) as UITableViewCell
            txtFieldContent = cell.subviews[0].subviews[0].subviews[0] as! UITextField
            txtFieldContent.tag = formFieldValue.rawValue
            txtFieldContent.delegate = self
            let button = (cell.subviews[0].subviews[0]).viewWithTag(1001) as? UIButton
            button?.isHidden = true
            txtFieldContent.attributedPlaceholder = NSAttributedString(string: FORM_FIELD.getPlaceHolderOrButtonTitle(feildTag: formFieldValue.rawValue),
                                                                       attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
            if selectedButtonTag == FORM_FIELD.ASSIGN_HOUSE.rawValue{
                txtFieldContent.text = house
            }
            
        }
        
        return cell
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let formFieldValue = arrFormFields[indexPath.row]
        return CGFloat(FORM_FIELD.getRowHeight(feildTag: formFieldValue.rawValue))
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    @objc func buttonAction(sender: UIButton!)
    {
        print("Button tapped")
        var homeobject = self.kitchenArray[0]
        
        let alertController = UIAlertController(title: NSLocalizedString("Edit Kitchen", comment:""), message: NSLocalizedString("Please enter Kitchen Name", comment:""), preferredStyle: .alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = NSLocalizedString("Enter Kitchen Name", comment:"")
            textField.text = homeobject.kitchenTitle
        }
        let saveAction = UIAlertAction(title: NSLocalizedString("Save", comment:""), style: .default, handler: { alert -> Void in
            let firstTextField = alertController.textFields![0] as UITextField
            if let firstTextFieldtxt = firstTextField.text
            {
                homeobject.kitchenTitle = firstTextFieldtxt
                self.modifykitchenApiCall(homeobject)
            }
    
        })
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment:""), style: .default, handler: { (action : UIAlertAction!) -> Void in })
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func modifykitchenApiCall(_ kitchen:DKAPIKitchenRespone)
    {
        self.showLoader()
        NewtworkManager.shared.ModifiedKitchen(kitchen) { (status, datain) in
            self.hideloader()
            if (status)
            {
                DispatchQueue.main.async
                    {
                     self.kitchenArray[0] = kitchen
                        self.tblView.reloadData()
                }
            }
        }
    }
    
}
extension CommonFormVC:UITextViewDelegate{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count // for Swift use count(newText)
        return numberOfChars < 250;
    }
    public func textViewDidBeginEditing(_ textView: UITextView) {
        switch textView.tag {
        case FORM_FIELD.DESCRIPTION.rawValue:
            if textView.text == "Description"{
                textView.text = ""
                textView.textColor = UIColor.black
                textView.inputAccessoryView = toolbar
            }
        case FORM_FIELD.DESCRIPTION_ARABIC.rawValue:
            if textView.text == "الوصف"{
                textView.text = ""
                textView.textColor = UIColor.black
                textView.inputAccessoryView = toolbar
            }
        default:
            print("")
        }
       
        
    }
    public func textViewDidEndEditing(_ textView: UITextView) {
        switch textView.tag {
        case FORM_FIELD.DESCRIPTION.rawValue:
            if textView.text.length > 0{
                
                descriptionText = textView.text
            } else {
                if textView.text != "Description" {
                    textView.textColor = UIColor.darkGray
                    textView.text = "Description"
                }
                
            }
        case FORM_FIELD.DESCRIPTION_ARABIC.rawValue:
            if textView.text.length > 0{
                descriptionTextArabic = textView.text
            } else {
                if textView.text != "الوصف" {
                    textView.textColor = UIColor.darkGray
                    textView.text = "الوصف"
                }
                
            }
        default:
            print("")
        }
    }
}
// MARK: - Textfield Delegate
extension CommonFormVC : UITextFieldDelegate{
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if constPickerViewHeight.constant == 230{
            self.constPickerViewHeight.constant = 0
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        }
        currentTextField.resignFirstResponder()
        currentTextField = textField
        /*let cell = currentTextField.superview?.superview as! UITableViewCell
         cell.backgroundColor = UIColor.orange
         if((cell.contentView.subviews[0] as Any ) is UILabel){
         (cell.contentView.subviews[0] as! UILabel ).textColor = UIColor.init(hexString: "#fea049")
         }*/
        
        textField.inputAssistantItem.leadingBarButtonGroups = []
        textField.inputAssistantItem.trailingBarButtonGroups = []
        
        //textField.tintColor = UIColor.clear
         textField.keyboardType = .default
        if(textField.tag == FORM_FIELD.PHONE.rawValue)
        {
             textField.keyboardType = .phonePad
        }
        
        switch textField.tag
        {
        case FORM_FIELD.DESCRIPTION.rawValue,  FORM_FIELD.DESCRIPTION_ARABIC.rawValue:
            textField.inputAccessoryView = toolbar
            view.addGestureRecognizer(tap)
            break
        case FORM_FIELD.ASSIGN_HOUSE.rawValue:
            selectedButtonTag = FORM_FIELD.ASSIGN_HOUSE.rawValue
            self.constPickerViewHeight.constant = 230
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
            arrPickerContect = houseArray.map({$0.homeTitle})
            selectionPickerView.reloadAllComponents()
            //currentTextField.inputView = selectionPickerView
            view.addGestureRecognizer(tap)
            return false
        case FORM_FIELD.ASSIGN_KITCHEN.rawValue:
            selectedButtonTag = textField.tag
            self.constPickerViewHeight.constant = 230
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
            arrPickerContect = houseArray.map({$0.homeTitle})
            selectionPickerView.reloadAllComponents()
            //textField.inputView = selectionPickerView
            view.addGestureRecognizer(tap)
            return false
        default:
            //currentTextField.becomeFirstResponder()
            textField.inputAccessoryView = toolbar
            view.addGestureRecognizer(tap)
            break
        }
        
        return true;
        
    }
    public func textFieldDidEndEditing(_ textField: UITextField) {
       
        switch textField.tag {
        case FORM_FIELD.ASSIGN_HOUSE.rawValue:
            house = dropDownValue
            textField.resignFirstResponder()
            break
        case FORM_FIELD.ASSIGN_KITCHEN.rawValue:
            kitchen = textField.text!
            textField.resignFirstResponder()
            break
        case FORM_FIELD.ADD_LOCATION.rawValue:
            location = textField.text!
            textField.resignFirstResponder()
            break
        case FORM_FIELD.ADD_LOCATION_AR.rawValue:
            location_ar = textField.text!
            textField.resignFirstResponder()
            break
        case FORM_FIELD.PHONE.rawValue:
            phone = textField.text!
            //textField.keyboardType = .phonePad
            textField.resignFirstResponder()
            break
        case FORM_FIELD.PASSWORD.rawValue:
            password = textField.text!
            textField.resignFirstResponder()
            break
        case FORM_FIELD.EMAIL.rawValue:
            email = textField.text!
            textField.resignFirstResponder()
            break
        case FORM_FIELD.COOK_NAME_ARABIC.rawValue, FORM_FIELD.KITCHEN_NAME_ARABIC.rawValue, FORM_FIELD.CATEGORY_NAME_ARABIC.rawValue, FORM_FIELD.USER_NAME_ARABIC.rawValue, FORM_FIELD.HOUSE_NAME_ARABIC.rawValue, FORM_FIELD.ITEM_NAME_ARABIC.rawValue:
            nameArabic = textField.text!
            textField.resignFirstResponder()
            break
        case FORM_FIELD.COOK_NAME.rawValue, FORM_FIELD.KITCHEN_NAME.rawValue, FORM_FIELD.CATEGORY_NAME.rawValue, FORM_FIELD.USER_NAME.rawValue, FORM_FIELD.HOUSE_NAME.rawValue, FORM_FIELD.ITEM_NAME.rawValue:
            name = textField.text!
            textField.resignFirstResponder()
            break
        default:
            name = textField.text!
            textField.resignFirstResponder()
            break
        }
        
        /*let cell = currentTextField.superview?.superview as! UITableViewCell
         cell.backgroundColor = UIColor.clear
         
         if((cell.contentView.subviews[0] as Any ) is UILabel){
         (cell.contentView.subviews[0] as! UILabel ).textColor = UIColor.init(hexString: "#3A566E")
         }*/
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.tag == FORM_FIELD.PHONE.rawValue {
            //if (textField.text?.length)! < 20 {
            let aSet = NSCharacterSet(charactersIn:"0123456789.").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return string == numberFiltered
            //                }
            //                else {
            //                    return false
            //                }
            
        }
        
        return true
    }
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
        
    }
}

extension CommonFormVC: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        
        return selectedButtonTag == FORM_FIELD.ASSIGN_HOUSE.rawValue ? houseArray.count : arrPickerContect.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        var strTitle = ""
        
        strTitle  = selectedButtonTag == FORM_FIELD.ASSIGN_HOUSE.rawValue ? (houseArray[row].homeTitle + " - " + houseArray[row].homeTitle_AR) : arrPickerContect[row]
        return strTitle
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        dropDownValue = selectedButtonTag == FORM_FIELD.ASSIGN_HOUSE.rawValue ? (houseArray[row].homeTitle + " - " + houseArray[row].homeTitle_AR)  : arrPickerContect[row]
        if selectedButtonTag == FORM_FIELD.ASSIGN_HOUSE.rawValue{
            house = houseArray[row].homeTitle
            selectedHouseObj.homeID = houseArray[row].homeID
            selectedHouseObj.homeTitle = houseArray[row].homeTitle
            selectedHouseObj.homeDesc = houseArray[row].homeDesc
        }
    }
    
    func apicall()
    {
        randomNumber = "\(generateRandomDigits(10))"
        
        self.arrPickerContect.removeAll()
        self.arrPickerContectId.removeAll()
        
        self.showLoader()
        NewtworkManager.shared.GetAllCategoryDetails() { (status, dataIn) in
            self.hideloader()
            if (status)
            {
                self.categoryArray = (dataIn as? [DKAPICatDetail])!
                for objedata in self.categoryArray
                {
                    self.arrPickerContect.append(objedata.catName + " - " + objedata.catName_AR)
                    self.arrPickerContectId.append(Int(objedata.catID)!)
                }
                DispatchQueue.main.async {
                    
                }
            }
        }
        
    }
    
   private func getallKitchen()
    {
        self.showLoader()
        NewtworkManager.shared.GetAllKitchenDetails { (status, dataIn) in
            self.hideloader()
            if(status)
            {
                self.kitchenArray = (dataIn  as? [DKAPIKitchenRespone])!
                DispatchQueue.main.async {
                    self.tblView.reloadData()
                }
            }
        }
    }
}



