//
//  ManagementVC.swift
//  DubaiKitchen
//
//  Created by Pankaj Kumhar on 2/23/19.
//  Copyright © 2019 Pankaj Kumhar. All rights reserved.
//

import UIKit
enum USERTYPE
{
    case HOUSE
    case USER
    case COOK
    case MENU
    case KITHCEN
    case CATEGORY
    case DEFAULT
}

class HouseDetailsVC: CommonViewController {
    
    var selectedTypeorder:String = "Active"
    let active = "Active"
    let inactive = "Inactive"
    
    @IBOutlet weak var tabviewheightConstraints: NSLayoutConstraint!
    var usersArray:[DKAPIUserResponseElement] = [DKAPIUserResponseElement]()
    var allusersArray:[DKAPIUserResponseElement] = [DKAPIUserResponseElement]()
    var activeuser:[DKAPIUserResponseElement] = [DKAPIUserResponseElement]()
    var inactiveuser:[DKAPIUserResponseElement] = [DKAPIUserResponseElement]()
    var allCookArray:[DKAPICookResponseElement] = [DKAPICookResponseElement]()
    var cooksArray:[DKAPICookResponseElement] = [DKAPICookResponseElement]()
    var activeCook:[DKAPICookResponseElement] = [DKAPICookResponseElement]()
    var inactiveCook:[DKAPICookResponseElement] = [DKAPICookResponseElement]()
    private var houseArray:[DKAPIHomeResponseElement] = [DKAPIHomeResponseElement]()
    var menuArray:[DKAPIMenuResponseElement] = [DKAPIMenuResponseElement]()
    private var categoryArray:[DKAPICatDetail] = [DKAPICatDetail]()
    
    @IBOutlet weak var btnorderaccepted: UIButton!
    @IBOutlet weak var btnNewOrder: UIButton!
    private var isMenuApiCalled = false
    
    @IBAction func btnorderacceptclick(_ sender: UIButton)
    {
        ChnagebuttonStatus(sender)
    }
    
    @IBAction func btnneworderclick(_ sender: UIButton)
    {
        ChnagebuttonStatus(sender)
    }
    
    func ChnagebuttonStatus(_ sender: UIButton)
    {
        btnNewOrder.backgroundColor = UIColor.white
        btnorderaccepted.backgroundColor = UIColor.white
        
        if(sender == btnorderaccepted)
        {
            btnorderaccepted.backgroundColor = UIColor.clear
            parseOrder(inactive)
        }
        
        if(sender == btnNewOrder)
        {
            btnNewOrder.backgroundColor = UIColor.clear
            parseOrder(active)
        }
    }
    
    func parseOrder(_ stringorder:String = "Active")
    {
        
        
        
        self.selectedTypeorder = stringorder
        
        if userType == .USER{
            self.inactiveuser.removeAll()
            self.activeuser.removeAll()
            self.usersArray.removeAll()
            
            for usersIn in self.allusersArray
            {
                if(usersIn.isActive == true)
                {
                    self.activeuser.append(usersIn)
                }
                else
                {
                    self.inactiveuser.append(usersIn)
                }
            }
            
            if self.selectedTypeorder == self.active
            {
                self.usersArray =   self.activeuser
            }
            else {
                self.usersArray =   self.inactiveuser
            }
        } else if userType == .COOK{
            self.inactiveuser.removeAll()
            self.activeCook.removeAll()
            self.inactiveCook.removeAll()
            
            for usersIn in self.allCookArray
            {
                if(usersIn.isActive == true)
                {
                    self.activeCook.append(usersIn)
                }
                else
                {
                    self.inactiveCook.append(usersIn)
                }
            }
            
            if self.selectedTypeorder == self.active
            {
                self.cooksArray =   self.activeCook
            }
            else {
                self.cooksArray =   self.inactiveCook
            }
        }
        
        
        
        
        self.tblView.reloadData()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblView.estimatedRowHeight = 70
        self.tblView.rowHeight = UITableView.automaticDimension
        
        //create a new button
        let button = UIButton(type: .custom)
        //set image for button
        button.setImage(UIImage(named: "Add"), for: .normal)
        //add function for button
        //button.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        //set frame
        button.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        
        let barButton = UIBarButtonItem(customView: button)
        //assign button to navigationbar
        self.navigationItem.rightBarButtonItem = barButton
        
        //self.navigationController?.navigationBar.isHidden = false
        self.title = NSLocalizedString("House", comment:"")
        var titleStr = ""
        var navtitleStr = ""
        self.tabviewheightConstraints.constant = 0
        if(userType == USERTYPE.USER)
        {
            self.tabviewheightConstraints.constant = 50
            titleStr = NSLocalizedString("Users", comment:"")
            navtitleStr = NSLocalizedString("Add User", comment:"")
        }
        if(userType == USERTYPE.COOK)
        {
            self.tabviewheightConstraints.constant = 50
            titleStr = NSLocalizedString("Cooks", comment:"")
            navtitleStr = NSLocalizedString("Add Cook", comment:"")
        }
        if(userType == USERTYPE.HOUSE)
        {
            titleStr = NSLocalizedString("Houses", comment:"")
            navtitleStr = NSLocalizedString("Add House", comment:"")
        }
        if(userType == USERTYPE.MENU)
        {
            titleStr = NSLocalizedString("Menus", comment:"")
            navtitleStr = NSLocalizedString("Add Menu", comment:"")
        }
        if(userType == USERTYPE.CATEGORY)
        {
            titleStr = NSLocalizedString("Category", comment:"")
            navtitleStr = NSLocalizedString("Add Category", comment:"")
        }
        
        self.updateNavTitle(titleStr, true) { (name) in
            
            if(name == "right")
            {
                //                let notiVc = self.storyboard?.instantiateViewController(withIdentifier: "AddHouseViewController") as! AddHouseViewController
                //                notiVc.modalTransitionStyle = .coverVertical
                //                notiVc.modalPresentationStyle = .overCurrentContext
                //                self.navigationController?.present(notiVc, animated: true, completion: {
                //
                //                })
                let notiVc = self.storyboard?.instantiateViewController(withIdentifier: "CommonFormVC") as! CommonFormVC
                notiVc.userType = self.userType
                notiVc.strFormTitle = navtitleStr
                notiVc.complitionHandler =
                    { (status,data) in
                        if status == true
                        {
                            self.ApiCall()
                        }
                }
                
                DispatchQueue.main.async {
                    self.navigationController?.pushViewController(notiVc, animated: false)
                }
            } else {
                if self.isMenuApiCalled{
                    self.gloablHeaderView.IsRightEnable = true
                    self.gloablHeaderView.lblTitle.text = "Category"
                    self.isMenuApiCalled = false
                    self.tblView.reloadData()
                } else {
                    DispatchQueue.main.async {
                        self.navigationController?.popViewController(animated: false)
                    }
                }
                
            }
            
            
        }
        
        tblView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
        ApiCall()
    }
    
    func ApiCall()
    {
        if(userType == USERTYPE.USER)
        {
            self.showLoader()
            NewtworkManager.shared.GetAllActiveInActiveUserDetails { (status, dataIn) in
                self.hideloader()
                if (status)
                {
                    self.allusersArray = (dataIn as? [DKAPIUserResponseElement])!
                    DispatchQueue.main.async
                        {
                            if self.selectedTypeorder == self.active
                            {
                                self.ChnagebuttonStatus(self.btnNewOrder)
                            }
                            else {
                                self.ChnagebuttonStatus(self.btnorderaccepted)
                            }
                            
                    }
                }
            }
        }
        
        if(userType == USERTYPE.COOK)
        {
            self.showLoader()
            NewtworkManager.shared.GetAllCookActiveInactiveDetails { (status, datain) in
                self.hideloader()
                
                if (status)
                {
                    self.allCookArray = (datain as? [DKAPICookResponseElement])!
                    DispatchQueue.main.async {
                        if self.selectedTypeorder == self.active
                        {
                            self.ChnagebuttonStatus(self.btnNewOrder)
                        }
                        else {
                            self.ChnagebuttonStatus(self.btnorderaccepted)
                        }
                        
                    }
                }
            }
        }
        
        if(userType == USERTYPE.HOUSE)
        {
            self.showLoader()
            NewtworkManager.shared.GetAllHomeDetails { (status, dataIn) in
                self.hideloader()
                if (status)
                {
                    self.houseArray = (dataIn as? [DKAPIHomeResponseElement])!
                    DispatchQueue.main.async {
                        self.tblView.reloadData()
                    }
                }
            }
        }
        
        if(userType == USERTYPE.MENU)
        {
            self.showLoader()
            
            NewtworkManager.shared.GetAllMenuDetails { (status, dataIn) in
                self.hideloader()
                if (status)
                {
                    self.menuArray = (dataIn as? [DKAPIMenuResponseElement])!
                    DispatchQueue.main.async {
                        self.tblView.reloadData()
                    }
                }
            }
        }
        if(userType == USERTYPE.CATEGORY)
        {
            self.showLoader()
            NewtworkManager.shared.GetAllCategoryDetails() { (status, dataIn) in
                self.hideloader()
                if (status)
                {
                    self.categoryArray = (dataIn as? [DKAPICatDetail])!
                    DispatchQueue.main.async {
                        self.tblView.reloadData()
                    }
                }
            }
            //get category api call
            
        }
        
    }
    
    private func getMenuItemsByCategoryID(catID: String){
        
        self.showLoader()
        
        NewtworkManager.shared.GetAllMenuDetailsByCategoryID(categoryID: catID) { (status, dataIn) in
            self.hideloader()
            if (status)
            {
                
                self.menuArray = (dataIn as? [DKAPIMenuResponseElement])!
                if self.menuArray.count > 0{
                    
                    self.isMenuApiCalled = true
                    DispatchQueue.main.async {
                         self.gloablHeaderView.IsRightEnable = false
                        self.gloablHeaderView.lblTitle.text = NSLocalizedString("Menus", comment:"")
                        self.tblView.reloadData()
                    }
                } else {
                    self.showAlert(title: NSLocalizedString("Alert!", comment:""), message:NSLocalizedString("No menu items found in selected category", comment:""))
                }
                
            }
        }
        
    }
    //This method will call when you press button.
    @objc func buttonPressed() {
        
        let notiVc = self.storyboard?.instantiateViewController(withIdentifier: "AddHouseViewController") as! AddHouseViewController
        notiVc.modalTransitionStyle = .coverVertical
        notiVc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(notiVc, animated: true, completion: {
            
        })
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if(userType == USERTYPE.USER)
        {
            return usersArray.count
        } else  if(userType == USERTYPE.COOK)
        {
            return cooksArray.count
        } else if(userType == USERTYPE.HOUSE)
        {
            return houseArray.count
        } else if(userType == USERTYPE.MENU)
        {
            return menuArray.count
        }
        else if(userType == USERTYPE.CATEGORY)
        {
            if isMenuApiCalled{
                return menuArray.count
            } else {
                return categoryArray.count
            }
        }
        return 0
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell  = (tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? UITableViewCell)!
        let viewCOnatiner = cell.viewWithTag(100) as! HomeView
        viewCOnatiner.setLabeldateTime("")
        //viewCOnatiner.setImageRight("layer_\(indexPath.row+1)")
        
        if(userType == USERTYPE.USER)
        {
            let model = usersArray[indexPath.row]
            viewCOnatiner.UpdateUi(model.userName ,"user_icon")
            viewCOnatiner.setData(model)
            viewCOnatiner.setLabeldateTime(model.userGroup.capitalized)
            if model.isActive == false
            {
                viewCOnatiner.IsActive(true)
            } else {
                viewCOnatiner.IsActive(false)
            }
            
            viewCOnatiner.complitionHandlerWithData = {
                (name,dataIn) in
                
                let element = dataIn as! DKAPIUserResponseElement
                var activeState = false
                if name == "active"
                {
                    activeState = true
                }
                
                self.showLoader()
                
                NewtworkManager.shared.DeleteUserRoles("\((element.userID))", activeState, callback: { (status, dataIn) in
                    self.hideloader()
                    if(status)
                    {
                        if name == "active"
                        {
                            self.ApiCall()
                            
                        }
                        else
                        {
                            self.ApiCall()
                            //                        DispatchQueue.main.async
                            //                            {
                            //                                self.showAlert(title: "", message: "Deleted Successfully")
                            //                                self.usersArray.remove(at: indexPath.row)
                            //                                self.tblView.reloadData()
                            //                        }
                        }
                        
                    } else {
                        
                         if name == "active"
                         {
                            self.showAlert(title: "", message: NSLocalizedString("Unable to activate,Please try again", comment:""))
                         }
                         else
                         {
                            self.showAlert(title: "", message: NSLocalizedString("Unable to delete,Please try again", comment:""))
                        }
                        
                    }
                    
                })
            }
            
        }
        if(userType == USERTYPE.COOK)
        {
            let model = cooksArray[indexPath.row]
            if let keyGuest  = UserDefaults.standard.value(forKey: defaultsKeys.langSelected) {
                if keyGuest as! String == ARABIC {
                    if model.cookName_AR.length > 0{
                         viewCOnatiner.UpdateUi(model.cookName_AR ?? "")
                    } else {
                        viewCOnatiner.UpdateUi(model.cookName ?? "")
                    }
                   
                } else {
                    viewCOnatiner.UpdateUi(model.cookName ?? "")
                }
            } else {
                viewCOnatiner.UpdateUi(model.cookName ?? "")
            }
            
            viewCOnatiner.setData(model)
            if isApplanguageIsArabic{
                viewCOnatiner.setLabeldateTime((model.cookDesc_AR.isEmptyAndNil == false ? model.cookDesc_AR : model.cookDesc))
            } else {
                viewCOnatiner.setLabeldateTime( model.cookDesc)
            }
            if model.isActive == false
            {
                viewCOnatiner.IsActive(true)
            } else {
                viewCOnatiner.IsActive(false)
            }
            viewCOnatiner.complitionHandlerWithData =
                {
                (name,dataIn) in
                    
                    var activeState = false
                    if name == "active"
                    {
                        activeState = true
                    }
                let element = dataIn as! DKAPICookResponseElement
                NewtworkManager.shared.DeleteCookRoles("\((element.cookID!))", activeState, callback: { (status, dataIn) in
                    self.hideloader()
                    if(!status)
                    {
                        if name == "active"
                        {
                           self.showAlert(title: "", message: NSLocalizedString("Unable to activate,Please try again", comment:""))
                        } else {
                            self.showAlert(title: "", message: NSLocalizedString("Unable to delete,Please try again", comment:""))
                        }
                        
                        return
                    }
                    DispatchQueue.main.async {
                        if name == "active"
                        {
                           self.showAlert(title: "", message: NSLocalizedString("Activated Successfully", comment:""))
                        }else
                        {
                           self.showAlert(title: "", message: NSLocalizedString("Deleted Successfully", comment:""))
                        }
                        
                        
                    }
                    self.ApiCall()
                })
            }
        }
        if(userType == USERTYPE.HOUSE)
        {
            let model = houseArray[indexPath.row]
            if let keyGuest  = UserDefaults.standard.value(forKey: defaultsKeys.langSelected) {
                if keyGuest as! String == ARABIC {
                    if model.homeTitle_AR.length > 0{
                        viewCOnatiner.UpdateUi(model.homeTitle_AR ,"house")
                    } else {
                        viewCOnatiner.UpdateUi(model.homeTitle ,"house")
                    }
                    
                } else {
                    viewCOnatiner.UpdateUi(model.homeTitle ,"house")
                }
            } else {
                viewCOnatiner.UpdateUi(model.homeTitle ,"house")
            }
            
            viewCOnatiner.setData(model)
            
            if isApplanguageIsArabic{
                viewCOnatiner.setLabeldateTime(model.location + "\n" + (model.homeDesc_AR.isEmptyAndNil == false ? model.homeDesc_AR : model.homeDesc))
            } else {
                viewCOnatiner.setLabeldateTime(model.location + "\n" + model.homeDesc)
            }
            
            
            viewCOnatiner.complitionHandlerWithData = {
                (name,dataIn) in
                let element = dataIn as! DKAPIHomeResponseElement
                NewtworkManager.shared.DeleteHomeRoles("\((element.homeID))", false, callback: { (status, dataIn) in
                    self.hideloader()
                    if(!status)
                    {
                        self.showAlert(title: "", message: NSLocalizedString("Unable to delete,Please try again", comment:""))
                        return
                    }
                    DispatchQueue.main.async {
                        self.showAlert(title: "", message: NSLocalizedString("Deleted Successfully", comment:""))
                        self.houseArray.remove(at: indexPath.row)
                        self.tblView.reloadData()
                    }
                })
            }
        }
        if(userType == USERTYPE.MENU)
        {
            let model = menuArray[indexPath.row]
            if let keyGuest  = UserDefaults.standard.value(forKey: defaultsKeys.langSelected) {
                if keyGuest as! String == ARABIC {
                    if model.menuItemName_AR.length > 0{
                        viewCOnatiner.UpdateUi(model.menuItemName_AR ?? "","menu_list")
                    } else {
                        viewCOnatiner.UpdateUi(model.menuItemName ?? "","menu_list")
                    }
                    
                } else {
                    viewCOnatiner.UpdateUi(model.menuItemName ?? "","menu_list")
                }
            } else {
                viewCOnatiner.UpdateUi(model.menuItemName ?? "","menu_list")
            }
            
            
            viewCOnatiner.setData(model)
//            if isApplanguageIsArabic{
//                viewCOnatiner.setLabeldateTime((model.menuItemDesc_AR.isEmptyAndNil == false ? model.menuItemDesc_AR : model.menuItemDesc))
//            } else {
//                viewCOnatiner.setLabeldateTime(model.menuItemDesc)
//            }
            viewCOnatiner.lblHeaderTopConstraint.constant = 15
            viewCOnatiner.lbldatetime.isHidden = true
            viewCOnatiner.setImageRemote(model.fullImagePath, placeholder: "Menu")
            viewCOnatiner.complitionHandlerWithData = {
                (name,dataIn) in
                let element = dataIn as! DKAPIMenuResponseElement
                NewtworkManager.shared.DeleteMenuRoles("\((element.menuItemID)!)", false, callback: { (status, dataIn) in
                    self.hideloader()
                    if(!status)
                    {
                        self.showAlert(title: "", message: NSLocalizedString("Unable to delete,Please try again", comment:""))
                        return
                    }
                    DispatchQueue.main.async {
                        self.showAlert(title: "", message: NSLocalizedString("Deleted Successfully", comment:""))
                        self.menuArray.remove(at: indexPath.row)
                        self.tblView.reloadData()
                    }
                })
            }
        }
        if(userType == USERTYPE.CATEGORY)
        {
            if isMenuApiCalled{
                let model = menuArray[indexPath.row]
                
                if let keyGuest  = UserDefaults.standard.value(forKey: defaultsKeys.langSelected) {
                    if keyGuest as! String == ARABIC {
                        if model.menuItemName_AR.length > 0{
                            viewCOnatiner.UpdateUi(model.menuItemName_AR ?? "","menu_list")
                        } else {
                            viewCOnatiner.UpdateUi(model.menuItemName ?? "","menu_list")
                        }
                        
                    } else {
                        viewCOnatiner.UpdateUi(model.menuItemName ?? "","menu_list")
                    }
                } else {
                    viewCOnatiner.UpdateUi(model.menuItemName ?? "","menu_list")
                }
                viewCOnatiner.setData(model)
//                if isApplanguageIsArabic{
//                    viewCOnatiner.setLabeldateTime((model.menuItemDesc_AR.isEmptyAndNil == false ? model.menuItemDesc_AR : model.menuItemDesc))
//                } else {
//                    viewCOnatiner.setLabeldateTime(model.menuItemDesc)
//                }
                viewCOnatiner.lblHeaderTopConstraint.constant = 15
                viewCOnatiner.lbldatetime.isHidden = true
                viewCOnatiner.setImageRemote(model.fullImagePath, placeholder: "Menu")
                viewCOnatiner.complitionHandlerWithData = {
                    (name,dataIn) in
                    let element = dataIn as! DKAPIMenuResponseElement
                    NewtworkManager.shared.DeleteMenuRoles("\((element.menuItemID)!)", false, callback: { (status, dataIn) in
                        self.hideloader()
                        if(!status)
                        {
                            self.showAlert(title: "", message: NSLocalizedString("Unable to delete,Please try again", comment:""))
                            return
                        }
                        DispatchQueue.main.async {
                            self.showAlert(title: "", message: NSLocalizedString("Deleted Successfully", comment:""))
                            self.menuArray.remove(at: indexPath.row)
                            self.tblView.reloadData()
                        }
                    })
                }
            } else {
                let model = categoryArray[indexPath.row]
                
                if let keyGuest  = UserDefaults.standard.value(forKey: defaultsKeys.langSelected) {
                    if keyGuest as! String == ARABIC {
                        if model.catName_AR.length > 0{
                             viewCOnatiner.UpdateUi(model.catName_AR ?? "","cat_\(model.catID!)")
                        } else {
                             viewCOnatiner.UpdateUi(model.catName ?? "","cat_\(model.catID!)")
                        }
                        
                    } else {
                         viewCOnatiner.UpdateUi(model.catName ?? "","cat_\(model.catID!)")
                    }
                } else {
                     viewCOnatiner.UpdateUi(model.catName ?? "","cat_\(model.catID!)")
                }
               
                viewCOnatiner.setData(model)
                viewCOnatiner.lblHeaderTopConstraint.constant = 15
//                if isApplanguageIsArabic{
//                    viewCOnatiner.setLabeldateTime((model.catDesc_AR.isEmptyAndNil == false ? model.catDesc_AR : model.catDesc))
//                } else {
//                    viewCOnatiner.setLabeldateTime(model.catDesc)
//                }
                viewCOnatiner.lbldatetime.isHidden = true
                viewCOnatiner.setImageRemote(model.catImageFullPath, placeholder: "Menu")
                //viewCOnatiner.btnDelete.isHidden = true
                viewCOnatiner.complitionHandlerWithData = {
                    (name,dataIn) in
                    //let element = dataIn as! DKMenuCatDetails
                    self.showLoader()
                    NewtworkManager.shared.DeleteCategory(model.catID ?? "0", false, callback: { (status, dataIn) in
                        self.hideloader()
                        if(!status)
                        {
                            self.showAlert(title: "", message: NSLocalizedString("Unable to delete,Please try again", comment:""))
                            return
                        }
                        DispatchQueue.main.async {
                            self.categoryArray.remove(at: indexPath.row)
                            self.tblView.reloadData()
                        }
                    })
                }
            }
            
        }
        return cell
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if userType != USERTYPE.MENU && userType != USERTYPE.CATEGORY
        {
            return UITableView.automaticDimension
        }
        return 70
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if userType == .CATEGORY && !isMenuApiCalled
        {
            self.getMenuItemsByCategoryID(catID: categoryArray[indexPath.row].catID)
        }
        else if userType == .USER
        {
            let editVc = self.storyboard?.instantiateViewController(withIdentifier: "EditFormVC") as! EditFormVC
            editVc.selectedDetailsID = usersArray[indexPath.row].userID
            editVc.userType = userType
            editVc.complitionHandler =
                { (status,data) in
                    if status == true
                    {
                        self.ApiCall()
                    }
            }
            self.navigationController?.pushViewController(editVc, animated: false)
        }
        else if userType == .COOK
        {
            let editVc = self.storyboard?.instantiateViewController(withIdentifier: "EditFormVC") as! EditFormVC
            editVc.selectedDetailsID = cooksArray[indexPath.row].cookID
            editVc.userType = userType
            editVc.complitionHandler =
                { (status,data) in
                    if status == true
                    {
                        self.ApiCall()
                    }
            }
            self.navigationController?.pushViewController(editVc, animated: false)
        } else  if userType == .MENU
        {
            let model = menuArray[indexPath.row]
            let arritems = DKAPIOrderElement.init()
            arritems.menuItemID = model.menuItemID
            arritems.userComment = model.menuItemDesc
            arritems.cookComment = model.menuItemDesc_AR
            arritems.fullImagePath = model.fullImagePath
            
            DispatchQueue.main.async {
                let notiVc = self.storyboard?.instantiateViewController(withIdentifier: "AddHouseViewController") as! AddHouseViewController
                notiVc.modalTransitionStyle = .coverVertical
                notiVc.modalPresentationStyle = .overCurrentContext
                notiVc.arritems = arritems
                notiVc.tappedMenuItem = model
                self.navigationController?.present(notiVc, animated: true, completion: {
                    
                })
            }
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
