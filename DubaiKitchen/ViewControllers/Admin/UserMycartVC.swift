//
//  OrderVC.swift
//  DubaiKitchen
//
//  Created by Pankaj Kumhar on 2/28/19.
//  Copyright © 2019 Pankaj Kumhar. All rights reserved.
//

import UIKit

class UserMycartVC: CommonViewController
{
    
    var catagoryId:Int = 0
    var celldes:UITableViewCell? = nil
    var viewdes:ViewDescription!
    var cellselecthouse:UITableViewCell? = nil
    var txtFiled:UITextField!
    var  buttonhouse:UIButton!
    var cellplaceorder:UITableViewCell? = nil
    var buttonplaceorder:UIButton! = nil
    var celldatetime:UITableViewCell? = nil
    var viewdateTime :ViewDateTime! = nil
    var celldeleveryloc:UITableViewCell? = nil
    var celldeleveryDatetimehead:UITableViewCell? = nil
    private var selectedHouseObj = DKUserHomeDetails()
    var selectionPickerView: UIPickerView! = UIPickerView()
    var IsComeFromNotifications:Bool = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        //addGradientColors_Nav()
        tblView.rowHeight = UITableView.automaticDimension
        tblView.estimatedRowHeight = 44
        self.title = NSLocalizedString("My Cart", comment:"")
        self.updateNavTitle(NSLocalizedString("My Cart", comment:""), true) { (name) in
            if(name == "right")
            {
                return
            }
             DispatchQueue.main.async {
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: USerVC.self) {
                        _ =  self.navigationController!.popToViewController(controller, animated: false)
                        break
                    }
                }
                        //self.navigationController?.popViewController(animated: false)
                    }
        }
        tblView.register(UINib(nibName: "AdminTVCell", bundle: nil), forCellReuseIdentifier: "cellAdminCommon")
        tblView.tableFooterView = UIView()
        tblView.tableHeaderView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 20, height: 30))
        // Do any additional setup after loading the view.
        selectionPickerView.delegate = self
        celldes = tblView.dequeueReusableCell(withIdentifier: "celldes")
        viewdes = celldes?.viewWithTag(100) as? ViewDescription
        celldeleveryloc = tblView.dequeueReusableCell(withIdentifier: "celldeleveryloc")
        cellselecthouse = tblView.dequeueReusableCell(withIdentifier: "cellselecthouse")
        txtFiled = (cellselecthouse?.viewWithTag(100) as! UITextField)
        buttonhouse = (cellselecthouse?.viewWithTag(101) as! UIButton)
        cellplaceorder = tblView.dequeueReusableCell(withIdentifier: "cellplaceorder")
        buttonplaceorder = (cellplaceorder?.viewWithTag(100) as! UIButton)
        celldatetime = tblView.dequeueReusableCell(withIdentifier: "celldatetime")
        viewdateTime = (celldatetime?.viewWithTag(100) as! ViewDateTime)
        
         celldeleveryDatetimehead = tblView.dequeueReusableCell(withIdentifier: "celldeleveryloc")
        let uilbl = celldeleveryDatetimehead?.viewWithTag(100) as! UILabel
        uilbl.text = NSLocalizedString("Select delivery date time", comment:"")
        
        txtFiled.inputView = selectionPickerView
        buttonhouse.addTarget(self, action: #selector(connected(sender:)), for: .touchUpInside)
        buttonplaceorder.addTarget(self, action: #selector(placeoprder(sender:)), for: .touchUpInside)
        
        if !IsComeFromNotifications
        {
            getAllHouses()
        } else {
            
            getAllHouses()
        }
        
    }
    
    @objc func connected(sender: UIButton)
    {
        self.txtFiled.becomeFirstResponder()
    }
    
    func generateRandomDigits(_ digitNumber: Int) -> String {
        var number = ""
        for i in 0..<digitNumber {
            var randomNumber = arc4random_uniform(10)
            while randomNumber == 0 && i == 0 {
                randomNumber = arc4random_uniform(10)
            }
            number += "\(randomNumber)"
        }
        return number
    }
    
    @objc func placeoprder(sender: UIButton)
    {
        if arrUserCartItems.count > 0
        {
            if(selectedHouseObj.homeID.isEmpty)
            {
                self.showAlert(title: "", message: NSLocalizedString("Please select house", comment:""))
                return
            }
            
            if(viewdateTime.selectedDate.isEmpty)
            {
                self.showAlert(title: "", message: NSLocalizedString("Please select order delivery date and time", comment:""))
                return
            }
            
            let ordernum = generateRandomDigits(4)
            var arrDict = [[String:Any]]()
            for model in arrUserCartItems
            {
                model.orderDate = viewdateTime.orderdate
                model.deliveryDate = viewdateTime.selectedDate
                model.userHomeID = (NewtworkManager.shared.selectedUser?.homeDetails.homeID)!
                model.deliveryHomeID = selectedHouseObj.homeID
                model.orderStatus = NSLocalizedString("Order Placed", comment:"") // accept == Order Received // Order Delivered // Cancel
                model.orderID = ordernum
                model.userID = NewtworkManager.shared.logoinUserid
                model.userComment = viewdes.txtView.text
                arrDict.append(model.toDictionary())
            }
            var yourString : String = ""
            do
            {
                if let postData : NSData = try JSONSerialization.data(withJSONObject: arrDict, options: JSONSerialization.WritingOptions.prettyPrinted) as NSData
                {
                    yourString = NSString(data: postData as Data, encoding: String.Encoding.utf8.rawValue)! as String
                    
                    self.showLoader()
                    NewtworkManager.shared.OrderPlaceOrder(yourString) { (status, datain) in
                        self.hideloader()
                        
                        if(status)
                        {
                            arrUserCartItems = [DKAPIOrderElement]()
                            DispatchQueue.main.async
                                {
                                    self.tblView.reloadData()
                                    self.viewdateTime.selectedDate = ""
                                    let refreshAlert = UIAlertController(title: NSLocalizedString("Success", comment:""), message: NSLocalizedString("Order Placed", comment:""), preferredStyle: UIAlertController.Style.alert)
                                    
                                    refreshAlert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment:""), style: .default, handler: { (action: UIAlertAction!) in
                                        DispatchQueue.main.async {
                                            self.navigationController?.popViewController(animated: false)
                                        }
                                    }))
                                    
                                    
                                    self.present(refreshAlert, animated: true, completion: nil)
                                    
                                    
                            }
                        } else {
                            
                            self.alert(message: NSLocalizedString("Unable to place order. Please try again", comment:""), title: "")
                        }
                        
                    }
                }
            }
            catch
            {
                print(error)
            }
            
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0)
        {
            return arrUserCartItems.count
        }
        return arrUserCartItems.count > 0 ? 6 : 0
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0)
        {
            let cell  = (tableView.dequeueReusableCell(withIdentifier: "cellcart", for: indexPath) as? UITableViewCell)
            let viewController = cell?.viewWithTag(100) as! UserCartView
            let model =  arrUserCartItems[indexPath.row]
            if let keyGuest  = UserDefaults.standard.value(forKey: defaultsKeys.langSelected) {
                if keyGuest as! String == ARABIC {
                    if model.menuItemname_AR.length > 0{
                        viewController.UpdateUi(model.menuItemname_AR ,"MyCart")
                    } else {
                        viewController.UpdateUi(model.menuItemname ,"MyCart")
                    }
                    
                } else {
                    viewController.UpdateUi(model.menuItemname ,"MyCart")
                }
            } else {
                viewController.UpdateUi(model.menuItemname ,"MyCart")
            }
            //viewController.UpdateUi("\(model.menuItemname)", "MyCart")
            viewController.setImageRight("");//"layer_\(indexPath.row+1)")
            viewController.setdataObject(model)
            viewController.setImageRemote(model.fullImagePath ?? "", placeholder: "MyCart")
            viewController.complitionHandler = { name in
                DispatchQueue.main.async {
                    self.tblView.reloadData()
                }
            }
            return cell!
        }
        else {
            if(indexPath.row == 0)
            {
                return celldes!
            }
            else
                if(indexPath.row == 1)
                {
                    return celldeleveryloc!
                }
                else
                    if(indexPath.row == 2)
                    {
                        return cellselecthouse!
                    }
                    else
                        if(indexPath.row == 5)
                        {
                            return cellplaceorder!
                        } else
                            if(indexPath.row == 3)
                            {
                                return celldeleveryDatetimehead!
                                
                            }
                            else
                                if(indexPath.row == 4)
                                {
                                    return celldatetime!
                                    
            }
            
            
        }
        
        return UITableViewCell()
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.section == 0 ? 80 : UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if(indexPath.section == 0)
        {
            DispatchQueue.main.async {
                let model =  arrUserCartItems[indexPath.row]
                let notiVc = self.storyboard?.instantiateViewController(withIdentifier: "AddHouseViewController") as! AddHouseViewController
                notiVc.modalTransitionStyle = .coverVertical
                notiVc.modalPresentationStyle = .overCurrentContext
                notiVc.arritems = model
                self.navigationController?.present(notiVc, animated: true, completion: {
                    
                })
            }
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    private var houseArray:[DKAPIHomeResponseElement] = [DKAPIHomeResponseElement]()
    //MARK:- API calls
    private func getAllHouses()
    {
        self.showLoader()
        NewtworkManager.shared.GetAllHomeDetails { (status, dataIn) in
            self.hideloader()
            if (status)
            {
                if let datahouse = dataIn
                {
                   self.houseArray = datahouse as? [DKAPIHomeResponseElement] ?? [DKAPIHomeResponseElement]()
                    DispatchQueue.main.async {
                        self.selectionPickerView.reloadAllComponents()
                    }
                }
                
            }
        }
    }
}

extension UserMycartVC: UIPickerViewDelegate, UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        
        return houseArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        var strTitle = ""
        
        strTitle  = houseArray[row].homeTitle+"-"+houseArray[row].location
        
        if (isApplanguageIsArabic)
        {
            let homeTitle = houseArray[row].homeTitle_AR.isEmptyAndNil == false ? houseArray[row].homeTitle_AR : houseArray[row].homeTitle
            let houseLoc = houseArray[row].location_ar.isEmptyAndNil == false ? houseArray[row].location_ar : houseArray[row].location
            strTitle  = homeTitle+"-"+houseLoc
        }
        return strTitle
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        selectedHouseObj.homeID = houseArray[row].homeID
        selectedHouseObj.homeTitle = houseArray[row].homeTitle
        selectedHouseObj.homeDesc = houseArray[row].homeDesc
        selectedHouseObj.homeTitle_ar = houseArray[row].homeTitle_AR.isEmptyAndNil == false ? houseArray[row].homeTitle_AR : houseArray[row].homeTitle
         selectedHouseObj.homeDesc_ar = houseArray[row].homeDesc_AR.isEmptyAndNil == false ? houseArray[row].homeDesc_AR : houseArray[row].homeDesc
        selectedHouseObj.location_ar = houseArray[row].location_ar.isEmptyAndNil == false ? houseArray[row].location_ar : houseArray[row].location
        selectedHouseObj.location = houseArray[row].location
        
        self.buttonhouse.setTitle(selectedHouseObj.homeTitle+"-"+selectedHouseObj.location, for: .normal)
        if (isApplanguageIsArabic)
        {
            self.buttonhouse.setTitle(selectedHouseObj.homeTitle_ar+"-"+selectedHouseObj.location_ar, for: .normal)
        }
    }
}

extension String {
    
    var isEmptyAndNil:Bool
    {
        guard self != nil  else
        {
            return true
        }
        
        if self.isEmpty
        {
            return true
        }
        return false
    }
}

