//
//  AdminVC.swift
//  DubaiKitchen
//
//  Created by Pankaj Kumhar on 2/22/19.
//  Copyright © 2019 Pankaj Kumhar. All rights reserved.
//

import UIKit

class AdminVC: CommonViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        getAllUsers()
        //addGradientColors_Nav()
        //self.navigationController?.navigationBar.isHidden = false
        switch selectedUserRole{
        case 1:
            self.updateNavTitle(NSLocalizedString("Admin", comment:""))
        case 2:
            self.updateNavTitle(NSLocalizedString("User", comment:""))
        case 3:
            self.updateNavTitle(NSLocalizedString("Cook", comment:""))
        default:
            print("")
        }
        //self.title = "Admin"
        
        tblView.register(UINib(nibName: "AdminTVCell", bundle: nil), forCellReuseIdentifier: "cellAdminCommon")
        tblView.tableFooterView = UIView()
        self.view.layoutIfNeeded()
        // Do any additional setup after loading the view.
    }
    private func getAllUsers()
    {
        self.showLoader()
        NewtworkManager.shared.GetAllActiveInActiveUserDetails { (status, dataIn) in
            self.hideloader()
            if (status)
            {
                arrGlobalActiveInactiveUsers = (dataIn as? [DKAPIUserResponseElement])!
               if NewtworkManager.shared.logoinUserType == NewtworkManager.shared.CookLogOn ||  NewtworkManager.shared.logoinUserType == NewtworkManager.shared.KichtenLogOn
               {
                    if (Int(notificationOrderID) != -1)
                    {
                        self.Navigate()
                    }
                }
            }
        }
    }
    
    func Navigate()
    {
         let topController = self
            topController.showLoader()
            topController.getAllOrderByOrderId(callback: { (status, datain) in
                topController.hideloader()
                if status
                {
                    let arr  = datain as! [DKAPIOrderElement]
                    DispatchQueue.main.async
                        {
                            notificationOrderID = "-1"
                            let notiVc = topController.storyboard?.instantiateViewController(withIdentifier: "OrderDetailsVC") as! OrderDetailsVC
                            notiVc.arrOrderByKitchen = arr
                            topController.navigationController?.pushViewController(notiVc, animated: false)
                    }
                }
                
            })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:AdminTVCell  = (tableView.dequeueReusableCell(withIdentifier: "cellAdminCommon", for: indexPath) as? AdminTVCell)!
        
        //cell.imgBubble.image = UIImage.init(named: "layer_\(indexPath.row+1)")
        cell.imgBubble.isHidden = true
        switch indexPath.row
        {
        case 3:
            
            cell.lblTitle.text = NSLocalizedString("Logout", comment:"")
            cell.imgViewLogo.image = UIImage.init(named: "Logout")
            //cell.imgBubble.image = UIImage.init(named: "Shape-4")
        case 0:
            switch selectedUserRole{
                case 1:
                    cell.lblTitle.text = NSLocalizedString("Management", comment:"")
                    cell.imgViewLogo.image = UIImage.init(named: "Management")
                case 2:
                    cell.lblTitle.text = NSLocalizedString("Management", comment:"")
                    cell.imgViewLogo.image = UIImage.init(named: "Management")
                case 3:
                    cell.lblTitle.text = NSLocalizedString("Dashboard", comment:"")
                    cell.imgViewLogo.image = UIImage.init(named: "Dashboard")
                default:
                print("")
            }
            //cell.imgBubble.image = UIImage.init(named: "Shape-1")
            
        case 2:

            if selectedUserRole == 1{
            cell.lblTitle.text = NSLocalizedString("Dashboard", comment:"")
            cell.imgViewLogo.image = UIImage.init(named: "Dashboard")
            } else if selectedUserRole == 3{
                cell.lblTitle.text = NSLocalizedString("Notification", comment:"")
                cell.imgViewLogo.image = UIImage.init(named: "notification")
            }
            
        case 1:
            switch selectedUserRole{
            case 1:
                cell.lblTitle.text = NSLocalizedString("Notification", comment:"")
                cell.imgViewLogo.image = UIImage.init(named: "notification")
            case 2:
                cell.lblTitle.text = NSLocalizedString("Dashboard", comment:"")
                cell.imgViewLogo.image = UIImage.init(named: "Dashboard")
            case 3:
                cell.lblTitle.text = NSLocalizedString("Order", comment:"")
                cell.imgViewLogo.image = UIImage.init(named: "Menu")?.maskWithColor(color: UIColor.init(hexString: "#5E5E5E")!)
                //cell.imgViewLogo.image = UIImage.init(named: "Menu")
            default:
                print("")
            }
        
        default:
            print("")
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedFooterOption = indexPath.row + 1
        switch indexPath.row {
        case 3:
            print("logout")
            DispatchQueue.main.async {
                selectedFooterOption = -1
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                
                let initialViewController = storyboard.instantiateViewController(withIdentifier: "initialnavigationviewcontroller")
                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                appdelegate.window?.rootViewController = initialViewController
            }
        case 0:
            if selectedUserRole == 3{
                let notiVc = self.storyboard?.instantiateViewController(withIdentifier: "DashboardVc") as! DashboardVc
                self.navigationController?.pushViewController(notiVc, animated: false)
            } else if selectedUserRole == 2{
                
            } else {
                let notiVc = self.storyboard?.instantiateViewController(withIdentifier: "ManagementVC") as! ManagementVC
                self.navigationController?.pushViewController(notiVc, animated: false)
            }
            
        case 1:
            if selectedUserRole == 1
            {
                
                let notiVc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
                self.navigationController?.pushViewController(notiVc, animated: false)
            }
            else if selectedUserRole == 3
            {
                let notiVc = self.storyboard?.instantiateViewController(withIdentifier: "OrderVC") as! OrderVC
                self.navigationController?.pushViewController(notiVc, animated: false)
            }
           
        case 2:
            if selectedUserRole == 1{
                
                let notiVc = self.storyboard?.instantiateViewController(withIdentifier: "DashboardVc") as! DashboardVc
                self.navigationController?.pushViewController(notiVc, animated: false)
            
            } else if selectedUserRole == 3{
                let notiVc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
                self.navigationController?.pushViewController(notiVc, animated: false)
            }
        default:
            print("")
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
