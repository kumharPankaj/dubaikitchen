//
//  NotificationVC.swift
//  DubaiKitchen
//
//  Created by Pankaj Kumhar on 2/23/19.
//  Copyright © 2019 Pankaj Kumhar. All rights reserved.
//

import UIKit


class NotificationAllOrderVC: CommonViewController {

    private var userTypeSelected = 1
    var arrNotification = [1,2,3,4,5,6,7,8,9,10,11]
    var titleNavigation = NSLocalizedString("Notifications", comment:"")
   
    @IBAction func btnViewAllOrderClick(_ sender: ButtonExtender)
    {
    
        
    }
    
    @IBOutlet weak var btnToday: UIButton!
   
    @IBOutlet weak var btnThreeDays: UIButton!
    @IBOutlet weak var btnOneWeek: UIButton!
    
    @IBOutlet weak var btnOneMonth: UIButton!
    @IBOutlet weak var viewAllOrderHeightConstraints: NSLayoutConstraint!
    @IBOutlet weak var stackViewHeader: UIView!//UIStackView!
    @IBOutlet weak var constUserRoleNotiHeight: NSLayoutConstraint!
    @IBOutlet weak var constFilterHeight: NSLayoutConstraint!
    @IBOutlet weak var viewFilter: UIView!
    private var arrOrderByKitchen = [DKAPIOrderElement]()
    private var dictOrderBymenu = [String:DKAPIMenuResponseElement]()
    private var groupedOrderByID = [String:[DKAPIOrderElement]]()
    private var arrAllNotiForAdmin = [DKAPIOrderElement]()
    private var dictOrderBymenu1 = [String:DKAPIMenuResponseElement]()
    var menuArray:[DKAPIMenuResponseElement] = [DKAPIMenuResponseElement]()
    var filterType:FilterType = .None
    var arrAllNotiOrders = [NotificationOrderElements]()
    //private var dictOrderByKitchen = [String:[DKAPIOrderElement]]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnToday.setTitle(NSLocalizedString("Today", comment:""), for: .normal)
        btnOneWeek.setTitle(NSLocalizedString("1 Week", comment:""), for: .normal)
        btnOneMonth.setTitle(NSLocalizedString("1 Month", comment:""), for: .normal)
        btnThreeDays.setTitle(NSLocalizedString("3 days", comment:""), for: .normal)
        self.title = NSLocalizedString("Notification", comment:"")
        self.updateNavTitle(NSLocalizedString("Order", comment:""), true) { (name) in
            if(name == "right")
            {
                if self.viewFilter.isHidden{
                    self.viewFilter.isHidden = false
                    self.constFilterHeight.constant = 120
                    UIView.animate(withDuration: 0.5) {
                        self.view.layoutIfNeeded()
                    }
                    
                } else {
                    self.constFilterHeight.constant = 0
                    UIView.animate(withDuration: 0.5) {
                        self.view.layoutIfNeeded()
                        self.viewFilter.isHidden = true
                    }
                    
                }
                
            } else {
                DispatchQueue.main.async {
                    
                    if(seletcted_Role == NewtworkManager.shared.UserLogOn)
                    {
                       
                        for controller in self.navigationController!.viewControllers as Array {
                            if controller.isKind(of: USerVC.self) {
                                _ =  self.navigationController!.popToViewController(controller, animated: false)
                                break
                            }
                        }
                    } else {
                        for controller in self.navigationController!.viewControllers as Array {
                            if controller.isKind(of: AdminVC.self) {
                                _ =  self.navigationController!.popToViewController(controller, animated: false)
                                break
                            }
                        }
                       
                    }
//                    self.navigationController?.popViewController(animated: false)
                }
            }
        }
        
        
        tblView.register(UINib(nibName: "NotificationCell", bundle: nil), forCellReuseIdentifier: "cellNotification")
        tblView.tableFooterView = UIView()
        
        if selectedUserRole != 1{
            constUserRoleNotiHeight.constant = 0
            stackViewHeader.isHidden = true
        }
        
        self.view.layoutIfNeeded()
        //if selectedUserRole == 2 {
        tblView.estimatedRowHeight = 100
        tblView.rowHeight = UITableView.automaticDimension
        
        ChnagesTagsBg(1)
        Getallmenus()
        
            viewAllOrderHeightConstraints.constant = 0
//            if(seletcted_Role == NewtworkManager.shared.UserLogOn)
//            {
//                viewAllOrderHeightConstraints.constant = 50
//            }
       
        //}
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }
    func getAllOrder()
    {
    self.showLoader()
        NewtworkManager.shared.GetAllOrderByKitchenId("1") { (status, dataIn) in
            self.hideloader()
            if let dataObj = dataIn as? [DKAPIOrderElement]
            {
                var datefrom = Date()
                if(self.filterType == .Today)
                {
                    datefrom = Date().todayDate
                }
                if(self.filterType == .threedays)
                {
                    datefrom = Date().threedays
                }
                if(self.filterType == .Week)
                {
                    datefrom = Date().weekdate
                }
                if(self.filterType == .Month)
                {
                    datefrom = Date().monthdate
                }
                
                var tempObject = [DKAPIOrderElement]()
                 if(self.filterType != .None)
                 {
                    for order in dataObj
                    {
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                        let datedel = dateFormatter.date(from: order.deliveryDate)
                        if let status1 = datedel?.isBetween(Date(),and: datefrom)
                        {
                            if(status1)
                            {
                                print("in the dates")
                                tempObject.append(order)
                            }
                            else {
                                print("not in the dates")
                            }
                        }
                        
                    }
                 } else {
                    tempObject = dataObj
                }
                
                tempObject = tempObject.sorted(by: { Int($0.orderID)! > Int($1.orderID )!})
                
                self.arrOrderByKitchen = tempObject
                self.arrAllNotiForAdmin = tempObject
//                if selectedUserRole == 2
//                {
//                    self.arrOrderByKitchen = self.arrOrderByKitchen.filter({ (objOrder) -> Bool in
//                        return objOrder.userID == NewtworkManager.shared.logoinUserid
//                    })
//                }
//
//                if selectedUserRole == 2{
//                    self.arrOrderByKitchen = self.arrOrderByKitchen.filter({ (objOrder) -> Bool in
//                        return (objOrder.orderStatus == "Order Placed" || objOrder.orderStatus == "Order Recieved" ||  objOrder.orderStatus == "Order Cancelled" || objOrder.orderStatus == "OrderPlaced" ||  objOrder.orderStatus == "Order Accepted" || objOrder.orderStatus == "Order Delivered")
//                    })
//                }
//                else if selectedUserRole == 3
//                {
//                    self.arrOrderByKitchen = self.arrOrderByKitchen.filter({ (objOrder) -> Bool in
//                        return (objOrder.orderStatus == "Order Accepted" || objOrder.orderStatus == "New Order" || objOrder.orderStatus == "Delivered" || objOrder.orderStatus == "Order Placed" || objOrder.orderStatus == "Order Delivered" || objOrder.orderStatus == "OrderPlaced")
//                    })
//                }
                
                    self.groupedOrderByID = Dictionary(grouping: self.arrOrderByKitchen, by: {$0.orderID})
                
                    for obj in self.arrOrderByKitchen
                    {
                        if(self.dictOrderBymenu[obj.orderID] == nil)
                        {
                            let objMenuItem = DKAPIMenuResponseElement.init(fromDictionary: ["MenuItemID":obj.menuItemID, "MenuItemName":obj.menuItemname])
                            self.dictOrderBymenu[obj.orderID] = objMenuItem
                        }
                    }
                
                self.arrAllNotiOrders = [NotificationOrderElements]()
                for (_, values) in self.groupedOrderByID
                {
                    let tempVal = values[0]
                    let tempNotification = NotificationOrderElements()
                    tempNotification.orderElement = tempVal
                    tempNotification.orderDetails = values
                    
                    
                    if let dateorder = tempVal.deliveryDate
                    {
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                        let dateorder = dateFormatter.date(from: dateorder)
                        tempNotification.deliveryDate = dateorder
                    }
                    
                    if let dateorder = tempVal.orderDate
                    {
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                        let dateorder = dateFormatter.date(from: dateorder)
                        tempNotification.orderDate = dateorder
                    }
                    
                    self.arrAllNotiOrders.append(tempNotification)
                }
                
                self.arrAllNotiOrders.sort(by: { $0.deliveryDate.compare($1.deliveryDate) == .orderedDescending })
                
                DispatchQueue.main.async {
                    self.tblView.reloadData()
                }
                
            }
        }
    }
    
    @IBAction func btnNotificationClicked(_ sender: UIButton) {
        self.viewFilter.isHidden = true
        switch sender.tag {
        case 11:
            //today
            let today = Date().todayDate
            self.filterType = .Today
            
            print("")
        case 12:
            //3days
             print("")
             self.filterType = .threedays
        case 13:
            //1 week
             print("")
            self.filterType = .Week
        case 14:
            //1 month
             print("")
            self.filterType = .Month
            
        default:
            print("")
        }
        
       
        
        getAllOrder()
    }
    
    func  ChnagesTagsBg(_ tags:Int)
    {
        let myViews = stackViewHeader.subviews.filter{$0 is UIButton}
        
        for btn in myViews
        {
            if (btn is UIButton)
            {
                let tempbutton = btn as! UIButton
                if (tempbutton.tag == tags)
                {
                    tempbutton.setTitleColor(UIColor.white, for: .normal)
                    
                } else {
                    tempbutton.setTitleColor(UIColor.black, for: .normal)
                }
            }
            
        }
    }
    
    @IBAction func btnUserTypeClicked(_ sender: UIButton) {
        switch sender.tag {
        case 1:
            
            arrOrderByKitchen = arrAllNotiForAdmin
            
        case 2:
            arrOrderByKitchen = arrAllNotiForAdmin
        case 3:
           
            arrOrderByKitchen = arrAllNotiForAdmin.filter({ (objOrder) -> Bool in
                return objOrder.orderStatus == "Order Accepted" || objOrder.orderStatus == "New Order" || objOrder.orderStatus == "Delivered" || objOrder.orderStatus == "Order Placed" || objOrder.orderStatus == "Order Delivered" || objOrder.orderStatus == "OrderPlaced"
            })
        case 4:
            arrOrderByKitchen = arrAllNotiForAdmin.filter({ (objOrder) -> Bool in
                return objOrder.orderStatus == "Order Placed" || objOrder.orderStatus == "Order Recieved" || objOrder.orderStatus == "Order Delivered" || objOrder.orderStatus == "OrderPlaced"
            })
            
        default:
            print("")
        }
        
        ChnagesTagsBg(sender.tag)
        
        self.groupedOrderByID = Dictionary(grouping: self.arrOrderByKitchen, by: {$0.orderID})
        
        for obj in self.arrOrderByKitchen
        {
            if(self.dictOrderBymenu[obj.orderID] == nil)
            {
                let objMenuItem = DKAPIMenuResponseElement.init(fromDictionary: ["MenuItemID":obj.menuItemID, "MenuItemName":obj.menuItemname])
                self.dictOrderBymenu[obj.orderID] = objMenuItem
            }
        }
        
        self.arrAllNotiOrders = [NotificationOrderElements]()
        for (_, values) in self.groupedOrderByID
        {
            let tempVal = values[0]
            let tempNotification = NotificationOrderElements()
            tempNotification.orderElement = tempVal
            tempNotification.orderDetails = values
            
            
            if let dateorder = tempVal.deliveryDate
            {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                let dateorder = dateFormatter.date(from: dateorder)
                tempNotification.deliveryDate = dateorder
            }
            
            if let dateorder = tempVal.orderDate
            {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                let dateorder = dateFormatter.date(from: dateorder)
                tempNotification.orderDate = dateorder
            }
            
            self.arrAllNotiOrders.append(tempNotification)
        }
        
        self.arrAllNotiOrders.sort(by: { $0.deliveryDate.compare($1.deliveryDate) == .orderedDescending })
        
        DispatchQueue.main.async
            {
            UIView.animate(withDuration: 0.5, animations: {
                self.tblView.reloadData()
                self.tblView.setContentOffset(CGPoint.zero, animated: true)
            })
            
        }
       
    
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            return self.arrAllNotiOrders.count//groupedOrderByID.keys.count

    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        //if selectedUserRole == 2{
            let cell:NotificationCell  = (tableView.dequeueReusableCell(withIdentifier: "cellNotification", for: indexPath) as? NotificationCell)!
            
            let arrKeys = Array(groupedOrderByID.keys)
        
            let obj1 = self.arrAllNotiOrders[indexPath.row]//groupedOrderByID[arrKeys[indexPath.row]]
        
            let obj = obj1.orderElement
        
            //let obj = arrOrderByKitchen[indexPath.row]
            cell.lblOrderNo.text = "" //"Order No " + obj![0].orderID
            cell.lblOrderTitle.text =  obj!.menuItemname
        
        var ordrdate = obj!.deliveryDate
        if let dateorder = obj!.deliveryDate
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            let dateorder = dateFormatter.date(from: dateorder)
            dateFormatter.dateFormat = "dd MMM yyyy"
            ordrdate = dateFormatter.string(from: dateorder!)
        }
        
        var ordrdate1 = obj!.orderDate
        if let dateorder = obj!.orderDate
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            let dateorder = dateFormatter.date(from: dateorder)
            dateFormatter.dateFormat = "dd MMM yyyy"
            ordrdate1 = dateFormatter.string(from: dateorder!)
        }
        
        let arrObjetcs = obj1.orderDetails
        if arrObjetcs.count > 0
        {
            let arr = arrGlobalActiveInactiveUsers.filter { (objUser) -> Bool in
                return objUser.userID == arrObjetcs[0].userID
            }
            if arr.count > 0{
                cell.lblOrderNo.text = arr[0].userName
                if NewtworkManager.shared.logoinUserid ==  arr[0].userID{
                    cell.lblOrderStatus.backgroundColor = UIColor.orange
                } else {
                    cell.lblOrderStatus.backgroundColor = UIColor.init(hexString: "00943B")
                }
                if obj!.orderStatus == "Order Placed" || obj!.orderStatus == "Order Recieved"{
                    cell.lblOrderStatus.backgroundColor = UIColor.init(hexString: "00943B")
                    
                    if(NewtworkManager.shared.logoinUserType == NewtworkManager.shared.CookLogOn || NewtworkManager.shared.logoinUserType == NewtworkManager.shared.KichtenLogOn)
                    {
                        cell.lblOrderStatus.text = NSLocalizedString("New Order", comment:"")
                    }
                    else
                    {
                        cell.lblOrderStatus.text = NSLocalizedString("New Order", comment:"")
                    }
                    
                }
                else if obj!.orderStatus == "Order Accepted"
                {
                    //cell.lblOrderStatus.backgroundColor = UIColor.orange
                    cell.lblOrderStatus.text = NSLocalizedString("Preparing", comment:"")
                    
                }
                else if obj!.orderStatus == "Order Delivered"
                {
                    //cell.lblOrderStatus.backgroundColor = UIColor.gray
                    cell.lblOrderStatus.text = NSLocalizedString("Delivered", comment:"")
                    
                }
                else if obj!.orderStatus == "Order Cancelled"
                {
                    //cell.lblOrderStatus.backgroundColor = UIColor.red
                    cell.lblOrderStatus.text = NSLocalizedString("Cancelled", comment:"")
                    
                }
                else  if obj!.orderStatus == "Order Delivered"
                {
                    //cell.lblOrderStatus.backgroundColor = UIColor.init(hexString: "ff0000")
                    cell.lblOrderStatus.text = NSLocalizedString("Delivered", comment:"")
                    
                }
            }
        }
        
        cell.lblOrderTime.text = NSLocalizedString("Delivery Date", comment:"") + " " + ordrdate!
            if obj!.orderStatus == "Order Placed" || obj!.orderStatus == "Order Recieved" ||  obj!.orderStatus == "Order Cancelled"
            {
                cell.imgViewUserType.image = UIImage.init(named: "Menu") //house
            } else {
                cell.imgViewUserType.image = UIImage.init(named: "Menu") // Cook
            }
        
            var strMenuItems = ""
            
            for objOrder in obj1.orderDetails
            {
                let menuid = objOrder.menuItemID
                let menuname = self.dictOrderBymenu1[menuid!]
                if(isApplanguageIsArabic)
                {
                    var menuname1 = menuname?.menuItemName ?? ""
                    if let menuname2 = menuname?.menuItemName_AR
                    {
                        menuname1 = menuname2
                    }
                    strMenuItems = strMenuItems+(menuname1)+" "+"\(convertNumberToLanguage("\(objOrder.orderQty!)","ar_SA"))\n"
                } else {
                    strMenuItems = strMenuItems+(menuname?.menuItemName ?? "")+" "+"\(objOrder.orderQty!)\n"
                }
                
            }
            cell.lblMenuItemName.text = strMenuItems
        
       /* if obj!.orderStatus == "Order Placed" || obj!.orderStatus == "Order Recieved"{
            cell.lblOrderStatus.backgroundColor = UIColor.init(hexString: "00943B")
            
            if(NewtworkManager.shared.logoinUserType == NewtworkManager.shared.CookLogOn || NewtworkManager.shared.logoinUserType == NewtworkManager.shared.KichtenLogOn)
            {
                cell.lblOrderStatus.text = NSLocalizedString("New Order", comment:"")
            }
            else
            {
                cell.lblOrderStatus.text = NSLocalizedString("New Order", comment:"")
            }
            
        }
        else if obj!.orderStatus == "Order Accepted"
        {
             cell.lblOrderStatus.backgroundColor = UIColor.orange
             cell.lblOrderStatus.text = NSLocalizedString("Preparing", comment:"")
            
        }
        else if obj!.orderStatus == "Order Delivered"
        {
             cell.lblOrderStatus.backgroundColor = UIColor.gray
            cell.lblOrderStatus.text = NSLocalizedString("Delivered", comment:"")
           
        }
        else if obj!.orderStatus == "Order Cancelled"
        {
             cell.lblOrderStatus.backgroundColor = UIColor.red
            cell.lblOrderStatus.text = NSLocalizedString("Cancelled", comment:"")
            
        }
        else  if obj!.orderStatus == "Order Delivered"
        {
             cell.lblOrderStatus.backgroundColor = UIColor.init(hexString: "ff0000")
             cell.lblOrderStatus.text = NSLocalizedString("Delivered", comment:"")
            
        }*/
        
            return cell
        
        
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
//        else {
            return UITableView.automaticDimension
//        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
            let obj = self.arrAllNotiOrders[indexPath.row]//groupedOrderByID[arrKeys[indexPath.row]]
            let arrObjetcs = obj.orderDetails
            let notiVc = self.storyboard?.instantiateViewController(withIdentifier: "OrderDetailsVC") as! OrderDetailsVC
            notiVc.arrOrderByKitchen = arrObjetcs
            notiVc.isfromuser = true
            notiVc.isfromallOrder = true
            self.navigationController?.pushViewController(notiVc, animated: false)
    }
    
    

    func Getallmenus()
    {
        self.showLoader()
        NewtworkManager.shared.GetAllActiveInactiveMenuDetails { (status, dataIn) in
            self.hideloader()
            if (status)
            {
                self.menuArray = (dataIn as? [DKAPIMenuResponseElement])!
                
                for objOrder in self.menuArray
                {
                    if(self.dictOrderBymenu1[objOrder.menuItemID] == nil)
                    {
                        self.dictOrderBymenu1[objOrder.menuItemID] = objOrder
                    }
                }
            }
            
             self.getAllOrder()
        }
    }
}
