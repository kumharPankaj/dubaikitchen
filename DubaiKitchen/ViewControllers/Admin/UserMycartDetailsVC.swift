//
//  OrderVC.swift
//  DubaiKitchen
//
//  Created by Pankaj Kumhar on 2/28/19.
//  Copyright © 2019 Pankaj Kumhar. All rights reserved.
//

import UIKit

class UserMycartDetailsVC: CommonViewController
{

    var catagoryId:Int = 0
    var catagoryname:String = ""
    var arritems:[DKAPIOrderElement] =  [DKAPIOrderElement]()
    override func viewDidLoad()
    {
        super.viewDidLoad()

        self.title = catagoryname
        self.updateNavTitle(catagoryname, true) { (name) in
            if(name == "right")
            {
                return
            }
             DispatchQueue.main.async {
                        self.navigationController?.popViewController(animated: false)
                    }
        }
        tblView.register(UINib(nibName: "AdminTVCell", bundle: nil), forCellReuseIdentifier: "cellAdminCommon")
        tblView.tableFooterView = UIView()
        tblView.tableHeaderView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 20, height: 30))
        // Do any additional setup after loading the view.
          //addGradientColors_Nav()
        let dispatchQueue = DispatchQueue(label: "QueueIdentification", qos: .background)
        dispatchQueue.async
            {
                self.getCatagoryDetails()
        }
        
    }
    
    func getCatagoryDetails()
    {
        self.ShowLoader()
        NewtworkManager.shared.GetMenuDetailsByCategoryID("\(catagoryId)") { (status, datain) in
            self.hideloader()
            self.arritems = datain as! [DKAPIOrderElement]
            DispatchQueue.main.async {
                self.tblView.reloadData()
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if self.arritems.count > 0 {
            if arrUserCartItems.count == 0{
                for obj in arritems{
                    obj.orderQty = 0
                }
            }
        }

        DispatchQueue.main.async {
            self.tblView.reloadData()
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.arritems.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = (tableView.dequeueReusableCell(withIdentifier: "cellcart", for: indexPath) as? UITableViewCell)
        let viewController = cell?.viewWithTag(100) as! UserCartView
        let model =  self.arritems[indexPath.row]
//        if let keyGuest  = UserDefaults.standard.value(forKey: defaultsKeys.langSelected) {
//            if keyGuest as! String == ARABIC {
//                if model.menuItemname_AR.length > 0{
//                    viewController.UpdateUi(model.menuItemname_AR ,"MyCart")
//                } else {
//                    viewController.UpdateUi(model.menuItemname ,"MyCart")
//                }
//
//            } else {
//                viewController.UpdateUi(model.menuItemname ,"MyCart")
//            }
//        } else {
//            viewController.UpdateUi(model.menuItemname ,"MyCart")
//        }
        if isApplanguageIsArabic {
            viewController.UpdateUi(model.menuItemname_AR.isEmptyAndNil == false ? model.menuItemname_AR : model.menuItemname ,"MyCart")
        } else {
            viewController.UpdateUi(model.menuItemname ,"MyCart")
        }
        
        //viewController.UpdateUi("\(model.menuItemname)", "MyCart")
        viewController.setImageRight("")//"layer_\(indexPath.row+1)")
        viewController.setdataObject(model)
        viewController.setImageRemote(model.fullImagePath, placeholder: "MyCart")
        return cell!
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        DispatchQueue.main.async {
            let model =  self.arritems[indexPath.row]
            let notiVc = self.storyboard?.instantiateViewController(withIdentifier: "AddHouseViewController") as! AddHouseViewController
            notiVc.modalTransitionStyle = .coverVertical
            notiVc.modalPresentationStyle = .overCurrentContext
            notiVc.arritems = model
            self.navigationController?.present(notiVc, animated: true, completion: {
                
            })
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
