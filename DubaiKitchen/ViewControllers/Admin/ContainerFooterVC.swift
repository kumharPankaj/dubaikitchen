//
//  ContainerFooterVC.swift
//  DubaiKitchen
//
//  Created by Pankaj Kumhar on 2/23/19.
//  Copyright © 2019 Pankaj Kumhar. All rights reserved.
//

import UIKit


class ContainerFooterVC: UIViewController {

    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet var arrButtonFooters: [UIButton]!
    override func viewDidLoad() {
        super.viewDidLoad()

        if selectedUserRole == 3
        {
            arrButtonFooters[0].setImage(UIImage.init(named: "Dashboard")?.maskWithColor(color: UIColor.gray), for: .normal)
            arrButtonFooters[1].setImage(UIImage.init(named: "Menu")?.maskWithColor(color: UIColor.gray), for: .normal)
            arrButtonFooters[2].setImage(UIImage.init(named: "notification")?.maskWithColor(color: UIColor.gray), for: .normal)
            arrButtonFooters[3].setImage(UIImage.init(named: "Logout")?.maskWithColor(color: UIColor.gray), for: .normal)
        }
        else if  selectedUserRole == 2
        {
          arrButtonFooters[0].setImage(UIImage.init(named: "Menu")?.maskWithColor(color: UIColor.gray), for: .normal)
            arrButtonFooters[1].setImage(UIImage.init(named: "MyCart")?.maskWithColor(color: UIColor.gray), for: .normal)
            arrButtonFooters[2].setImage(UIImage.init(named: "notification")?.maskWithColor(color: UIColor.gray), for: .normal)
        }
        switch selectedFooterOption {
        case 4:
            
            arrButtonFooters[3].setImage(UIImage.init(named: "Logout")?.maskWithColor(color: UIColor.init(hexString: "FF7300")!), for: .normal)
        case 1:
            switch selectedUserRole{
                case 1:
                    arrButtonFooters[0].setImage(UIImage.init(named: "Management")?.maskWithColor(color: UIColor.init(hexString: "FF7300")!), for: .normal)
                case 3:
                arrButtonFooters[0].setImage(UIImage.init(named: "Dashboard")?.maskWithColor(color: UIColor.init(hexString: "FF7300")!), for: .normal)
            case 2:
               arrButtonFooters[0].setImage(UIImage.init(named: "Menu")?.maskWithColor(color: UIColor.init(hexString: "FF7300")!), for: .normal)
                default:
                print("")
            }
            
        case 2:
            switch selectedUserRole{
            case 1:
                arrButtonFooters[1].setImage(UIImage.init(named: "notification")?.maskWithColor(color: UIColor.init(hexString: "FF7300")!), for: .normal)
            case 2:
                 arrButtonFooters[1].setImage(UIImage.init(named: "MyCart")?.maskWithColor(color: UIColor.init(hexString: "FF7300")!), for: .normal)
            case 3:
                arrButtonFooters[1].setImage(UIImage.init(named: "Menu")?.maskWithColor(color: UIColor.init(hexString: "FF7300")!), for: .normal)
            default:
                print("")
            }
            
        case 3:
            switch selectedUserRole{
            case 1:
                arrButtonFooters[2].setImage(UIImage.init(named: "Dashboard")?.maskWithColor(color: UIColor.init(hexString: "FF7300")!), for: .normal)
            case 2,3:
                arrButtonFooters[2].setImage(UIImage.init(named: "notification")?.maskWithColor(color: UIColor.init(hexString: "FF7300")!), for: .normal)
            default:
                print("")
            }
        default:
            print("")
        }
        // Do any additional setup after loading the view.
        
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: .didReceiveData, object: nil)
        self.lblCount.isHidden = true
        self.updateBadge()
    }
    
    @objc func onDidReceiveData(_ notification:Notification)
    {
        // Do something now
        self.updateBadge()
    }
    
    func updateBadge()
    {
        if(NewtworkManager.shared.logoinUserType == NewtworkManager.shared.UserLogOn)
        {
            DispatchQueue.main.async {
                let noticount = arrCookComment.count
                self.lblCount.isHidden = true
                if(noticount>0)
                {
                    self.lblCount.isHidden = false
                    self.lblCount.text = "\(noticount)"
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print(" footer viewWillAppear")
    }
    
    @IBAction func btnFooterOptionClicked(_ sender: UIButton) {
        selectedFooterOption = sender.tag
        switch sender.tag {
        case 4:
            self.showAlerttwobtn(title: "", message: NSLocalizedString("Do you want to logout?", comment:""))
            { (alertaction) in
                
                DispatchQueue.main.async {
                    selectedFooterOption = -1
                    isUserLoggedIn = false
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    
                    let initialViewController = storyboard.instantiateViewController(withIdentifier: "initialnavigationviewcontroller")
                    let appdelegate = UIApplication.shared.delegate as! AppDelegate
                    appdelegate.window?.rootViewController = initialViewController
                }
            }
            
        case 1:
            if selectedUserRole == 3
            {
                let notiVc = self.storyboard?.instantiateViewController(withIdentifier: "DashboardVc") as! DashboardVc
                self.parent!.navigationController?.pushViewController(notiVc, animated: false)
            }
            else if selectedUserRole == 2
            {
                let notiVc = self.storyboard?.instantiateViewController(withIdentifier: "UserMenuVC") as! UserMenuVC
                self.parent!.navigationController?.pushViewController(notiVc, animated: false)
            }
            else
            {
                let notiVc = self.storyboard?.instantiateViewController(withIdentifier: "ManagementVC") as! ManagementVC
                self.parent!.navigationController?.pushViewController(notiVc, animated: false)
            }
            
        case 2:
            if selectedUserRole == 1
            {
                let notiVc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
                self.parent!.navigationController?.pushViewController(notiVc, animated: false)
            } else  if selectedUserRole == 2
            {
                let notiVc = self.storyboard?.instantiateViewController(withIdentifier: "UserMycartVC") as! UserMycartVC
                self.parent!.navigationController?.pushViewController(notiVc, animated: false)
            }
            else if selectedUserRole == 3
            {
                let notiVc = self.storyboard?.instantiateViewController(withIdentifier: "OrderVC") as! OrderVC
                self.parent!.navigationController?.pushViewController(notiVc, animated: false)
            }
            
        case 3:
            if selectedUserRole == 1
            {
                let notiVc = self.storyboard?.instantiateViewController(withIdentifier: "DashboardVc") as! DashboardVc
                self.parent!.navigationController?.pushViewController(notiVc, animated: false)
            }
            else if selectedUserRole == 2
            {
                let notiVc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
                notiVc.titleNavigation = "user"
                self.parent!.navigationController?.pushViewController(notiVc, animated: false)
            }
            else if selectedUserRole == 3
            {
                let notiVc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
                
                
                self.parent!.navigationController?.pushViewController(notiVc, animated: false)
            }
        default:
            print("")
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension Notification.Name {
    static let didReceiveData = Notification.Name("didReceiveData")
    static let didCompleteTask = Notification.Name("didCompleteTask")
    static let completedLengthyDownload = Notification.Name("completedLengthyDownload")
}
