//
//  OrderVC.swift
//  DubaiKitchen
//
//  Created by Pankaj Kumhar on 2/28/19.
//  Copyright © 2019 Pankaj Kumhar. All rights reserved.
//

import UIKit

class OrderVC: CommonViewController {

    var selectedTypeorder:String = "Pending"
    let stringorderpen = "Pending"
    let stringorderacc = "Accepted"
     private var arrOrderByKitchennew = [DKAPIOrderElement]()
    private var arrOrderByKitchenacc = [DKAPIOrderElement]()
    private var arrOrderByKitchen = [DKAPIOrderElement]()
    private var dictOrderByKitchen = [String:[DKAPIOrderElement]]()
    
    private var dictOrderBymenu = [String:DKAPIMenuResponseElement]()
    private var houseArray:[DKAPIHomeResponseElement] = [DKAPIHomeResponseElement]()
    private var dictOrderByHome = [String:DKAPIHomeResponseElement]()
    
    @IBAction func btnorderacceptclick(_ sender: UIButton)
    {
        ChnagebuttonStatus(sender)
    }
    
    @IBAction func btnneworderclick(_ sender: UIButton)
    {
        ChnagebuttonStatus(sender)
    }
    
    func ChnagebuttonStatus(_ sender: UIButton)
    {
        btnNewOrder.backgroundColor = UIColor.white
        btnorderaccepted.backgroundColor = UIColor.white
        
        if(sender == btnorderaccepted)
        {
            btnorderaccepted.backgroundColor = UIColor.clear
            parseOrder(stringorderacc)
        }
        
        if(sender == btnNewOrder)
        {
            btnNewOrder.backgroundColor = UIColor.clear
            parseOrder(stringorderpen)
        }
    }
    
    func parseOrder(_ stringorder:String = "Pending")
    {
        self.selectedTypeorder = stringorder
        self.arrOrderByKitchen.removeAll()
        self.arrOrderByKitchennew.removeAll()
         self.arrOrderByKitchenacc.removeAll()
        
        for (_ ,value) in dictOrderByKitchen
        {
            let objOrder = value[0]
            if(objOrder.orderStatus == "Order Placed" || objOrder.orderStatus == "Order Recieved")
            {
                self.arrOrderByKitchennew.append(objOrder)
            }
            else
            {
                self.arrOrderByKitchenacc.append(objOrder)
            }
        }
        
        if self.selectedTypeorder == self.stringorderacc
        {
            self.arrOrderByKitchen =   self.arrOrderByKitchenacc.reversed()
        }
        else {
            self.arrOrderByKitchen =   self.arrOrderByKitchennew
        }
        if self.selectedTypeorder == self.stringorderacc
        {
            var arrCancelled = self.arrOrderByKitchen.filter { (objCancell) -> Bool in
                return objCancell.orderStatus == "Order Cancelled"
            }
            arrCancelled = arrCancelled.sorted(by: {convertStringToDateFormate(strDate: $0.deliveryDate).compare (convertStringToDateFormate(strDate: $1.deliveryDate)) == .orderedDescending})
            var arrDelivered = self.arrOrderByKitchen.filter { (objCancell) -> Bool in
                return objCancell.orderStatus == "Order Delivered"
            }
            
            arrDelivered = arrDelivered.sorted(by: {convertStringToDateFormate(strDate: $0.deliveryDate).compare (convertStringToDateFormate(strDate: $1.deliveryDate)) == .orderedDescending})
            
            var arrPreparing = self.arrOrderByKitchen.filter { (objCancell) -> Bool in
                return objCancell.orderStatus == "Order Accepted"
            }
            arrPreparing = arrPreparing.sorted(by: {convertStringToDateFormate(strDate: $0.deliveryDate).compare (convertStringToDateFormate(strDate: $1.deliveryDate)) == .orderedDescending})
            
            self.arrOrderByKitchen.removeAll()
            self.arrOrderByKitchen.append(contentsOf: arrPreparing)
            self.arrOrderByKitchen.append(contentsOf: arrDelivered)
            self.arrOrderByKitchen.append(contentsOf: arrCancelled)
        }
        
        else {
        self.arrOrderByKitchen = self.arrOrderByKitchen.sorted(by: {convertStringToDateFormate(strDate: $0.deliveryDate).compare (convertStringToDateFormate(strDate: $1.deliveryDate)) == .orderedDescending})
        }
       // self.arrOrderByKitchen = self.arrOrderByKitchen.sorted(by: { Int($0.orderID)! > Int($1.orderID )!})
        GetallHome()
        //self.tblView.reloadData()
        
    }
    
    func convertStringToDateFormate(strDate: String) -> Date {
        var dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"// yyyy-MM-dd"
        //dateFormatter.timeZone = TimeZone(abbreviation: "GMT+5:30")
      
            let date = dateFormatter.date(from: strDate)
        
        return date ?? Date()
    }
    @IBOutlet weak var btnorderaccepted: UIButton!
    @IBOutlet weak var btnNewOrder: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        //addGradientColors_Nav()
        self.title = NSLocalizedString("Order", comment:"")
        self.updateNavTitle(NSLocalizedString("Order", comment:""), true) { (name) in
            if(name == "right")
            {
                return
            }
             DispatchQueue.main.async {
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: AdminVC.self) {
                        _ =  self.navigationController!.popToViewController(controller, animated: false)
                        break
                    }
                }
                       // self.navigationController?.popViewController(animated: false)
                    }
        }
        tblView.register(UINib(nibName: "OrderCell", bundle: nil), forCellReuseIdentifier: "cellOrder")
        tblView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
       
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         getAllOrder()
    }
    private func GetallHome()
    {
        self.showLoader()
        NewtworkManager.shared.GetAllHomeDetails { (status, dataIn) in
            self.hideloader()
            if (status)
            {
                self.houseArray = (dataIn as? [DKAPIHomeResponseElement])!
                for objOrder in self.houseArray
                {
                    if(self.dictOrderByHome[objOrder.homeID] == nil)
                    {
                        self.dictOrderByHome[objOrder.homeID] = objOrder
                    }
                }
            }
            DispatchQueue.main.async
                {
                    
                    self.tblView.reloadData()
            }
            
        }
    }
    
    func getAllOrder()
    {
        self.arrOrderByKitchennew.removeAll()
        self.arrOrderByKitchenacc.removeAll()
        self.dictOrderByKitchen.removeAll()
        
        self.showLoader()
        NewtworkManager.shared.GetAllOrderByKitchenId("1") { (status, dataIn) in
            self.hideloader()
            if (!status)
            {
                return
            }
            
            if let dataObj = dataIn as? [DKAPIOrderElement]
            {
                let temparrOrderByKitchen = dataObj
                
                for objOrder in temparrOrderByKitchen
                {
                    var arr:[DKAPIOrderElement]!
                    if(self.dictOrderByKitchen[objOrder.orderID] == nil)
                    {
                        arr = [DKAPIOrderElement]()
                        arr.append(objOrder)
                        self.dictOrderByKitchen[objOrder.orderID] = arr
                    } else {
                        arr =  self.dictOrderByKitchen[objOrder.orderID]
                        arr.append(objOrder)
                        self.dictOrderByKitchen[objOrder.orderID] = arr
                    }
                }
                
                
                DispatchQueue.main.async
                    {
                        if self.selectedTypeorder == self.stringorderacc
                        {
                            self.ChnagebuttonStatus(self.btnorderaccepted)
                        }
                        else {
                            self.ChnagebuttonStatus(self.btnNewOrder)
                        }
                        
                }
                
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOrderByKitchen.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        /*let cell:AdminTVCell  = (tableView.dequeueReusableCell(withIdentifier: "cellAdminCommon", for: indexPath) as? AdminTVCell)!
         cell.imgBubble.image = UIImage.init(named: "layer_\(indexPath.row+1)")
        if indexPath.row == 0{
            cell.lblTitle.text = "Pending"
            
        } else {
            cell.lblTitle.text = "Accepted"
        }
        cell.imgViewLogo.image = UIImage.init(named: "Menu")?.maskWithColor(color: UIColor.gray)
        return cell*/
        
        let cell:OrderCell  = (tableView.dequeueReusableCell(withIdentifier: "cellOrder", for: indexPath) as? OrderCell)!
         cell.selectionStyle = .none
        //cell.imgBubble.isHidden = true
        let obj = arrOrderByKitchen[indexPath.row]
        
        if let keyGuest  = UserDefaults.standard.value(forKey: defaultsKeys.langSelected) {
            if keyGuest as! String == ARABIC {
                if obj.menuItemname_AR.length > 0{
                    cell.lblMenuItemName.text = obj.menuItemname_AR + "\(obj.orderQty ?? 0)"
                } else {
                    cell.lblMenuItemName.text = obj.menuItemname + "\(obj.orderQty ?? 0)"
                }
                
            } else {
                cell.lblMenuItemName.text = obj.menuItemname + "\(obj.orderQty ?? 0)"
            }
        } else {
            cell.lblMenuItemName.text = obj.menuItemname + "\(obj.orderQty ?? 0)"
        }
        //cell.lblMenuItemName.text = obj.menuItemname + "\(obj.orderQty ?? 0)"
        
        var username = ""
        if arrOrderByKitchen.count > 0{
            let arr = arrGlobalActiveInactiveUsers.filter { (objUser) -> Bool in
                return Int(objUser.userID) == Int(obj.userID)
            }
            if arr.count > 0{
                username = arr[0].userName
            }
        }
        
        //cell.imgViewMenuItemImage.image = nil
        cell.btnstatus.setTitle("", for: .normal)
         cell.btnstatus.backgroundColor = UIColor.clear
        cell.btnstatus.isUserInteractionEnabled = true
        if obj.orderStatus == "Order Placed" || obj.orderStatus == "Order Recieved"{
            cell.btnstatus.backgroundColor = UIColor.init(hexString: "00943B")
            cell.btnstatus.setTitle(NSLocalizedString("Accept", comment:""), for: .normal)
            cell.lblMenuItemName.text = NSLocalizedString("User ", comment:"") + username + NSLocalizedString(" placed order ", comment:"")//+obj.orderID
        } else if obj.orderStatus == "Order Accepted"{
            cell.btnstatus.backgroundColor = UIColor.orange
            cell.btnstatus.setTitle(NSLocalizedString("Preparing", comment:""), for: .normal)
            cell.lblMenuItemName.text = NSLocalizedString("Kitchen ", comment:"") + username + NSLocalizedString(" preparing order", comment:"")// User"// + obj.userID
        } else if obj.orderStatus == "Order Delivered"{
            cell.btnstatus.backgroundColor = UIColor.gray
            cell.btnstatus.setTitle(NSLocalizedString("Delivered", comment:""), for: .normal)
            cell.btnstatus.isUserInteractionEnabled = false
            cell.lblMenuItemName.text =  NSLocalizedString("Kitchen ", comment:"") + username + NSLocalizedString(" delivered order", comment:"")// User"// + obj.userID
        }
        else if obj.orderStatus == "Order Cancelled"{
            cell.btnstatus.backgroundColor = UIColor.red
            cell.btnstatus.setTitle(NSLocalizedString("Cancelled", comment:""), for: .normal)
            cell.btnstatus.isUserInteractionEnabled = false
            cell.lblMenuItemName.text = NSLocalizedString("User ", comment:"") + username + NSLocalizedString(" cancelled order ", comment:"") + obj.orderID
        }
       
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let dateorder = dateFormatter.date(from: obj.orderDate)
        let datedel = dateFormatter.date(from: obj.deliveryDate)
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let strorderdate = dateFormatter.string(from: dateorder!)
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
        var strdeldate = obj.deliveryDate
        if let datedelev = datedel
        {
            strdeldate = dateFormatter.string(from: datedelev)
        }
        
        
        let homeDetails = self.dictOrderByHome[obj.deliveryHomeID]
        cell.orderdate.text = NSLocalizedString("Delivery Location", comment:"") + ":" + (homeDetails?.homeTitle ?? "")
        //cell.orderdate.text = "Delivery Location:"+obj.  //"OrderDate:"+strorderdate
        cell.deliverydate.text = NSLocalizedString("Delivery Date", comment:"") + ": " + strdeldate!
        
        cell.callbackHandler =
            { (status,data) in
                if status == "order"
                {
                    var orderstuas = ""
                    if obj.orderStatus == "Order Placed" || obj.orderStatus == "Order Recieved"
                    {
                        orderstuas = "Order Accepted"
                    } else if obj.orderStatus == "Order Accepted"{
                        orderstuas = "Order Delivered"
                    }
                    else if obj.orderStatus == "Order Delivered"{
                        
                    }
                    self.showLoader()
                    NewtworkManager.shared.ChnageOrderStatus(NewtworkManager.shared.logoinUserid,obj.orderID,orderstuas){(status, dataIn) in
                        self.hideloader()
                        if status{
                            self.getAllOrder()
                        }
                    }
                }
        }
        //cell.StatusButton(true)
        return cell
        
        
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 80 
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let obj = arrOrderByKitchen[indexPath.row]
        let arrObjetcs = self.dictOrderByKitchen[obj.orderID]
        
        let notiVc = self.storyboard?.instantiateViewController(withIdentifier: "OrderDetailsVC") as! OrderDetailsVC
        notiVc.arrOrderByKitchen = arrObjetcs!
        notiVc.complitionHandler =
            { (status,data) in
                if status == true
                {
                    self.getAllOrder()
                }
        }
        self.navigationController?.pushViewController(notiVc, animated: false)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
