//
//  OrderVC.swift
//  DubaiKitchen
//
//  Created by Pankaj Kumhar on 2/28/19.
//  Copyright © 2019 Pankaj Kumhar. All rights reserved.
//

import UIKit

class OrderDetailsVC: CommonViewController
{

    public var complitionHandler:WebCallbackHandler? = nil
    var arrOrderByKitchen = [DKAPIOrderElement]()
    var orderByKitchen:DKAPIOrderElement!
    var cellplaceorder:UITableViewCell? = nil
    var buttonplaceorder:UIButton! = nil
    var menuArray:[DKAPIMenuResponseElement] = [DKAPIMenuResponseElement]()
    var isMenuLoded:Bool = false
    var celldes:UITableViewCell? = nil
    var viewdes:ViewDescription!
    @IBOutlet weak var btnorderaccepted: UIButton!
    @IBOutlet weak var btnNewOrder: UIButton!
    private var dictOrderBymenu = [String:DKAPIMenuResponseElement]()
    private var houseArray:[DKAPIHomeResponseElement] = [DKAPIHomeResponseElement]()
    private var dictOrderByHome = [String:DKAPIHomeResponseElement]()
    var labelDetails:UILabel!
    var lblUserDetails: UILabel!
    var isfromuser: Bool  = false
    var isPostread: Bool  = false
    var isfromallOrder: Bool  = false
    var celldes1:UITableViewCell!
    var cellUserDetails:UITableViewCell!
    
    var celldesfeed:UITableViewCell? = nil
    var viewdesfeed:ViewDescription!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //addGradientColors_Nav()
        self.title = NSLocalizedString("Order Details", comment:"")
        self.updateNavTitle(NSLocalizedString("Order Details", comment:""), true) { (name) in
            if(name == "right")
            {
                return
            }
             DispatchQueue.main.async {
                        self.navigationController?.popViewController(animated: false)
                    }
        }
        tblView.register(UINib(nibName: "OrderCell", bundle: nil), forCellReuseIdentifier: "cellOrder")
        tblView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
        cellplaceorder = tblView.dequeueReusableCell(withIdentifier: "cellplaceorder")
        buttonplaceorder = (cellplaceorder?.viewWithTag(100) as! UIButton)
        buttonplaceorder.addTarget(self, action: #selector(placeoprder(sender:)), for: .touchUpInside)
        orderByKitchen = arrOrderByKitchen[0]
        self.buttonplaceorder.backgroundColor = UIColor.clear
        self.buttonplaceorder.setTitle("", for: .normal)
        celldes = tblView.dequeueReusableCell(withIdentifier: "celldes")
        viewdes = (celldes?.viewWithTag(100) as! ViewDescription)
        viewdes.setuserintraction(false)
        viewdes.setplaceHolder(NSLocalizedString("User Comments", comment:""))
        viewdes.settextstr(self.orderByKitchen.userComment ?? " ")
     
        
        //celldesfeed
        celldesfeed = tblView.dequeueReusableCell(withIdentifier: "celldesfeed")
        viewdesfeed = (celldesfeed?.viewWithTag(100) as! ViewDescription)
        if (isfromuser == true)
        {
            viewdesfeed.setuserintraction(false)
        }else
        {
            viewdesfeed.setuserintraction(true)
        }
        
        viewdesfeed.setplaceHolder(NSLocalizedString("Cook feedback", comment:""))
        self.orderByKitchen.cookComment = self.orderByKitchen.cookComment.replace(target: "'", withString: "")
        viewdesfeed.settextstr(self.orderByKitchen.cookComment ?? "")
        
        celldes1 = tblView.dequeueReusableCell(withIdentifier: "celldes1")
        labelDetails = (celldes1?.viewWithTag(100) as! UILabel)
        
        cellUserDetails = tblView.dequeueReusableCell(withIdentifier: "cellUserDetails")
        lblUserDetails = (cellUserDetails?.viewWithTag(1) as! UILabel)
        if arrOrderByKitchen.count > 0{
            let arr = arrGlobalActiveInactiveUsers.filter { (objUser) -> Bool in
                return objUser.userID == arrOrderByKitchen[0].userID
            }
            if arr.count > 0{
                lblUserDetails.text = "\n" + NSLocalizedString(" Order by: ", comment:"") + arr[0].userName + "\n" + NSLocalizedString(" Phone no: ", comment:"") + arr[0].phoneNo + "\n" + NSLocalizedString(" Age Group: ", comment:"") + arr[0].userGroup + "\n"
            }
        }
        
        if (isfromuser == true)
        {
            if let user = NewtworkManager.shared.selectedUser
            {
                 lblUserDetails.text = "\n" + NSLocalizedString(" Order by: ", comment:"") + user.userName + "\n" + NSLocalizedString(" Phone no: ", comment:"") + user.phoneNo + "\n" + NSLocalizedString(" Age Group: ", comment:"") + user.userGroup + "\n"
            }
           
        }
        
        
        ApiCall()
    }
    
    func GetallHome()
    {
        self.showLoader()
        NewtworkManager.shared.GetAllActiveInActiveHomeDetails { (status, dataIn) in
            self.hideloader()
            if (status)
            {
                self.houseArray = (dataIn as? [DKAPIHomeResponseElement])!
                for objOrder in self.houseArray
                {
                    if(self.dictOrderByHome[objOrder.homeID] == nil)
                    {
                        self.dictOrderByHome[objOrder.homeID] = objOrder
                    }
                }
                
                DispatchQueue.main.async
                    {
                      let homeDetails = self.dictOrderByHome[self.orderByKitchen.deliveryHomeID]
                        var housetitle = homeDetails?.homeTitle
                        var houseLocation = homeDetails?.location
                        var housedes = homeDetails?.homeDesc
                        
                        if(self.isApplanguageIsArabic)
                        {
                            housetitle = homeDetails?.homeTitle_AR.isEmptyAndNil == false ? homeDetails?.homeTitle_AR:homeDetails?.homeTitle
                            houseLocation = homeDetails?.location_ar.isEmptyAndNil == false ? homeDetails?.location_ar:homeDetails?.location
                            housedes = homeDetails?.homeDesc_AR.isEmptyAndNil == false ? homeDetails?.homeDesc_AR:homeDetails?.homeDesc
                        }
                        let delveryLoc = " " + NSLocalizedString("Delivery Location", comment:"")  + ":"
                        
                        self.labelDetails.text = "\(delveryLoc)\(housetitle ?? "")\n" +
                        NSLocalizedString("House Location", comment:"") + ":\(houseLocation!)\n" + NSLocalizedString("Description", comment:"") + ":\(housedes!)"
                        //delveryLoc+housetitle ?? ""+"\nHouse Location:"+houseLocation+"\nDescription:"+housedes
                        self.celldes1.reloadInputViews()
                    self.tblView.reloadData()
                }
            }
            //Post read request
            self.PostRead()
        }
    }
    
    func PostRead()
    {
        self.showLoader()
        NewtworkManager.shared.UpdateUnreadMessage("", orderByKitchen.orderID) { (status, dataIn) in
            self.hideloader()
           
            if(arrCookComment.contains(self.orderByKitchen.orderID))
            {
                let index = arrCookComment.firstIndex(of: self.orderByKitchen.orderID)
                if let index1 = index
                {
                    arrCookComment.remove(at: index1)
                }
                
                NotificationCenter.default.post(name: .didReceiveData, object: nil)
            }
            
            if(status)
            {
                
            }
            else {
                
            }
        }
    }
    
    func ApiCall()
    {
        self.showLoader()
        NewtworkManager.shared.GetAllActiveInactiveMenuDetails { (status, dataIn) in
            self.hideloader()
            self.GetallHome()
            if (status)
            {
                self.isMenuLoded = true
                self.menuArray = (dataIn as? [DKAPIMenuResponseElement])!
                
                for objOrder in self.menuArray
                {
                    if(self.dictOrderBymenu[objOrder.menuItemID] == nil)
                    {
                        self.dictOrderBymenu[objOrder.menuItemID] = objOrder
                    }
                }
                
                DispatchQueue.main.async
                    {
                    if self.orderByKitchen.orderStatus == "Order Placed" || self.orderByKitchen.orderStatus == "Order Recieved"{
                        self.buttonplaceorder.backgroundColor = UIColor.init(hexString: "00943B")
                        self.buttonplaceorder.setTitle(NSLocalizedString("Accept", comment:""), for: .normal)
                    } else if self.orderByKitchen.orderStatus == "Order Accepted"{
                        self.buttonplaceorder.backgroundColor = UIColor.orange
                        self.buttonplaceorder.setTitle(NSLocalizedString("Preparing", comment:""), for: .normal)
                    }
                    else if self.orderByKitchen.orderStatus == "Order Delivered"
                    {
                        self.buttonplaceorder.backgroundColor = UIColor.gray
                        self.buttonplaceorder.setTitle(NSLocalizedString("Delivered", comment:""), for: .normal)
                        self.buttonplaceorder.isUserInteractionEnabled = false
                    }
                    else if self.orderByKitchen.orderStatus == "Order Cancelled"
                    {
                        self.buttonplaceorder.backgroundColor = UIColor.red
                        self.buttonplaceorder.setTitle(NSLocalizedString("Order Cancelled", comment:""), for: .normal)
                        self.buttonplaceorder.isUserInteractionEnabled = false
                        }
                        
                    
                        if(self.isfromuser)
                        {
                            self.buttonplaceorder.isHidden = true
                            self.buttonplaceorder.isUserInteractionEnabled = false
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                            let datedel = dateFormatter.date(from: self.orderByKitchen.deliveryDate)
                            if let datede = datedel
                            {
                                let response =    datede.offsetFrom(date: Date())
                                if(response.1 > 1)
                                {
                                    
                                    if self.orderByKitchen.orderStatus == "Order Placed" || self.orderByKitchen.orderStatus == "Order Recieved"
                                    {
                                        self.buttonplaceorder.isHidden = false
                                        self.buttonplaceorder.isUserInteractionEnabled = true
                                        self.buttonplaceorder.backgroundColor = UIColor.init(hexString: "ff0000")
                                        self.buttonplaceorder.setTitle(NSLocalizedString("Cancel", comment:""), for: .normal)
                                    }
                                }
                            }
                            
                            if self.orderByKitchen.orderStatus == "Order Delivered"
                            {
                                self.buttonplaceorder.isHidden = false
                                self.buttonplaceorder.isUserInteractionEnabled = true
                                self.buttonplaceorder.backgroundColor = UIColor.init(hexString: "ff0000")
                                self.buttonplaceorder.setTitle(NSLocalizedString("Re Order", comment:""), for: .normal)
                            }
                            
                        }
                        
                        if(self.isfromallOrder)
                        {
                            self.buttonplaceorder.isHidden = false
                            self.buttonplaceorder.isUserInteractionEnabled = true
                            self.buttonplaceorder.backgroundColor = UIColor.init(hexString: "ff0000")
                            self.buttonplaceorder.setTitle(NSLocalizedString("Re Order", comment:""), for: .normal)
                        }
                    self.tblView.reloadData()
                }
            }
        }
    }
    
    @objc func placeoprder(sender: UIButton)
    {
        var orderstuas = ""
        if orderByKitchen.orderStatus == "Order Placed" || orderByKitchen.orderStatus == "Order Recieved"
        {
            if (self.isfromuser)
            {
               orderstuas = "Order Cancelled"
            }
            else
            {
               orderstuas = "Order Accepted"
            }
            
            orderByKitchen.orderStatus = orderstuas
        } else if orderByKitchen.orderStatus == "Order Accepted"{
            orderstuas = "Order Delivered"
            orderByKitchen.orderStatus = orderstuas
        }
        else if orderByKitchen.orderStatus == "Order Delivered"{
            
            if (self.isfromuser)
            {
                //Pleace order again
                let arrOrders = arrOrderByKitchen
                //let menudes = self.dictOrderBymenu[obj.menuItemID] 
                arrUserCartItems = arrOrders
                let notiVc = self.storyboard?.instantiateViewController(withIdentifier: "UserMycartVC") as! UserMycartVC
                self.navigationController?.pushViewController(notiVc, animated: false)
                return
            }
        }
        
        if(self.isfromallOrder)
        {
            //Pleace order again
            let arrOrders = arrOrderByKitchen
            //let menudes = self.dictOrderBymenu[obj.menuItemID]
            arrUserCartItems = arrOrders
            let notiVc = self.storyboard?.instantiateViewController(withIdentifier: "UserMycartVC") as! UserMycartVC
            self.navigationController?.pushViewController(notiVc, animated: false)
            return
        }
        
        self.showLoader()
        if(!orderstuas.isEmpty)
        {
                var cookComment = "''"
                if((self.orderByKitchen.cookComment) != nil)
                {
                    if(self.orderByKitchen.cookComment == self.viewdesfeed.gettext())
                    {
                        
                    }
                    else
                    {
                        if  let txt = self.viewdesfeed.gettext()
                        {
                            cookComment = txt.urlEncode
                        }
                    }
                    
                } else {
                    if  let txt = self.viewdesfeed.gettext()
                    {
                        cookComment = txt.urlEncode
                    }
                }
            NewtworkManager.shared.ChnageOrderStatus(NewtworkManager.shared.logoinUserid,orderByKitchen.orderID,orderstuas,"1",cookComment){(status, dataIn) in
                self.hideloader()
                if status{
                    DispatchQueue.main.async
                        {
                            if self.orderByKitchen.orderStatus == "Order Placed" || self.orderByKitchen.orderStatus == "Order Recieved"
                            {
                                self.buttonplaceorder.backgroundColor = UIColor.init(hexString: "00943B")
                                self.buttonplaceorder.setTitle(NSLocalizedString("Accept", comment:""), for: .normal)
                            }
                            else if self.orderByKitchen.orderStatus == "Order Accepted"
                            {
                                self.buttonplaceorder.backgroundColor = UIColor.orange
                                self.buttonplaceorder.setTitle(NSLocalizedString("Preparing", comment:""), for: .normal)
                            }
                            else if self.orderByKitchen.orderStatus == "Order Delivered"
                            {
                                self.buttonplaceorder.backgroundColor = UIColor.gray
                                self.buttonplaceorder.setTitle(NSLocalizedString("Delivered", comment:""), for: .normal)
                                self.buttonplaceorder.isUserInteractionEnabled = false
                            }
                            else if self.orderByKitchen.orderStatus == "Order Cancelled"
                            {
                                self.buttonplaceorder.backgroundColor = UIColor.red
                                self.buttonplaceorder.setTitle(NSLocalizedString("Order Cancelled", comment:""), for: .normal)
                                self.buttonplaceorder.isUserInteractionEnabled = false
                            }
                    }
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return self.isMenuLoded == true ? 2 : 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? arrOrderByKitchen.count : (arrOrderByKitchen.count>0 ? 5 : 0)
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if(indexPath.section == 0)
        {
            let cell:OrderCell  = (tableView.dequeueReusableCell(withIdentifier: "cellOrder", for: indexPath) as? OrderCell)!
            let obj = arrOrderByKitchen[indexPath.row]
            let menudes = self.dictOrderBymenu[obj.menuItemID]
            cell.selectionStyle = .none
            cell.updateWidthConstraints()
            
            if let keyGuest  = UserDefaults.standard.value(forKey: defaultsKeys.langSelected) {
                if keyGuest as! String == ARABIC {
                    if obj.menuItemname_AR.length > 0{
                        cell.lblMenuItemName.text = obj.menuItemname_AR + "\(obj.orderQty ?? 0)"
                    } else {
                        cell.lblMenuItemName.text = obj.menuItemname + "\(obj.orderQty ?? 0)"
                    }
                    
                } else {
                    cell.lblMenuItemName.text = obj.menuItemname + "\(obj.orderQty ?? 0)"
                }
            } else {
                cell.lblMenuItemName.text = obj.menuItemname + "\(obj.orderQty ?? 0)"
            }
            //cell.imgViewMenuItemImage.image = nil
            cell.btnstatus.setTitle("", for: .normal)
            cell.btnstatus.backgroundColor = UIColor.clear
            cell.btnstatus.isUserInteractionEnabled = true
            if obj.orderStatus == "Order Placed" || obj.orderStatus == "Order Recieved"{
                //cell.btnstatus.backgroundColor = UIColor.init(hexString: "00943B")
                //cell.btnstatus.setTitle("Accept", for: .normal)
            } else if obj.orderStatus == "Order Accepted"{
                //cell.btnstatus.backgroundColor = UIColor.orange
                //cell.btnstatus.setTitle("Preparing", for: .normal)
            } else if obj.orderStatus == "Order Delivered"{
                //cell.btnstatus.backgroundColor = UIColor.gray
                //cell.btnstatus.setTitle("Delivered", for: .normal)
                //cell.btnstatus.isUserInteractionEnabled = false
            }
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            let dateorder = dateFormatter.date(from: obj.orderDate)
            let datedel = dateFormatter.date(from: obj.deliveryDate)
            dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
//             var strorderdate  = obj.orderDate
             var strdeldate = obj.deliveryDate
            if(dateorder != nil)
            {
//                strorderdate = dateFormatter.string(from: dateorder!)
            }
            
            if(datedel != nil)
            {
                strdeldate = dateFormatter.string(from: datedel!)
            }
            
            
            if isApplanguageIsArabic{
                if let menuname = menudes?.menuItemName_AR
                {
                    if (menudes?.menuItemName_AR.length)! > 0{
                        cell.lblMenuItemName.text = NSLocalizedString("Item", comment:"") + ": " + (menuname)
                        obj.menuItemname = menuname
                    } else {
                        cell.lblMenuItemName.text = NSLocalizedString("Item", comment:"") + ": " + (menudes?.menuItemName ?? "")
                        obj.menuItemname = menuname
                    }
                    
                } else if let menuname = menudes?.menuItemName
                {
                    cell.lblMenuItemName.text = NSLocalizedString("Item", comment:"") + ": " + (menuname)
                    obj.menuItemname = menuname
                } else
                {
                    cell.lblMenuItemName.text = NSLocalizedString("Item", comment:"") + ": " + ("")
                    obj.menuItemname = ""
                }
            } else {
                if let menuname = menudes?.menuItemName
                {
                    cell.lblMenuItemName.text = NSLocalizedString("Item", comment:"") + ": " + (menuname)
                    obj.menuItemname = menuname
                }
                else
                {
                    cell.lblMenuItemName.text = NSLocalizedString("Item", comment:"") + ": " + ("")
                    obj.menuItemname = ""
                }
            }
            
            
            
            if let imagurl = menudes?.fullImagePath
            {
                 obj.fullImagePath = imagurl
              cell.setImageRemote(imagurl, placeholder: "Menu")
            } else {
                obj.fullImagePath = ""
               cell.setImageRemote("", placeholder: "Menu")
            }
            
            
            cell.orderdate.text = NSLocalizedString("Qty", comment:"") + ": " + convertNumberToLanguage("\((obj.orderQty)!)","ar_SA") 
            cell.deliverydate.text = NSLocalizedString("Delivery Date", comment:"") + ": " + strdeldate!
            
            cell.callbackHandler =
                { (status,data) in
                    if status == "order"
                    {
                        var orderstuas = ""
                        if obj.orderStatus == "Order Placed" || obj.orderStatus == "Order Recieved"
                        {
                            orderstuas = "Order Accepted"
                        } else if obj.orderStatus == "Order Accepted"{
                            orderstuas = "Order Delivered"
                        }
                        else if obj.orderStatus == "Order Delivered"{
                            
                        }
                        self.showLoader()
                        NewtworkManager.shared.ChnageOrderStatus(NewtworkManager.shared.logoinUserid,obj.orderID,orderstuas){(status, dataIn) in
                            self.hideloader()
                            if status{
                                DispatchQueue.main.async {
                                    if self.complitionHandler != nil
                                    {
                                        self.complitionHandler!(true,"added" as AnyObject)
                                    }
                                    self.navigationController?.popViewController(animated: false)
                                }
                            }
                        }
                    }
            }
            
            return cell
        } else {
            if(indexPath.row == 0) {return celldes!}
            if(indexPath.row == 1) {return celldes1!}
            if(indexPath.row == 2) {return cellUserDetails!}
            if(indexPath.row == 3) {return celldesfeed!}
            return cellplaceorder!
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if(indexPath.section == 0)
        {
            return 80
        }
        else if(indexPath.section == 1)
        {
            if indexPath.row == 0 && (self.orderByKitchen.userComment != nil && !self.orderByKitchen.userComment.isEmpty) {
                 return UITableView.automaticDimension
            }
            if (indexPath.row == 1)
            {
                return UITableView.automaticDimension//50
            }
            if (indexPath.row == 2)
            {
                return UITableView.automaticDimension
            }
            if (indexPath.row == 3 && !self.isfromuser)
            {
                return 100
            }
            if (indexPath.row == 3 && self.isfromuser == true && (self.orderByKitchen.cookComment != nil && !self.orderByKitchen.cookComment.isEmpty))
            {
                return UITableView.automaticDimension
            }
            if (indexPath.row == 4)
            {
                return 50
            }
           
        }
        return 0
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let notiVc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
//        notiVc.titleNavigation = indexPath.row == 0 ? "Pending" : "Accepted"
//        self.navigationController?.pushViewController(notiVc, animated: false)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
