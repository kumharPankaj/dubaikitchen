//
//  AppDelegate.swift
//  DubaiKitchen
//
//  Created by Pankaj Kumhar on 2/21/19.
//  Copyright © 2019 Pankaj Kumhar. All rights reserved.
//

import UIKit
import SDWebImage
import UserNotifications
import Messages
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,MessagingDelegate {
    
    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    static let sharedDelegate = {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        FirebaseApp.configure()
        self.configurePushNotification(application)
        // let arrWeekDates = Date().monthdate
        //let thre = Date().threedays
        //let thr5 = Date().weekdate
        NSObject.DoTheMagic()
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func configurePushNotification(_ application:UIApplication)   {
        
        Messaging.messaging().delegate = self
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        let token = Messaging.messaging().fcmToken
        print("FCM token: \(token ?? "")")
        //        DrawerRootViewController.shared().fcm_token = token
        if((token) != nil)
        {
            self.setNotificationToken(token!)
        }
        
        
    }
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String)
    {
        print("Firebase didRefreshRegistrationToken: \(fcmToken)")
        //DrawerRootViewController.shared().fcm_token = fcmToken
        self.setNotificationToken(fcmToken)
        
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage)
    {
        print("Did receive message:\(remoteMessage.appData)")
        handlePushNotification(remoteMessage.appData as NSDictionary)
    }
    
    func handlePushNotification(_ dictInfo:NSDictionary? = nil)
    {
        
        print("Notification receive:\(dictInfo)")
        
        let state: UIApplication.State = UIApplication.shared.applicationState
        
        if state == .background
        {
            
            return
        }
        
        if (dictInfo != nil)
        {
            print("dictInfo: \(dictInfo!)")
            let aps = dictInfo![AnyHashable("aps")] as? NSDictionary
            let alert = aps!["alert"] as? NSDictionary
            let body = alert!["body"] as? String
            let title = alert!["title"] as? String
            var notificationOrderId: String {
                
                if let noti = dictInfo?["gcm.notification.OrderID"]
                {
                    //noti = (noti as! String).trimming()
                    return (noti as! String).trimming()
                }
                return "-1"
            }
            
            notificationOrderID = notificationOrderId
            
            
            print("notificationOrderId:\(notificationOrderId)")
            
            if(state == .active)
            {
                if(title != nil && body != nil)
                {
                    AGPushNoteView.show(withNotificationMessage: body!, title: title!)
                    {
                        
                    }
                    
                    if NewtworkManager.shared.logoinUserType == NewtworkManager.shared.UserLogOn
                    {
                        if let topController = UIApplication.topViewController()
                        {
                            
                            DispatchQueue.global(qos: .background).async {
                                topController.getAllOrderByOrderId { (status, datain) in
                                    if status
                                    {
                                        let arr  = datain as! [DKAPIOrderElement]
                                        let orderByKitchen = arr[0]
                                        
                                        if orderByKitchen.cookComment != nil
                                        {
                                            if (!orderByKitchen.cookComment.isEmptyAndNil)
                                            {
                                                if(!arrCookComment.contains(notificationOrderID))
                                                {
                                                    arrCookComment.append(notificationOrderID)
                                                NotificationCenter.default.post(name: .didReceiveData, object: nil)
                                                    
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            
                        }
                    }
                    
                }
                
                return
            }
            
            NavigatetoVC()
        }
        
    }
    
    func NavigatetoVC()
    {
        if(Int(notificationOrderID) != -1)
        {
            if isUserLoggedIn
            {
                if NewtworkManager.shared.logoinUserType == NewtworkManager.shared.UserLogOn
                {
                    if let topController = UIApplication.topViewController()
                    {
                        topController.showLoader()
                        topController.getAllOrderByOrderId(callback: { (status, datain) in
                            topController.hideloader()
                            if status
                            {
                                let arr  = datain as! [DKAPIOrderElement]
                                let orderByKitchen = arr[0]
                                if orderByKitchen.orderStatus == "Order Placed"
                                {
                                    arrUserCartItems = arr
                                    topController.showLoader()
                                    topController.GetallmenusNames({ (status1, datain1) in
                                        topController.hideloader()
                                        
                                        if(status1)
                                        {
                                            var dictmenues = datain1 as![String:DKAPIMenuResponseElement]
                                            for model in arrUserCartItems
                                            {
                                                let menuitem = dictmenues[model.menuItemID]
                                                if (menuitem != nil)
                                                {
                                                    model.menuItemname = (menuitem?.menuItemName)!
                                                    model.menuItemname_AR = (menuitem?.menuItemName_AR)!
                                                }
                                            }
                                            
                                            DispatchQueue.main.async
                                                {
                                                    /* notificationOrderID = "-1"
                                                     let notiVc = topController.storyboard?.instantiateViewController(withIdentifier: "UserMycartVC") as! UserMycartVC
                                                     notiVc.IsComeFromNotifications = true
                                                     topController.navigationController?.pushViewController(notiVc, animated: false)*/
                                                    let notiVc = topController.storyboard?.instantiateViewController(withIdentifier: "OrderDetailsVC") as! OrderDetailsVC
                                                    notiVc.arrOrderByKitchen = arr
                                                    notiVc.isfromuser = true
                                                    notiVc.isfromallOrder = true
                                                    
                                                    if orderByKitchen.cookComment != nil
                                                    {
                                                        notiVc.isPostread = true
                                                    }
                                                    topController.navigationController?.pushViewController(notiVc, animated: false)
                                                    
                                            }
                                        }
                                    })
                                }
                                else
                                {
                                    DispatchQueue.main.async
                                        {
                                            notificationOrderID = "-1"
                                            let notiVc = topController.storyboard?.instantiateViewController(withIdentifier: "OrderDetailsVC") as! OrderDetailsVC
                                            notiVc.arrOrderByKitchen = arr
                                            notiVc.isfromuser = true
                                            topController.navigationController?.pushViewController(notiVc, animated: false)
                                    }
                                }
                                
                            }
                            
                        })
                        
                        
                    } else {
                        notificationOrderID = "-1"
                    }
                }
                else if NewtworkManager.shared.logoinUserType == NewtworkManager.shared.AdminLogOn
                {
                    
                }
                else
                {
                    // Cook and Kitchen Navigation
                    
                    if let topController = UIApplication.topViewController()
                    {
                        topController.showLoader()
                        topController.getAllOrderByOrderId(callback: { (status, datain) in
                            topController.hideloader()
                            if status
                            {
                                let arr  = datain as! [DKAPIOrderElement]
                                DispatchQueue.main.async
                                    {
                                        notificationOrderID = "-1"
                                        let notiVc = topController.storyboard?.instantiateViewController(withIdentifier: "OrderDetailsVC") as! OrderDetailsVC
                                        notiVc.arrOrderByKitchen = arr
                                        topController.navigationController?.pushViewController(notiVc, animated: false)
                                }
                            }
                            
                        })
                        
                        
                    } else {
                        notificationOrderID = "-1"
                    }
                }
            }
            else
            {
                // Login
            }
        }
    }
    
    
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        let token = Messaging.messaging().fcmToken
        //DrawerRootViewController.shared().fcm_token = token
        print("didRegisterForRemoteNotificationsWithDeviceToken: \(token)")
        guard token != nil else {
            return
        }
        
        self.setNotificationToken(token!)
        //self.refToken.child("token").setValue(token)
    }
    
    
    
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        Messaging.messaging().appDidReceiveMessage(userInfo)
        
        let userInfotemp = userInfo as? NSDictionary
        handlePushNotification(userInfotemp)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    // [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        Messaging.messaging().appDidReceiveMessage(userInfo)
        
        let userInfotemp = userInfo as? NSDictionary
        handlePushNotification(userInfotemp)
    }
    
}

// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey]
        {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print("Delegate:\n\n")
        let userInfotemp = userInfo as? NSDictionary
        //handlePushNotification(userInfotemp)
        completionHandler([])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        let userInfotemp = userInfo as? NSDictionary
        handlePushNotification(userInfotemp)
        
        completionHandler()
    }
}


extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}


